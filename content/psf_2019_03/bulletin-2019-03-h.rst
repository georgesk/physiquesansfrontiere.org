VULGARISATION
#############

:date: 2019-04-29 12:00
:category: Annonces
:tags: vulgarisation


Nous vous engageons à lire un livre d’un grand intérêt qui se propose de regarder la physique qui se trouve derrière des phénomènes du quotidien et qui à chaque chapitre, propose une petite manip didactique.

|image1|

**Du merveilleux caché dans le quotidien :   Benoît Roman, Etienne Reyssat, José Bico et Étienne Guyon**

Voir aussi : https://www.youtube.com/watch?v=ucEmTPuYx0s

.. |image1| image::  /images/bulletin-2019-03/bulletin-2019-3_21.jpg
    :height: 130px		     
