SELECTION D’ARTICLES de HARDWAREX (open access)
###############################################

:date: 2019-04-29 12:00
:category: Open access
:tags: laboratoire, optique, chimie

Remarque liminaire :
====================

Ce journal fait partie des éditions Elsevier, cependant nous ne
faisons pas la promotion de Elsevier mais nous pensons que l’accès
ouvert est une importante opportunité pour beaucoup de scientifiques
de pays à faibles ressources d’avoir accès à ces articles.

We are not doing the promotion of Elsevier but we think that the open
access of this journal will be interesting for a lot of scientist from
low resource countries.

**A low-cost bench-top research device for turbidity measurement by radially distributed illumination intensity sensing at multiple wavelengths**

`Ben G.B. Kitchenera, Simon D. Dixona Kieren O. Howartha Anthony J. Parsonsb John Wainwrightc Mark D. Batemanb James R. CooperdGraham K. Hargravee Edward J. Longe Caspar J.M. Hewettf <https://www.sciencedirect.com/science/article/pii/S2468067218300762#!>`__

https://doi.org/10.1016/j.ohx.2019.e00052

**Open-source Automatic Weather Station and Electronic Ablation Station for measuring the impacts of climate change on glaciers**

`Guilherme Tomaschewski Nettoa Jorge Arigony-Netoab <https://www.sciencedirect.com/science/article/pii/S2468067218300749#!>`__

https://doi.org/10.1016/j.ohx.2019.e00053

|image1|

**A simplified LED-driven switch for fast-scan controlled-adsorption voltammetry instrumentation**

`Rhiannon Robkea Parastoo Hashemia Eric Ramssonb <https://www.sciencedirect.com/science/article/pii/S246806721830035X#!>`__

https://doi.org/10.1016/j.ohx.2018.e00051

**An open-source multi-robot construction system**

`Michael Allwright Weixu Zhu Marco Dorigo <https://www.sciencedirect.com/science/article/pii/S2468067218300786#!>`__

https://doi.org/10.1016/j.ohx.2018.e00050

|image2|

**Programmable and low-cost ultraviolet room disinfection device**

`Marcel Bentancor Sabina Vidal <https://www.sciencedirect.com/science/article/pii/S2468067218300452#!>`__

https://doi.org/10.1016/j.ohx.2018.e00046

|image3|

**Open source 3D-printed 1000 µL micropump**

Jorge Bravo-Martinez http://dx.doi.org/10.1016/j.ohx.2017.08.002

|image4|

**Low-cost touchscreen driven programmable dual syringe pump for life science applications**

`Valentina E. Garciaa Jamin Liuc Joseph L. De Risi <https://www.sciencedirect.com/science/article/pii/S2468067218300269#!>`__

|image5|

A suivre……

A noter qu’il y a maintenant une revue *open access* associée pour le
software : SOFTWAREX   !!!

__________________

|image6| ÉDITION SCIENTIFIQUE,  Histoire du boycott de Elsevier, `« The cost of Knowledge », article dans Wikipedia <https://en.wikipedia.org/wiki/The_Cost_of_Knowledge>`__



.. |image1| image:: /images/bulletin-2019-03/bulletin-2019-3_1.jpg
   :width: 340px
   :class: nofloat
	   
.. |image2| image:: /images/bulletin-2019-03/bulletin-2019-3_2.jpg
   :width: 340px
   :class: nofloat
	   
.. |image3| image:: /images/bulletin-2019-03/bulletin-2019-3_3.jpg
   :width: 240px
   :class: nofloat
	   
.. |image4| image:: /images/bulletin-2019-03/bulletin-2019-3_4.jpg
   :width: 680px
	   
.. |image5| image:: /images/bulletin-2019-03/bulletin-2019-3_5.jpg
   :height: 340px
   :class: nofloat
	   
.. |image6| image:: /images/bulletin-2019-03/bulletin-2019-3_6.jpg
   :width: 200px
