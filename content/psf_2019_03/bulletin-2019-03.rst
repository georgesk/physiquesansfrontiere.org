Bulletin Physique sans Frontières (mars 2019)
#############################################

:date: 2019-04-29 12:00
:category: Bulletin
:tags: 


ÉDITORIAL:
==========

Ce bulletin couvre les mois de mars et d’avril car il n’y avait pas
assez de nouvelles à vous faire parvenir en mars. A ce propos je vous
incite à m’envoyer des nouvelles à me signaler des articles, des
conférences, pour que je ne me sente pas trop seul ! Ce bulletin
comporte des informations très diverses, j’espère que les informations
qu’il contient vous serons utiles.

**Une nouvelle triste** : suivant une
information du journal Le Monde,
**le décret instituant l’augmentation des frais d’inscription pour les masters pour les étudiants non communautaires**
aurait été inscrit au journal officiel.

Bienvenue aux membres d’**Optique sans Frontières** qui nous
rejoignent. Le nom de notre commission commune SFP SFO devrait donc
être Physique et Optique sans Frontières, sous réserve d’acceptation
par les instances ad-hoc. Un logo provisoire a été concocté, nous vous
le présenterons pour appréciation dans le prochain bulletin.

Enfin, en coopération avec
l’**APSA**, notre vice-présidente Odette Fokapu et notre partenaire
africain, le Professeur Paul Woafo (Université de Yaoundé), organisent
la deuxième édition du **"Challenge Physique Expérimentale Afrique"** qui
cette fois ci s’étend aux pays africains.  Le site internet est
http://www.concoursphysiqueafrique.org.  Nous remercions notre
collègue **Daniel Hennequin** pour la réalisation du site. Soulignons que
la première édition a abouti à la publication de deux articles,
respectivement dans **Reflets de la Physique** et **European Physics News**.

Bonne lecture François PIUZZI

----

Les articles du bulletin :
==========================

- `DEMANDES DE NOS COLLÈGUES AFRICAINS : </articles/2019/avril/29/bulletin-2019-03-a>`__
- `NOUVELLES EN INSTRUMENTATION : </articles/2019/avril/29/bulletin-2019-03-b>`__
- `SÉLECTION D’ARTICLES de HARDWAREX (open access) </articles/2019/avril/29/bulletin-2019-03-c>`__
- `Portait d’une jeune physicienne sénégalaise et militante pour la science : Maïmouna Diouf </articles/2019/avril/29/bulletin-2019-03-d>`__
- `ANNONCE à partager envoyée par Mikkel Brydegaard :  nouveau poste de doctorat en instrumentation ... </articles/2019/avril/29/bulletin-2019-03-e>`__
- `ÉNERGIES RENOUVELABLES: Dyson Award </articles/2019/avril/29/bulletin-2019-03-f>`__
- `NOUVELLES </articles/2019/avril/29/bulletin-2019-03-g>`__
- `Livre de Vulgarisation </articles/2019/avril/29/bulletin-2019-03-h>`__
- `NOUVELLES DE NOS PARTENAIRES </articles/2019/avril/29/bulletin-2019-03-i>`__
