NOUVELLES DE NOS PARTENAIRE
###########################

:date: 2019-04-29 12:00
:category: Annonces
:tags: Asie, nouvelles


IUPAP commission C13 « Physics for Development »
================================================

Michael Steinitz membre de la commission s’est rendu à la Second
conference *on women in physics in Asia* soutenue à hauteur de 7000 €
par cette commission. Au vu des photos nous regrettons de ne pas y
être allés !

|image1|
|image2|

.. |image1| image:: /images/bulletin-2019-03/bulletin-2019-3_31.jpg
   :width: 480px
   :class: nofloat
.. |image2| image:: /images/bulletin-2019-03/bulletin-2019-3_32.jpg
   :width: 480px
   :class: nofloat

