Portait d’une jeune physicienne sénégalaise et militante pour la science : Maïmouna Diouf
#########################################################################################

:date: 2019-04-29 12:00
:category: Portraits
:tags: Afrique, vulgarisation

Depuis toute jeune, Maïmouna se rêvait ingénieure télécoms, comme
papa. Elle découvre et adore les sciences physiques au lycée Blaise
Diagne de Dakar (Sénégal).  Au moment de choisir son orientation
post-bac, elle se rend compte que les télécoms, c’est plutôt des
maths. Elle opte alors pour une licence de Physique, « pour se laisser
le temps de choisir ». Une mention Bien au baccalauréat lui offre une
bourse d’excellence financée par le Sénégal pour étudier en France. La
voilà qui s’envole vers Lyon. Elle obtient à La Doua [#]_ une licence
en PCSI [#]_ option Physique avec la mention Très Bien.

L’énergie, sous toutes ses formes, est sa nouvelle passion. Elle est
fascinée par l’énergie nucléaire mais aussi attirée par les énergies
renouvelables (le Grenelle de l’environnement est passé par
là…). Après un M1 Énergies du XXIème Siècle à l'École Polytechnique
avec des dominantes en énergie nucléaire et énergie solaire, elle se
laisse séduire par cette dernière option. Après tout, les réactions
nucléaires sont à l’œuvre au cœur du soleil!

|image1|

Un M2 Renewable Energies Science and Technology à dominante énergie
solaire en poche, elle poursuit actuellement une thèse en sciences des
matériaux au Centre Interdisciplinaire de Nano-sciences de Marseille
(CINaM, Aix-Marseille Université-CNRS) [#]_.

En parallèle, elle a rejoint une toute jeune université au Sénégal, la
Dakar-American University of Science and Technology (DAUST). Elle y
enseigne les Sciences Physiques en 1ère année et est également chargée
du développement de la recherche.

Son autre passion ? La vulgarisation scientifique! Elle a eu le
plaisir de la pratiquer avec la Cellule Culture Scientifique et
Technique d’Aix-Marseille Université, à travers notamment le programme
Expérimentarium.

Active aussi dans le milieu associatif, elle est responsable du
partenariat pour la Société des Ingénieurs et Chercheurs Sénégalais [#]_
(3SE, acronyme de l’anglais). La 3SE œuvre pour le développement des
STIM [#]_ au Sénégal.

Dans cet esprit, Maïmouna travaille sur un projet d’atelier sur
l’Atomic Layer Deposition (ALD) au Sénégal en partenariat avec l'École
Supérieure Polytechnique de Dakar.

C’est la technique qu’elle utilise pour la synthèse de films minces
dans son travail de thèse. L’ALD permet la synthèse de nombreux
oxydes, quelques nitrures et métaux purs. Une variante, la Molecular
Layer Deposition, permet la synthèse de films organiques et hybrides.
L’épaisseur des couches se contrôle au nanomètre près et les domaines
d’application sont variés [1].

Les réacteurs ALD commerciaux sont onéreux mais l’utilisation de
réacteurs faits maison est devenue fréquente dans la communauté
ALD. Maïmouna espère que l’atelier impulsera le développement de
réacteurs home-made au Sénégal et la construction d’une communauté ALD
qui s’étendra en Afrique de l’Ouest. Son projet d’atelier sur la
déposition en couche atomique est décrit ci-après.

+--------------------------------------------------------------+
| |image2|                                                     |
+--------------------------------------------------------------+
| Maimouna devant un réacteur ALD au CINAM (Marseille, France) |
+--------------------------------------------------------------+

Projet d’atelier sur la déposition en couche atomique
=====================================================

Détails de l’atelier :
----------------------

-  Thème: Débuter en ALD avec un réacteur “maison” 
-  Lieu: Ecole Supérieure Polytechnique – Université Cheikh Anta Diop de Dakar
-  Ville, pays : Dakar, Sénégal 
-  Dates: à fixer 
-  Institution hôte: Ecole Supérieure Polytechnique (ESP) 
-  Organisateur: Maïmouna Wagane Diouf (Aix-Marseille University) ; Dr Ababacar Ndiaye (ESP) 
-  Nombre de participants attendus : 50

Description et Objectifs de l’atelier:
--------------------------------------

L'atelier présentera à un
**public de chercheurs en sciences des matériaux**,
la **technologie ALD** par le biais de sessions de deux
jours. L'accent sera mis sur le développement de
**« réacteurs faits maison », les équipements de laboratoire abordables**
étant un sujet
d'intérêt majeur pour la recherche au Sénégal.

L'objectif de l'atelier est de créer une communauté autour du
développement de l'ALD au Sénégal et d'initier un
**programme de renforcement des capacités pour ce développement**,
**tant en compétences qu'en équipement**. L’atelier aura également
pour objectif l’**établissement de relations de coopération de cette communauté à construire avec les communautés ALD développées ailleurs dans le monde**.

La participation des jeunes chercheurs et des femmes sera
particulièrement encouragée par le biais d’un soutien financier.

Finances :
----------

-  Budget total prévu: huit mille euros (8000€) détaillés comme suit :

  -  Frais de déplacement et per diem de 3 intervenants: trois mille six cent euros (3600€) (vol 700€/personne, per diem 125€/jour/personne pendant 4 jours)
  -  Restauration: mille quatre cent euros (1400€)
  -  Goodies et communication: mille euros (1000€)
  -  Bourses pour 20 participants pour encourager la participation  des femmes et des jeunes chercheurs : deux mille euros (2000€). 
  -  Salles : l’atelier sera accueilli par l’ESP à titre gracieux

-  Plan pour générer des revenus :

Une contribution symbolique de 160€/personne sera demandée pour
participer à l’atelier de façon à couvrir les frais. La contribution
pourra être directement payée par le participant ou par une
participation globale de leur institution ou d’autres organismes qui
soutiennent le développement de la carrière des chercheurs au Sénégal.

________________________________

Notes :
=======
.. [#] Université Claude Bernard Lyon 1
.. [#] Physique Chimie Science de l’Ingénieur
.. [#] Voir http://www.experimentarium.fr/les-chercheurs/allonger-la-duree-de-vie-des-cellules-photovoltaiques
.. [#] https://www.3se-global.org/about-us/
.. [#] S. M. George, ‘Atomic Layer Deposition: An Overview’, *Chem. Rev.*, vol. 110, no. 1, pp. 111–131, Jan. 2010.
   
.. |image1| image:: /images/bulletin-2019-03/bulletin-2019-3_11.jpg
   :width: 480px
.. |image2| image:: /images/bulletin-2019-03/bulletin-2019-3_12.jpg
   :width: 680px
