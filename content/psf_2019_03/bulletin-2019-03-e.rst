ANNONCE à partager
##################

:date: 2019-04-29 12:00
:category: Annonces
:tags: optique

\ ... envoyée par **Mikkel Brydegaard** (Associate prof. Dept. Physics, Lund University)

**EN**: We are announcing a new PhD position in DIY biophonic
 instrumentation for developing countries. It will be a hilarious
 photonics playground with compact 3D printed fluorescence lidars,
 LEGO goniometers and infrared smartphone spectrometers and I look
 much forward to this. If you have any clever, crafty and adventurous
 master student do not hesitate to share the link for the announcement
 and application below. (Swedish and English versions available).

https://lu.mynetworkglobal.com/en/what:job/jobID:257308/

**FR**\ : Nous annonçons un nouveau poste de doctorat en
 instrumentation biophotonique faite maison pour les pays en
 développement. Ce sera un terrain de jeu photonique hilarant avec des
 lidars fluorescents compacts imprimés en 3D, des goniomètres LEGO et
 des spectromètres infrarouges pour smartphones et j'ai bien hâte d'y
 être. Si vous avez des étudiants en master intelligents, rusés et
 aventureux, n'hésitez pas à partager le lien pour l'annonce et la
 candidature ci-dessous. (versions suédoise et anglaise disponibles).

*Traduit avec www.DeepL.com/Translator*
