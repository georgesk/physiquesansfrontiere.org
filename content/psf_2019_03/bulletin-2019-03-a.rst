DEMANDES DE NOS COLLÈGUES AFRICAINS
###################################

:date: 2019-04-29 12:00
:category: Enseignement
:tags: Afrique

Le professeur **Jérémie Zoueu** m’a envoyé un message pour la
revalorisation des déchets électroniques en **Côte d’Ivoire**
*Institut National Polytechnique Félix Houphouët-Boigny Unité Mixte de Recherche en Électronique et Électricité Appliquées*,
**Directeur** *Laboratoire d’Instrumentation Image et Spectroscopie*,
**Responsable** *Yamoussoukro*

« Je suis à la recherche d'une institution partenaire
et une personne contact pour un projet sur la revalorisation des
déchets électroniques. J'ai pensé que l'idée de construire des
équipements à partir du matériels de récupération pouvait bien
s'insérer dans une telle démarche.

Il s'agira principalement pour le partenaire au projet, de venir
former des étudiants, comme tu l'as fait l'année dernière. Cependant,
pour l'aspect administratif du projet, nous avons besoin que le
partenaire soit adossé à une institution. »


**Projet pour la conférence Quantum Africa 5** : le projet consisterait à
y envoyer un des trois doctorants du LKB qui nous avaient présenté une
expérience de travaux pratiques sur la physique quantique lors de la
3ème édition des Rencontres des Jeunes Chercheurs Africains en
France. Le professeur Andreas Buchleitner (directeur du laboratoire de
Physique Quantique de l’université de Fribourg et membre IUPAP C13)
est membre du comité d’organisation de cette conférence et pourrait
servir d’interface. Il faut penser au financement du voyage.

**Gabon** : la présidente de la Société Française de Physique, Catherine
Langlais a envoyé une lettre de soutien pour l’appel d’offre Adesa de
pour le master de physique au Gabon.

**Mejdi** document sur le concours Éthiopie ( en attente)

**Gabon** demande pour appel d’offre
