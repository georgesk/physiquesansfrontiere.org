NOUVELLES
#########

:date: 2019-04-29 12:00
:category: Annonces
:tags: Afrique, nouvelles


APS bulletin for Africa
-----------------------

https://www.aps.org/publications/apsnews/201903/international.cfm

First female in astrophysics in Senegal
---------------------------------------

https://africanshapers.com/salma-sylla-premiere-astrophysicienne-du-senegal/

Journée Pierre Curie et magnétisme/Day on Pierre Curie and magnetism
--------------------------------------------------------------------

https://www.espci.fr/fr/actualites/2019/journee-pierre-curie-et-le-magnetisme

Article du Monde sur le nouveau labo en IA (Google) au Ghana
------------------------------------------------------------

https://www.lemonde.fr/economie/article/2019/04/20/le-ghana-poste-avance-de-google-en-afrique_5452859_3234.html
