NOUVELLES EN INSTRUMENTATION
############################

:date: 2019-04-29 12:00
:category: Instrumentation
:tags: laboratoire, optique, chimie

LIBS ( Laser Induced Breakdown Spectroscvopy) applications:
**Air liquide**, **soudures** testées par **LIBS** https://www.photoniques.com/actualites/toutes-les-actualites/88-societes/4250-air-liquide-l-analyse-libs-pour-le-controle-des-alliages-et-soudures

analyseurs chimiques temps réel in situ industriels (LIBS) : IUMTEK  Orsay :  https://iumtek.com/

Microspectrofluorimètre portable  pour étudier les objets d’art
**Institut de recherche sur les achéomatériaux CNRS Bordeaux**

http://www.iramat-crp2a.cnrs.fr/spip/spip.php?article296
