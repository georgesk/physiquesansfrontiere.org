Projet d’échanges et collaborations scientifiques entre les villes de Grenoble et de Sfax (Tunisie) dans le cadre de leur jumelage
##################################################################################################################################

:date: 2018-01-10
:category: Recherche
:tags: jumelages

**Bernard BARBARA (DR-émérite, CNRS, Institut Néel, Grenoble)**

**Thématiques principales :** **Physique du solide et des matériaux, applications.**

**Échange** d’étudiants en **Master 2** et en **thèse** dans les deux sens
**Grenoble --> Sfax et Sfax --> Grenoble**.

**Organisation d’écoles de physiques** sur des thématiques choisies avec soin
afin de **satisfaire les besoins scientifiques et sociétaux** des
deux participants, organisées alternativement **à Grenoble et à Sfax**
avec dans les deux cas ** des étudiants** de chaque ville.

**I - Universitaires Sfaxiens impliqués** : pour accueillir/encadrer
des étudiants Grenoblois en Master 2 et enseigner dans le cadre des
écoles qui seront organisées à Grenoble et Sfax.

**Groupe de Abdelwaheb CHEIKHROUHOU Professeur de Physique émérite à
la faculté des Sciences de Sfax, ancien Président de l’Université,
Imam, avec le concours du laboratoire LT2S (laboratoire des
Technologies des Systèmes SMART avec Wissem CHEIKHROUHOU , Mohamed
KOUBAA).**
 
**Sujets d’études et spécialités** :

**Études fondamentales**: alliages
intermétalliques comprenant des éléments 3d et 4f.

Dopages des manganites et pérovskites.

Effet magnétocalorique, anisotropie magnétique.

Techniques de dépôt des couches minces.

Microscopie en champ proche (AFM/STM).

Matériaux à base de dioxyde de titane pour la photocatalyse.

Nanomatériaux pour le traitement des tumeurs et « smart materials » et
« smart sensors » pour le diagnostic et le traitement de certaines
maladies (cancers etc..).

Tissus micro-encapsulés et connectés.

**Applications:** réfrigération magnétique, aimants permanents,
photocatalyse, hyperthermie magnétique. Suivi des patients à distance.

**Groupe de Mohamed DAMMAK Professeur de Physique à la
faculté des Sciences de Sfax**

**Sujets d’études et spécialités** : 
**Études fondamentales:** synthèse caractérisation optique de
différents matériaux transparents dopés avec des ions de terres rares
(aux propriétés de luminescence exceptionnelles) ou/et des métaux de
transition. Effet de la dimension (poudre cristalline puis
nanostructures de différentes morphologies : nanofils,
nanoparticules).

**Applications** : biologiques et le photovoltaïques, LEDs,
Nanophosphors for medical, photovoltaic and display applications.

**Simulations numériques:** afin d’exploiter les propriétés optiques
des ions actifs dans les matrices hôtes.

**Groupe de Mohamed BEN SALEM**: **microscopie** électronique à transmission. 

**II - Universitaires Grenoblois impliqués :** Pour
accueillir/encadrer des étudiants Sfaxiens en Master 2 et enseigner
dans le cadre des écoles qui seront organisées à Sfax et Grenoble.

`Didier MAYOU <http://neel.cnrs.fr/spip.php?article260&personne=Didier.MAYOU>`__:
**La physique des cellules solaires** (Optique, semi-cond., approche quantique)

`Équipe de Benoit
BOULANGER : <http://neel.cnrs.fr/spip.php?article260&personne=Benoit.BOULANGER>`__\
**optique quantique**

Équipe `de Alain IBANEZ : <http://neel.cnrs.fr/spip.php?article260&personne=Alain.IBANEZ>`__
**chimie/optique**

`Aude BARBARA <http://neel.cnrs.fr/spip.php?article260&personne=Aude.BARBARA>`__:
**nano-Optique**

**Jean-Yves BIGOT et son équipe** : processus \ **femtoseconde**.

**Edgard BONET** : \ **nanophysique,** méthodes **numériques**,
**micro-électronique**

**Bernard BARBARA** : **nanophysique** classique et quantique, magnétisme.

**Daniel FRUCHART** : Les **alliages intermétalliques** et applications
dans les **aimant**\ s permanents et la **réfrigération magnétique**

**Gérard BIDAN** : stockage électrochimique de l’énergie (batterie,
supercapacités) et nanomatériaux.

Quelques autres **universitaires** français, **non-grenoblois**,
participeront aussi en raison de leurs relations étroites et anciennes
avec l’université de Sfax : Souad AMMAR (Paris 7): les méthodes
**d'élaboration par chimie douce** et applications des oxydes dans
**l'hyperthermie**.

**Jean Marc GRENECHE** (le Mans): La **spectroscopie Mossbaüer**


Enfin, nous aurons aussi la présence de **Albert FERT** (Orsay),
**Prix Nobel de physique**, qui donnera les conférences d’ouverture
(électronique de spin et/ou autres sujets
de son choix).

Une mission à Sfax, va être effectuée au mois d’avril par des membres
de la mairie de Grenoble qui devraient rencontrer le Président de
l’Université, dans le cadre du jumelage de ces deux villes.

**Apports possible dans le sens Grenoble --> Sfax**

Accueil, à Grenoble, d’étudiants en Master 2 et de doctorants Sfaxiens
et organisation de sessions (écoles etc..) dévouées à la transmission
de connaissances dans des domaines intéressant les Sfaxiens et où
Grenoble a une avance au niveau international.

**Apport possible dans le sens Sfax <-- Grenoble**

C’est la même chose avec, en plus un point particulier relatif à
l’approche Sfaxienne qui consiste à mettre les moyens humains
nécessaires, souvent assez importants, pour effectuer des études
systématiques de matériaux ayant des applications (différents dopages
différentes températures de recuit etc..), et ceci afin d’optimiser
les propriétés recherchées pour une application donnée.  Ce type de
recherches, risqué, est souvent négligé à Grenoble par des thésards
visant un poste permanent.  Ces études, si elles étaient effectuées en
cotutelle pourraient amener au dépôt de brevets communs.

Comme on le voit ce projet tient à éviter l’écueil d’un paternalisme
sous-jacent, contre-productif.  Pour cela il est basé sur une symétrie
totale dans les échanges d’étudiants, écoles …et aussi des bénéfices
dont chaque université pourra tirer de cette collaboration.

La mixité des jeunes participants dans des contextes géographiques
différents (pays d’origines, à Sfax et ses villes voisines au bords de
la Méditerranée ou à Grenoble et ses montagnes) sexes, religions,
devrait aussi contribuer à l’impact sociétal de ce projet.
