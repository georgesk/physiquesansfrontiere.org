Conférences ASESMA et Logiciel Open Source Quantum Expresso
###########################################################

:date: 2018-01-10
:category: Enseignement
:tags: logiciel


**Par Matteo Gatti**

|image25|
**ASESMA** est l’acronyme de *African School on Electronic Structure:*
*Methods and Applications*. Le but est de construire un réseau africain
pour les calculs sur les matériaux et les sciences biologiques. Pour
cela des écoles-ateliers sont organisées tous les deux ans dans divers
pays africains.  La première a eu lieu en 2008, la prochaine édition
aura lieu en **Éthiopie** en 2018. Les instructeurs de l’école sont
des chercheurs provenant de différents pays (dont l’Afrique), des post
docs et des doctorants. La participation moyenne est de 30 à 35.

Cette école permet aussi aux jeunes participants africains aux travers
des rencontres qu’ils ont avec leurs instructeurs de trouver des
opportunités de master et de thèses. Sur la photo suivante prise à
ASESMA 2016,
|image27|
on peut voir **Azima Seidu** étudiante ghanéenne (au
centre) auprès du chercheur (et instructeur) Matteo Gatti (à droite).
Elle est ensuite venue effectuer un stage de trois mois auprès de
Matteo Gatti à l’école Polytechnique et est maintenant en thèse en
Finlande.


Elle avait assisté à l’une des réunions de Physique sans
Frontières. Des bourses pour assister aux écoles et des bourses pour
des courts séjour dans des laboratoires peuvent être demandées.

Les écoles ASESMA sont soutenues par :

* **International Union for Pure and Applied Physics (IUPAP)**
* **International Centre for Theoretical Physics (ICTP)**
* **National Institute for Theoretical Physics (NITheP) - South Africa**
* **National Centre of Competence in Research MARVEL**
* **The Thomas Young Centre**
* **US Liaison Committee for IUPAP**
  
Logiciel Quantum ESPRESSO
-------------------------

`http://www.quantum-espresso.org <http://www.quantum-espresso.org/>`__

C’est une initiative ouverte collaborative, ou de nombreux groupes du
monde sont sont impliqués et coordonnés par la
`Quantum ESPRESSO Foundation <http://foundation.quantum-espresso.org/>`__
dont les membres incluent :  la
`Scuola Internazionale Superiore di Studi Avanzati (SISSA) <http://www.sissa.it/>`__,
le `Abdus Salam International Centre for Theoretical Physics (ICTP) <http://www.ictp.trieste.it/>`__,
le `CINECA <http://www.cineca.it/>`__\ National Supercomputing Center ,
l’`École Polytechnique Fédérale de Lausanne <http://theossrv1.epfl.ch/>`__,
l'`University of North Texas <http://cascam.unt.edu/people/mbnardelli.html>`__,
et l’`Oxford University <http://mgiustino.materials.ox.ac.uk/>`__

+---------------+------------------+
|   |image26|   |   |image28|      |
+---------------+------------------+

(source : http://www.quantum-espresso.org/#)



Ce logiciel fait partie des logiciels Open Source, il permet
de faire des calculs sur les structures 
électroniques et de la modélisation de matériaux à l’échelle
nano. Il est basé sur la théorie de la 
fonctionnelle de la densité, des ondes planes et des pseudos
potentiels. 
 
Quantum ESPRESSO has evolved into a distribution of independent and
inter-operable codes in the spirit of an open-source project. The
Quantum ESPRESSO distribution consists of a “historical” core set of
components, and a set of plug-ins that perform more advanced tasks,
plus a number of third- party packages designed to be inter-operable
with the core components. Researchers active in the field of
electronic-structure calculations are encouraged to participate in the
project by contributing their own codes or by implementing their own
ideas into existing codes.

*Matteo Gatti*
 
|image29|

.. |image25| image:: /images/bulletin-2018-1/bulletin-20_1.jpg
.. |image26| image:: /images/bulletin-2018-1/bulletin-20_2.jpg
   :width: 350px
.. |image27| image:: /images/bulletin-2018-1/bulletin-20_3.jpg
.. |image28| image:: /images/bulletin-2018-1/bulletin-20_4.jpg
   :width: 350px
.. |image29| image:: /images/bulletin-2018-1/bulletin-21_1.jpg
   :width: 700px
