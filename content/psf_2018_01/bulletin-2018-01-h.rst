Développement d’un microscope à sonde locale à Beni Mellal (Maroc)
##################################################################

:date: 2018-01-10
:category: Recherche
:tags: technologie frugale


**Par Souad TOUAIRI(a), Jacques COUSTY(b), Ahmed HAMRAOUI(c) et Mustapha MABROUKI(a),**

**a) Sultan Molay Slimane University, Beni Mellal, Maroc**

**b) Société Française de Physique, Paris, France**

**c) Laboratoire de Chimie de la Matière Condensée – Sorbonne Université UMR7574 SU-CNRS-Collège de France, Paris, France**

Les microscopes à sonde locale comme le microscope à effet tunnel
(STM) ou à force atomique (AFM) sont les instruments privilégiés pour
explorer des échantillons à des échelles comprises entre quelques
dizaines de micromètres et l’atome dans des environnements variés tels
l’air, l’ultra vide ou un milieu liquide. En particulier, ils
permettent l’étude d’objets biologiques vivants ou fixés (Cellules,
Bactéries,…) ou encore des molécules biologiques uniques in-situ dans
des milieux physiologiques.  Ces microscopes AFM ou STM produisent des
images reflétant la topographie d’une surface ou d’objets déposés sur
un substrat plan. Des cartographies montrant les variations locales de
certaines propriétés comme la conductivité électrique locale ou
l’élasticité peuvent aussi être obtenues.
 
Schématiquement, ces instruments sont constitués de 3 grandes parties :
une mécanique, une électronique de commande et un ordinateur qui
pilote l’ensemble et assure l’interface homme/machine. Par exemple,
pour le plus simple des microscopes à sonde locale, le STM, la
mécanique permet de balayer la surface de l’échantillon à l’aide d’une
microsonde (pointe métallique) de façon que l’intensité du courant
électronique circulant par effet tunnel reste constante grâce à la
boucle de rétroaction de l’électronique de pilotage (feedback) qui
ajuste la distance pointe/échantillon. La trajectoire de la pointe est
enregistrée par l’ordinateur qui reconstruit une image reproduisant la
topographie de l’échantillon.
 
Pour un STM fonctionnant à l’air, la mécanique du microscope est
relativement simple à fabriquer à l’aide de composants standards (vis
micrométriques, moteur, céramique piézoélectrique) et de pièces
usinées avec des machines-outils usuelles (tour, fraiseuse). Pour
l’électronique de commande, la diffusion de microcontrôleurs bon
marché (Arduino, Raspberry…) rend possible la construction d’un STM
par des étudiants pour un coût modeste. De plus, une telle
construction présente plusieurs avantages :

1. elle constitue une activité pédagogique très riche car les
   étudiants mettent en pratique des compétences variées : conception
   mécanique, maîtrise des vibrations, électronique à bas niveau de
   bruit, maîtrise de céramique piézoélectrique, fabrication de
   pointes, logiciel de pilotage de l’asservissement, logiciel de
   représentation graphique, traitement de données… ;
2. elle facilite la maintenance puisque la panne peut être
   identifiée et réparée localement. Par contre, la durée de
   fabrication dépend de nombreux paramètres comme les délais
   d’approvisionnement…

L’université de Beni Mellal s’est engagée dans la fabrication d’un STM
qui dérive d’un microscope fabriqué par D. Berard (McGill Canada). La
mécanique qui est en cours de conception comprend un moteur pas à pas
pour assurer une approche grossière fiable. Le système qui assure les
déplacements de la pointe est un disque bimorphe constitué d’une
couche de céramique piézo- électrique déposée sur un disque
métallique. Usiné de façon judicieuse, ce dispositif peut déplacer la
pointe du microscope dans un volume de l’ordre de quelques µm3 pour
des tensions appliquées qui ne dépassent pas 15V. Les mesures précises
des déplacements de la céramique piézoélectrique sont en cours.
