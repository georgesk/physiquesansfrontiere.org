L’initiative Afrique: ICTP /UNESCO/ IUCr workshop
#################################################

:date: 2018-01-10
:category: Enseignement
:tags: laboratoire, optique, conférences, Afrique

et installation d’un laboratoire de diffraction en Côte d’Ivoire
================================================================

**Claude Lecomte CRM2 Université de Lorraine.**

Les actions les plus importantes pour l’initiative Afrique depuis
janvier 2017 ont été 

1. **COURS DE CRISTALLOGRAPHIE** par Claude  Lecomte à l'Université de
   **Cotonou** (Bénin 8-14 décembre) et préparation d’un Open Lab pour
   décembre 2018, 40 participants, en majorité
   des étudiants de master et doctorants, Claude Lecomte a été un des
   conseillers et membre du jury de la thèse de Marielle. Agbahoungbata
   (lauréate internationale de ma thèse en 180 secondes ).
   
2. **ICTP UNESCO IUCR workshop Ziguinchor (Sénégal)** : Ce workshop, intitulé
   **Laboratory and Synchrotron X-ray Crystallography: Applications to Emerging Countries**,
   a eu lieu à l’université Assane SECK de Ziguinchor, Sénégal du 22
   novembre au 2 décembre 2017 et a rassemblé 40 étudiants du Sénégal
   et d’Afrique Subsaharienne en provenance des pays suivants : Bénin,
   Burkina Faso, Cameroun, Cote d'Ivoire, Congo Brazzaville, République
   Démocratique du Congo (DRC), Gabon, Madagascar, Nigéria, Soudan
   et Sénégal. 

  |image1|

  Monsieur Cheikh Kanté, Ministre chargé du plan for « Emerging Senegal
  (PES ) » est venu visiter ce workshop; l’ensemble des cours a été en
  anglais par suite du patronage de l’ICTP ce qui a été un problème pour
  la majorité des étudiants. Le cours a été centré sur la
  cristallographie et la résolution de structures de poudres ou de
  monocristaux et sur l’utilisation du synchrotron en vue de préparer
  les jeunes au futur synchrotron africain.

3. **INSTALLATION D’UN LABORATOIRE DE DIFFRACTION EN COTE D’IVOIRE**
   En janvier ont été installés à l’Université Houphoet Boigny  d’Abidjan
   deux diffractomètres de deuxième main entièrement rénovés et équipés
   de basse température donnés par la société Bruker. 
   |image2|
   Ce service de diffraction géré et dirigé par le professeur Abodou
   Tenon sera un service national et pour l’Afrique de l’Ouest ; l’IUCr,
   l’UNESCO ont participé au financement conjointement à l’université.
   L’inauguration suivie d’un Open Lab utilisant ces appareils aura lieu du
   22 mars au 2 avril 2018.

----------------
   
PROGRAMME DE: CRYSTALLOGRAPHY AND DEVELOPMENT : WHAT IMPACT FOR AFRICA ?
========================================================================

+-----------+---------------------------+-------------------------------------+
| **time**  | **Invited lecturer**      | **Title**                           |
+-----------+---------------------------+-------------------------------------+
| 15h-16h   | Dan Shechtman Nobel prize | Quasi-Periodic Materials :          |
|           | in Chemistry              | a paradigm shift in crystallography |
+-----------+---------------------------+-------------------------------------+
| 15h15-17h | Alesia BACCHI             | Molecules and Models : The role of  |
|           |                           | crystallography in chemical vision  |
|           |                           | and education                       |
+-----------+---------------------------+-------------------------------------+
| 17h-17h15 | Coffe break                                                     |
+-----------+---------------------------+-------------------------------------+
| 17h15-18h | Andreas ROODT             | Expanding diagnostic nuclear        |
|           |                           | medicine : some perspectives        |
+-----------+---------------------------+-------------------------------------+
| 18h-18h45 | Richard GARRAT            | Protein crystallography in Brazil : |
|           |                           | a story of synchrotronand tropical  |
|           |                           | diseases                            |
+-----------+---------------------------+-------------------------------------+


+-------------+----------------------+-----------------------------------------+
| **time**    | **Invited lecturer** | **Title**                               |
+-------------+----------------------+-----------------------------------------+
| 9h-9h45     | Sven LIDN            | Modulated intermetallics                |
+-------------+----------------------+-----------------------------------------+
| 9h45-10h30  | Delia A. HAYNES      | Meeting of African Crystallography      |
|             |                      | Association                             |
+-------------+----------------------+-----------------------------------------+
| 10h30-11h   | Coffe break                                                    |
+-------------+----------------------+-----------------------------------------+
| 10h-11h45   | Hervé CAILLEAU       | Real-time Ultrafast Crystallography     |
+-------------+----------------------+-----------------------------------------+
| 12H30-14H   | **Lunch**                                                      |
+-------------+----------------------+-----------------------------------------+
| 14h-14h45   | Eugène MÉGNASSAN     | RX Structural Information and           |
|             |                      | conception of Molecules                 |
+-------------+----------------------+-----------------------------------------+
| 14h45-15h30 | Sylvain RAVY         | Crystallography and solid state physics |
+-------------+----------------------+-----------------------------------------+
| 15h30-16h15 | Simon CONNELL        | African Light Source                    |
+-------------+----------------------+-----------------------------------------+
| 16H15-16h45 | Coffe break          |                                         |
+-------------+----------------------+-----------------------------------------+
| 17h-17h30   | Martiale Zebaze KANA | Overview of Materials characterization  |
|             |                      | with emphasis on thin films             |
+-------------+----------------------+-----------------------------------------+
| 17h30-18h   | Claude LECOMTE       | Electron density studies : an overview  |
+-------------+----------------------+-----------------------------------------+
| 18h-18h30   | Michele ZEMA         | In situ and in operado crystallography  |
+-------------+----------------------+-----------------------------------------+
| 18h30-19h   | Bruker               | Development of Instrumentation in       |
|             |                      | Africa                                  |
+-------------+----------------------+-----------------------------------------+
 

.. |image1| image:: /images/bulletin-2018-1/bulletin-3_1.jpg
   :width: 680px
.. |image2| image:: /images/bulletin-2018-1/bulletin-4_1.jpg
   :width: 550px
