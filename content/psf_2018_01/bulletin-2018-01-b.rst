APSA Challenge Physique expérimentale Afrique
#############################################

:date: 2018-01-10
:category: Concours
:tags: technologie frugale, arduino

**Odette Fokapu-Bouquet**

Les incubateurs de start-up technologiques, les techno-parcs, les
communautés de hackers se multiplient en Afrique. Cette frénésie
montre qu’il est temps d’envisager sérieusement de fabriquer
localement des instruments dédiés à l’enseignement des sciences
expérimentales dans les universités africaines.

Du fait des coûts souvent très élevés, très peu d’universités en
Afrique disposent d’instruments pour les travaux pratiques de
physiques et pour la recherche en physique.  Il en découle une
formation très théorique inadaptée au marché du travail et un déficit
d’ingénieurs ou de techniciens qualifiés dans de nombreux secteurs
professionnels.

L’APSA (Association Pour la Promotion Scientifique de l’Afrique) est
une association française dont l’objectif est de soutenir le
développement de la science et de la technologie en Afrique. Elle a
organisé une compétition intitulée « APSA Challenge Physique
Expérimentale Afrique » dans le but de stimuler chez les jeunes
scientifiques africains, la volonté de fabriquer eux-mêmes les
instruments. Il sera ainsi possible de baisser les coûts des
dispositifs expérimentaux car plus de frais de douane. La maintenance
des appareils sera également plus facile.

Cette compétition a débuté le 3 janvier 2017 et s’est déroulée en 3
étapes. La première étape a permis de sélectionner 10 projets sur 17 à
partir de fichiers numériques déposés sur le site du concours et
décrivant les prototypes proposés par les candidats. Les deux autres
étapes se sont déroulées à Yaoundé au Cameroun dans les locaux de la
société Sci-Tec-Service, partenaire locale du concours (figure 1). Les
candidats pré-sélectionnés ont reçu une formation « arduino » et ont
réalisé des « mini maquettes » à partir des connaissances acquises
lors de la formation. L’évaluation des « mini réalisations » a permis
de retenir 4 finalistes. Avec l’aide logistique de la plate forme
Sci-Tech- Service les 4 candidats ont finalisés leurs prototypes. Les
différentes évaluations ont été menées par un jury composé de
physiciens camerounais et européens. Le classement des lauréats
(figure 2) avec une remise de prix a eu lieu le 8 décembre 2017.

Cette première édition, une première en Afrique a montré la
faisabilité de l’idée et l’intérêt des jeunes chercheurs pour ce type
de challenge. La plate-forme qui a permis le succès du concours est un
concept qui devrait inspirer d’autres pays africains.

*Site :* `concoursphysiqueafrique.org <mailto:contact@concoursphysiqueafrique.org>`__
 
+---------------------------------+---------------------------------------+
|                                 | SCI-TECH-SERVICE                      |
|                                 | Plate-forme Technologique pour les    |
|                                 | sciences expérimentales  et           |
|                                 | l’innovation :                        |
|                                 |                                       |
|                                 | 4 laboratoires équipés, des           |
| |image4|                        | hébergements pour scientifiques       |
|                                 | étrangers.                            |
|                                 |                                       |
|                                 | **Siège Social** : Yaoundé BP : 8210, |
|                                 | Tél. :00237 99 67 52 04               |
+---------------------------------+---------------------------------------+
| |image6|                        | |image7|                              |
|                                 |                                       |
| *Formation des candidats*       | *Évaluation d’une de porteuse de*     |
| *présélectionnés*               | *projet sur sa réalisation*           |
| *lors de l’étape 2 du concours* | *avec une carte arduino*              |
+---------------------------------+---------------------------------------+
|                               Figure1                                   |
+---------------------------------+---------------------------------------+

+-----------+-------------------------------------------------+
| |image8|  | 1er prix, Banc didactique                       |
|           | Coût de réalisation : 55 euros                  |
|           |                                                 |
|           | Kevin KENZA ZANA                                |
|           |                                                 |
|           | Valise transportable simple d’utilisation offre |
|           | une large gamme de TP d´électronique            |
|           | analogique avec une approche pédagogique        |
|           | évolutive.                                      |
|           |                                                 |
|           | Composition :  Composants , GBF, oscilloscope,  |
|           | appareils de mesure (ohmmètre, voltmètre),      |
|           | capacimètre, fréquencemètre)                    |
|           |                                                 |
+-----------+-------------------------------------------------+
|           Figure 2                                          |
+-----------+-------------------------------------------------+

+-------------------------------------------------------------------------+
| **2ème prix : Physicistlab**: Coût de réalisation : 50 euros            |
|                                                                         |
| Beranger NYNGA NINI                                                     |
+-------------------------------------------------------------------------+
|         |image9|                                                        |
+-------------------------------------------------------------------------+
| Dispositif pour l’enseignement et la recherche : Générateur de signaux, |
| appareil de mesure, carte arduino interface E/S pour le traitement      |
| de données expérimentales. Permet de brancher différents types          |
| de capteurs  (température,   humidité…).                                |
+-------------------------------------------------------------------------+

+-----------+-------------------------------------------+
| |image12| | 3ème prix : Générateurs de signaux        |
|           | spéciaux                                  |
|           |                                           |
|           | Coût de réalisation : 50 euros            |
|           |                                           |
|           | Ulrich SIMO DOMGUIA et Raoul THEPI SIEWE. |
+-----------+                                           +
| |image10| | À partir d’équations différentielles      |
|           | décrivant le comportement de              |
|           | systèmes dynamiques non linéaires         |
|           | et d’une carte Arduino, on peut           |
|           | élaborer des signaux pulsés,              |
|           | paquets de signaux périodiques et         |
|           | signaux chaotiques.                       |
+-----------+-------------------------------------------+
|           Figure 2                                    |
+-----------+-------------------------------------------+

.. |image3| image:: /images/bulletin-2018-1/bulletin-5_1.png
.. |image4| image:: /images/bulletin-2018-1/bulletin-5_2.jpg
    :width: 300px
.. |image5| image:: /images/bulletin-2018-1/bulletin-5_3.png
.. |image6| image:: /images/bulletin-2018-1/bulletin-5_4.jpg
    :width: 300px
.. |image7| image:: /images/bulletin-2018-1/bulletin-5_5.jpg
    :width: 300px
.. |image8| image:: /images/bulletin-2018-1/bulletin-6_1.jpg
    :width: 300px
.. |image9| image:: /images/bulletin-2018-1/bulletin-6_2.jpg
.. |image10| image:: /images/bulletin-2018-1/bulletin-6_3.jpg
    :width: 300px
.. |image11| image:: /images/bulletin-2018-1/bulletin-6_4.png
.. |image12| image:: /images/bulletin-2018-1/bulletin-6_5.jpg
    :width: 300px
.. |image13| image:: /images/bulletin-2018-1/bulletin-6_6.png
.. |image14| image:: /images/bulletin-2018-1/bulletin-6_7.png
