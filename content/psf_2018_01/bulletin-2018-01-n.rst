NanoMada
########

:date: 2018-01-10
:category: Enseignement
:tags: jumelages

**Robert Baptist**

Créé dans une optique d’accompagnement aux pays en développement, le
consortium grenoblois MINATEC Nanolab (CEA/MINATEC, CIME Nanotech, et
40-30), participe au projet de création d’un laboratoire de micro et
nano-technologies à Madagascar. Dans ce cadre, une école de
nano-sciences, NanoMada 2018, est organisée à Antananarivo du
**5 au 10 Avril 2018**, à destination de 50 étudiants en master et doctorat
sur les énergies nouvelles et les nanomatériaux. L’école est suivie
d’un colloque international de deux jours sur les micro et
nano-technologies et leurs applications à la santé, au photovoltaïque
et aux matériaux, c’est-à-dire aux thèmes de recherche du futur
laboratoire malgache. Au programme de l’école
(http://www.instn.mg/nanomada2017/) des cours de base et des travaux
pratiques dispensés par des chercheurs du CEA à Grenoble avec le
soutien de l’UGA *via* l’IDEX et sous le parrainage de
l’association Puya Internationale. Les sujets abordés vont des
cellules solaires aux batteries, en passant par la synthèse de
nanoparticules, la nano caractérisation et la diffraction de RX (ce
dernier cours étant assuré par le Professeur. Alain Gibaud de
l’Institut des Molécules et Matériaux du Mans).La part dédiée à
l’instrumentation à bas coût est très importante et concerne tant les
mesures de caractéristiques des panneaux solaires (divers montages à
base de microcontrôleurs) que la fabrication sur place de batteries et
de super capacités ainsi que la mesure de leurs caractéristiques de
charge, décharge et mise en défaut. Notons enfin que des cours de
nano-sciences sont aussi donnés par des collègues d’universités
marocaines et malgaches.  Cette école participe à la formation pour la
mise en valeur des ressources naturelles et ressources humaines de
Madagascar et devrait se répéter en 2020 avec, en plus, des cours/TP
sur les plantes naturelles et l’extraction de leurs principes actifs
suivie de leur caractérisation par chromatographie liquide haute
pression couplée à la spectrométrie de masse (HPLC-MS).
