Améliorer l’enseignement de la physique à l’Université au Mali
##############################################################

:date: 2018-01-10
:category: Enseignement
:tags: 


**Mahamadou Seydou (Université)**

CONTEXTE GÉNÉRAL
================

Du 15 au 30 juin 2017, j’ai été invité à la faculté des sciences et
techniques de l’Université de Bamako pour enseigner un cours
d’initiation à la modélisation des matériaux au niveau du master 2 et
à aider à monter des équipes de recherche dans le domaine de la
modélisation. Le financement de cette mission est assuré par le projet
TOKTEN (Transfert des connaissances à travers les nationaux
Expatriés), financé par la banque mondiale et le gouvernement du
Mali. Au cours de mon séjour à Bamako, j’ai essayé de faire une
évaluation des besoins au niveau des départements de Physique, chimie
et biologie de la faculté des Sciences et Techniques (FST) de
l’Université des Sciences, des Techniques et des Technologies de
Bamako (USTTB). Après un aperçu de la situation de l’enseignement
supérieur au Mali, les besoins spécifiques des départements de
physique de la FST et du LISA sont listés.

L’enseignement supérieur au Mali se passe dans cinq universités
publiques (une à Ségou et quatre à Bamako), des grandes écoles
publiques et plusieurs écoles privées. Les universités publiques sont
: L’Université des Sciences, des Techniques et des Technologies de
Bamako (USTTB), l’Université des Sciences Sociales et de Gestion de
Bamako (USSGB), l’Université des Lettres et Sciences Humaines de
Bamako (ULSHB), l’Université des Sciences Juridiques et Politiques de
Bamako (USJPB), l’Université de Ségou.

Les effectifs de l’enseignement supérieur ont connu une croissance
exponentielle sans commune mesure avec les capacités de l’État de
financer ce secteur de façon adéquate.  En 2014, les cinq universités
publiques totalisent un effectif de 77 884 étudiants pour environ 1000
enseignants chercheurs dont la grande majorité titulaire de master 2.
Les inégalités filles/garçons s’accentuent encore plus dans les
niveaux supérieurs. Dans les universités publiques de Bamako, les
filles ne représentent que 28 % de l’effectif des étudiants.  Il y a
très peu d’étudiants dans les filières scientifiques et
technologiques. En 2011, 30% des étudiants sont enregistrés en droit ;
5% en géographie ; 3% en médecine ; 2% en socio-anthropologie ; 0,2%
en physique appliquée ; 0,2% en agronomie ; 0,02% en sciences
biologiques ; 0,01% en mathématiques- physique. Le sous-secteur est
également confronté à un certain nombre de défis particulièrement
importants au nombre desquels figurent :

l’insuffisance du nombre d’enseignants permanents, la vétusté des
équipements et infrastructures pédagogiques, la non-maîtrise des
effectifs d’étudiants (en particulier dans les disciplines
littéraires), les problèmes de financement, l’absence de bibliothèque
universitaire de référence et des publications scientifiques locales,
le manque d'accès aux publications scientifiques internationales, le
faible nombre d’enseignants de rang magistral (dit « de rang A ») sur
qui repose la responsabilité académique des cours magistraux, la
direction de thèses, ainsi que l’encadrement de jeunes chercheurs.

LA RECHERCHE SCIENTIFIQUE
=========================

Le système national de recherche en sciences est constitué de
structures universitaires que sont les facultés, instituts et écoles à
travers les Départements d’Enseignement et de Recherche (DER). Il y a
également des instituts nationaux de recherche : Centre National de la
Recherche Scientifique et Technologique (CNRST), Comité National de la
Recherche Agricole ; Institut d’Économie Rurale (IER) ; Laboratoire
Central Vétérinaire (LCV) ; Institut National de Recherche en Santé
Publique (INRSP) ; Centre National de Recherche et d’Expérimentation
pour le Bâtiment et les Travaux Publics (CNREX - BTP) ; Centre
National de l’Énergie Solaire et des Énergies Renouvelables
(CNESOLER).

La faiblesse (en quantité et en qualité) des ressources humaines qui a
pour conséquence une formation doctorale limitée, le sous-équipement,
la faible appropriation des résultats par les utilisateurs, la faible
mobilisation financière, le manque de relation entre le secteur privé
et les structures de recherche sont autant de préoccupations qui
entravent le développement de la recherche scientifique au Mali.

L’Université des sciences des techniques et des technologies comprend
la Faculté des Sciences et Techniques (FST), la Faculté de Pharmacie
(FAPH), la Faculté de Médecine et d’Odontostomatologie (FMOS) et
l’Institut des Sciences Appliquées (ISA). À l’USTTB le pourcentage
d’étudiants sur l’effectif total des étudiants dans les universités
publiques du Mali est passé de 14% en 2013 à 12% en 2014.

À LA FACULTÉ DES SCIENCES ET TECHNIQUES
=======================================

La Faculté des sciences et techniques est localisée sur la colline de
Badalabougou sur une superficie de près de 9 ha. Elle dispose d’un
amphithéâtre de 550 places, 9 salles de 150 à 200 places, 40 salles de
plus de 50 places, 28 laboratoires de recherche, 14 salles de TP et
d’autres salles spécialisées, accueille chaque année plus de 4 000
étudiants. Les missions qui lui sont confiées sont la formation
supérieure générale pratique et spécialisée, la recherche et la
promotion de la recherche scientifique et technologique, la formation
professionnalisée, la formation post-universitaire, la préparation aux
grandes écoles, le développement et la diffusion de la culture et des
connaissances, la réalisation d’expertises dans le domaine des
sciences et techniques. Cinq départements sont chargés d’assurer ces
missions spécifiques. Ce sont : les départements de Biologie, de
Chimie, de Géologie, de Mathématiques et d'Informatique et de
Physique.

J’ai focalisé mes activités au niveau du département de Physique que
je connais bien et qui manque cruellement de ressources humaines. Ce
département ne dispose que de deux (2) professeurs, sept
(7) Maîtres -Assistants et douze (12) Assistants (niveau DEA) pour la
plus part en formation doctorale financée par le gouvernement malien
et ses partenaires.

DIFFICULTÉS ET BESOINS EN PHYSIQUE
==================================

Au niveau du département de Physique, les besoins immédiats sont les
matériels de TP pour l’enseignement. Il s’agit essentiellement de
générateurs à basse tension, d’oscilloscopes, de sources lasers
monochromatiques et de dispositifs électroniques et optiques.

*C’est ainsi que le don des oscilloscopes et GBF par l’IUT de Cachan
(voir photo ci-dessous) a été apprécié à sa juste valeur par les plus
hautes autorités de la faculté (le doyen, le chef de département de
physique). D’autres actions sont en cours, parmi lesquels on peut
citer l’offre d’une quinzaine d’oscilloscopes par l’IUT de Marseille
facilité par le groupe physique sans frontière.*

Au niveau de la recherche, un laboratoire de Modélisation et
simulations numériques (CCMS) est financé par le ministère et commence
à fonctionner. Plusieurs sujets d’intérêt pour le mali ont été
identifiés et des jeunes chercheurs sont mobilisés en fonction de leur
formation de base pour animer les thématiques allant du
photovoltaïque, au traitement des déchets de la teinture (très utilisé
au Mali dans le textile), la pollution atmosphérique aux problèmes des
écoulements et de l’ensablement.

+-------------------+-------------------+-------------------+
|    |image22|      |    |image23|      |     |image24|     |
+-------------------+-------------------+-------------------+
 
Don d’oscilloscopes et GBF par l’IUT de Cachan (à gauche), cours aux
étudiants de master physique (au centre), formation des formateurs au
centre de calcul en août 2016 (à droite).

Cependant un besoin important en formation sur les méthodes
numériques, l’utilisation de logiciels libres, la maintenance du
matériel informatiques, et l’accès aux serveurs de calculs plus
importants ainsi qu’aux revues internationales.

Un deuxième laboratoire expérimental qui s’intéresse à la physique de
l’atmosphère, le changement climatique, la détection de polluants
atmosphériques est fonctionnel. Ce laboratoire a bénéficié de
financement suédois (à travers l’ICTP et le programme sous régional «
changement climatique ») et du ministère de l’enseignement supérieur
du Mali. Ils semblent disposer de matériels spectroscopiques de bon
niveau. Mais les enseignants chercheurs ne savent pas les utiliser. Il
y a de sérieux problèmes pour l’acquisition des données, leur
interprétation. Une formation est nécessaire en spectroscopie
théorique, en acquisition de donnée (logiciels d’interfaçage libres ou
payant comme LabVIEW, Matlab, etc.). Dans ce cadre les membres de la
commission spécialistes de ces domaines sont fortement interpellés.

Le besoin du l’ISA est essentiellement dans le cadre de la mise en
place de formations en adéquation avec le marché de l’emploi.

Par exemple, La mise en place d’une formation dans le domaine de la
maintenance de matériel biomédical est aujourd’hui, une grande
préoccupation exprimée par les responsables de cette structure. Il y a
un besoin pressant d’enseignants dans le domaine de la maintenance des
grands appareils (scanner, radio, …)

Des matériels de TP pour une meilleure compréhension des dispositifs
optiques et électroniques modernes. Il me semble que dans ce domaine
également la commission Physique sans Frontière pourrait intervenir
dans la formation des formateurs et la mise en place de travaux
pratiques à distance sur la maintenance des appareils électroniques
lourds. Il semble que l’institut dispose de moyens pour financer des
séjours pouvant aller jusqu’à deux semaines pour des enseignants
étranger compétents qui veulent participer à des ateliers pédagogiques
pour élaborer des formations, l’aide à la mise en place d’unités
d’enseignement et la formation des formateurs. Il faut noter que cet
institut a pour mission d’assurer la formation continue des agents de
l’état des sociétés privées qui s’investissent beaucoup
financièrement. Les membres qui seraient intéressés et compétents dans
ce domaine peuvent prendre en contact avec les différents responsables
de l’institut.

.. |image22| image:: /images/bulletin-2018-1/bulletin-17_1.jpg
   :width: 220px
.. |image23| image:: /images/bulletin-2018-1/bulletin-17_2.jpg
   :width: 220px
.. |image24| image:: /images/bulletin-2018-1/bulletin-17_3.jpg
   :width: 220px
