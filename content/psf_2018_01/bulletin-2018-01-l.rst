Amérique latine : rapport sur l’école Optoandina 2017
#####################################################

:date: 2018-01-10
:category: Enseignement
:tags: technologie frugale

**par François Piuzzi**

|image30|
En préambule, je voudrais revenir sur l’affiche de l'École
**Optoandina** d’optique et photonique (voir ci-dessous) qui s’est
déroulée du 13 au 17 novembre à la Pontificia Universidad Catolica del
Peru (PUCP) à Lima. L’image de l’affiche est une représentation
artistique de l’analyse d’une pièce archéologique par la méthode
**LIBS** (Laser Induced Breakdown Spectroscopy), un masque mortuaire
de la civilisation Moche. C’est une activité de recherche développée à
la PUCP grâce et sous l’impulsion de l’association **Puya de Raimondi**.

Cela a débuté en **2005** lorsque nous avons organisé la
première conférence **SPECTRA** ainsi que **l'École andine de
spectroscopie** pour les étudiants latino- américains. J’avais reçu un
don d’un laser Nd-Yag de la part du professeur **Majed Chergui**
(EPFL Lausanne) que j’avais emmené à Lima en deux valises et mis en
route le lendemain de mon arrivée.  C’était le premier laser
impulsionnel à avoir été installé au Pérou. Il faut remarquer que 13
ans après, le laser fonctionne toujours, même si maintenant il n’est
uniquement utilisé que pour les travaux pratiques des étudiants du
département de physique.

L’enthousiasme et la compétence de deux professeurs assistants
(**Kevin Contreras et Miguel Asmath**) a fait le reste et cette
activité s’est développée. C’est un exemple qui prouve que la
coopération peut se développer quand certaines conditions sont
réunies. Nous avons pu aussi développer cette activité dans une autre
université ; la Universidad Nacional de Ingenieria et impulsé la thèse
d’un jeune étudiant **José Diaz** (accueilli au CEA Saclay) qui est
maintenant professeur.
 
Cette école s’est déroulée sous la forme de **conférences** données
le matin, par des chercheurs provenant de différents pays latino
américains et d’Europe (France,Espagne et Danemark) et sous la forme
d’**ateliers,** l’après midi pour les étudiants provenant de
différents pays d’Amérique latine.

Les domaines concernés étaient l’optique fondamentale, la
spectroscopie, l’utilisation des lasers, l’ingénierie optique, la
photonique, la métrologie, les instruments scientifiques à coût
soutenable ainsi que les applications dans tous ces domaines.

Les participants français étaient nombreux ; le Dr **Kevin Contreras**
Opticien (Valéo et Puya de Raimondi) responsable du projet
"Microscope sans lentille" (Association Puya de Raimondi), le
Professeur **René Farcy** (Paris XI) : Application des lasers
télémétrie laser et canne électronique pour aveugles (liaison avec
associations péruvienne et colombienne d'aveugles), le Dr
**Maxime Jacquot** CNRS Laboratoire Femto (CNRS Besançon), Holographie,
l’ingénieur opticien **Mejdi Nciri** (Institut Optique Graduate
School) Application de l'imagerie multi-spectrale à la santé, le Dr
**Ismael Moya** chercheur CNRS Fluorescence de la végétation, le Dr
**François Piuzzi** (Société française de Physique - Physico Chimiste
et Puya de Raimondi) Instrumentation scientifique à coût soutenable.

Tous ces intervenants ont proposé et réalisé des ateliers. Par
ailleurs nous avons été invités par le professeur **Mirko Zimic** à
visiter son laboratoire de biologie à la Universidad Peruana Cayetano
Heredia. Le personnel de son laboratoire est très nombreux (30
personnes), cependant le matériel de laboratoire est réalisé en
interne par des méthodes « Do It Yourself (DIY) ». Nous avons pu
discuter de ses attentes concernant le projet « Microscope sans
lentille » pour lequel il pense que l’application principale devrait
être la détection de la tuberculose. La tuberculose latente est un des
problèmes sanitaires du Pérou. Nous avons aussi pu dialoguer avec
**Daniel Guerra** (organisateur de SPECTRA 2013) et **Pierre Padilla**
(premier biohacker du Pérou).

Un atelier sur l’instrumentation scientifique à coût soutenable a été
organisé le vendredi après-midi à la faculté des sciences de la UNI à
la demande de **Walter Estrada** vice-recteur avec la présence
d’environ vingt étudiants et du professeur **José Diaz**. L’une des
activités était le montage d’un microscope à partir de l’hybridation
d’une web cam dont la lentille est inversée et une partie mécanique
d’un lecteur de CD-ROM (voir photo ci-dessous).

Cette école est aussi l’occasion pour les chercheurs de créer des
liens, renforcer des réseaux et pour les étudiants de trouver des
opportunités de doctorats et de créer leurs réseaux.

|image31|


.. |image30| image:: /images/bulletin-2018-1/bulletin-22_1.jpg
   :width: 700px
.. |image31| image:: /images/bulletin-2018-1/bulletin-23_1.jpg
   :width: 700px
