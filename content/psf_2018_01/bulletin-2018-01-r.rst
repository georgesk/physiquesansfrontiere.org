Un oscillo quatre voies, pour une poignée de Roupies
####################################################

:date: 2018-01-10
:category: Enseignement
:tags: technologie frugale

**Georges KHAZNADAR** `(georges.khaznadar@ac-lille.fr) <mailto:georges.khaznadar@ac-lille.fr>`__ 

|image37|
Depuis maintenant une quinzaine d’années, **Ajith Kumar**, ingénieur 
de recherche à **l’Inter-University Accelerator Centre**\ [#f1]_, à New Delhi, 
développe des matériels pour l’enseignement des sciences, dans 
l’esprit de la *technologie frugale*.

Le projet **P**\ hysics with **Ho**\ me-made **E**\ quipment **& I**\
nnovative E\ **x**\ periments, alias **PHOENIX**\ [#f2]_, est l’idée
qu’en utilisant des « moyens du bord », on peut mettre à la portée des
enseignants de sciences des lycées et des universités en Inde, de quoi
organiser des travaux pratiques. Une des motivations d’Ajith Kumar,
c’était de voir les plus brillants élèves de troisième cycle
universitaire, qui entraient pour une thèse dans son institut, et
perdaient leur première année à comprendre comment on monte une
expérience simple de physique ; il a été soutenu sans faille par son
institut dans ce projet. En 2004, un premier gros boîtier avait été
mis au point, que l’on connectait aux ordinateurs par la prise
parallèle pour imprimantes. Avec cela, on échantillonnait des signaux
audio, entre 0 et 5 V, et on disposait de quelques entrées-sorties
numériques : *grosso-modo*, la même chose qu’un ARDUINO\ [#f3]_, qui
naissait à la même époque. *Les concepts ont ensuite divergé.*

Expeyes, Arduino
----------------

En 2018, les projets PHOENIX et ARDUINO ont mûri, ARDUINO a conquis le
monde des bricoleurs, et des laboratoires où on trouve des gens
curieux. La différence entre le dernier-né du projet PHOENIX, nommé
Expeyes-17\ [#f4]_, et l’Arduino, peut être résumée dans un petit
tableau :

+-----------------+-------------------------+------------------------------------+
|                 | **Expeyes-17**          | **Arduino**                        |
+-----------------+-------------------------+------------------------------------+
| **processeur**  | Rapide                  | Rapidités diverses selon le modèle |
|                 |                         |                                    |
+-----------------+-------------------------+------------------------------------+
| **Entrées**     | Quatre, symétriques,    | Cinq, 0-5V ou 0-3,3V,              |
| **analogiques** | multi-calibres,         | résolution 8 bits,                 |
|                 | résolution 12 bits,     | :math:`200×10^3` éch./s            |
|                 | :math:`10^6` éch./s     |                                    |
|                 |                         |                                    |
+-----------------+-------------------------+------------------------------------+
| **Sorties**     | Une dédiée : signaux    | Une ou plusieurs, utilisables      |
| **analogiques** | sinus et triangle, de   | en modulation de largeur (PWM) ;   |
|                 | fréquence ajustable ;   | prévoir des composants             |
|                 | et deux sorties         | externes pour le lissage           |
|                 | programmables par       |                                    |
|                 | table d’onde            |                                    |
|                 |                         |                                    |
+-----------------+-------------------------+------------------------------------+
| **Entrées**     | Deux à quatre,          | Douze (à partager avec les         |
| **numériques**  | selon l’usage.          | sorties numériques)                |
|                 |                         |                                    |
+-----------------+-------------------------+------------------------------------+
| **Sorties**     | Trois, dont une de      | Douze (à partager avec les         |
| **numériques**  | puissance               | entrées numériques)                |
|                 |                         |                                    |
+-----------------+-------------------------+------------------------------------+
| **BUS**         | Pris en charge          | Pris en charge                     |
| :math:`I_2C`    |                         |                                    |
|                 |                         |                                    |
+-----------------+-------------------------+------------------------------------+
| **Source de**   | Une, stabilisée         | À bricoler                         |
| **courant**     |                         |                                    |
|                 |                         |                                    |
+-----------------+-------------------------+------------------------------------+
| **Entrée**      | Une entrée dédiée       |                                    |
| **microphone**  | (microphone à électret) | À bricoler                         |
+-----------------+-------------------------+------------------------------------+
 

À qui s’adresse Expeyes-17 ?
----------------------------

|image38|

Quand on ouvre un kit Expeyes-17, commercialisé actuellement à 4000
roupies\ [#f5]_ (50 €), on trouve un livret de cinquante expériences
scientifiques\ [#f6]_, réalisables avec les accessoires (bobines,
résistance, condensateurs, aimants, LDR, CTN, moteur à courant
continus, aimants permanents, diodes, DELs, phototransistor, buzzers,
fils, pinces crocos). Expeyes-17 a été taillé sur mesure pour
enseigner la physique à l’aide d’expériences réalisables, à des élèves
débutants. La compréhension de tension, courant, loi d’Ohm n’est pas
un prérequis. L’appareil peut être considéré comme un combiné
oscilloscope à quatre voies, plus un générateur de signal basse
fréquence, plus un fréquencemètre.  Les projets avec Arduino supposent
quelques bases de technologie, à moins de pratiquer juste des «
recettes de cuisine ». Par exemple, pour générer une onde sinusoïdale,
il faut recopier un programme dédié et ajouter des composants pour
filtrer les signaux.

Comment s’utilise Expeyes-17 ?
------------------------------

L’auteur travaille avec Ajith Kumar depuis l’année 2005, il est
responsable de la maintenance du paquet expeyes\ [#f7]_ dans les
distributions Debian et Ubuntu. La plupart des ordinateurs dans les
établissements d’enseignement utilisent un système Windows. En cours
de physique, on s’affranchit de plusieurs problèmes de ce système
(lenteurs, virus et anti-virus, gaspillage de ressources machine), en
démarrant l’ordinateur à l’aide d’un DVD- ROM ou d’une clé USB\ [#f8]_, qui
fournit un bureau graphique complet, quelques dizaines d’applications
pédagogiques de haut niveau, dont la suite logicielle Expeyes et de
bons logiciels de traitement de données (tableurs, mais pas
seulement). Actuellement, la distribution logicielle qui accompagne
Expeyes est réactive et agréable à utiliser sur des ordinateurs de dix
ans d’âge, et bien sûr aussi avec les ordinateurs récents. Voici
comment on peut travailler :

1. démarrage du DVD-ROM ou de la clé USB : une minute 
2. lancement de la suite logicielle Expeyes : dix secondes 
3. expériences\ [#f9]_ … traitement des données … rédaction de compte-rendu,
   enregistrement local (sur clé USB, sur disque), ou dans le réseau local
   (partage Windows, ou disque réseau – NAS, ou service dans le cloud). 
4. Fin de session : trente secondes. Dans le cas d’un démarrage par une clé
   USB, les données persistantes sont mises à jour avant l’extinction. 
                              
L’autre grande utilisation est l’enseignement de la programmation :
Expeyes-17 est entièrement piloté en langage Python3, et le manuel
donne la façon de le programmer, avec des exemples.
 
.. rubric:: Footnotes
.. [#f1] Inter-University Accelerator Centre (IUAC) : http://www.iuac.res.in/
.. [#f2] https://expeyes.wordpress.com/phoenix/ The Story of PHOENIX by Kishore A. (2008)
.. [#f3] https://www.arduino.cc/page d'accueil de l’organisation arduino
.. [#f4] https://expeyes.in/ Page d'accueil pour Expeyes
.. [#f5] https://www.fabtolab.com/expeyes Un site de vente par correspondance, prix = 3985 roupies le 6 février 2018, soit 50 euros au cours du jour (sans port ni taxes)
.. [#f6] Le livret est actuellement disponible en anglais et en français, il est sous forme électronique dans le paquet logiciel expeyes : https://packages.debian.org/fr/buster/expeyes
.. [#f7] https://qa.debian.org/developer.php?login=georgesk@debian.org#expeyes page de contrôle-qualité pour les paquets logiciels maintenus par l’auteur : le logiciel expeyes est fonctionnel et tous les rapports de bug à ce jour ont été résolus
.. [#f8] Vous pouvez vous procurer la clé USB développée dans le lycée de l’auteur, par simple téléchargement (il faut une bonne connexion ADSL). Même en l’absence de matériel Expeyes, elle résout de nombreux problèmes d’intérêt pédagogique
.. [#f9] Quand la suite logicielle d’expeyes est en fonction, les cinquante expériences sont chacune accessible par un choix dans les menus, et la partie correspondante du manuel est affichée. Dans le cas où un logiciel spécialisé est nécessaire, celui-ci est lancé à la volée

.. |image37| image:: /images/bulletin-2018-1/bulletin-30_1.jpg
   :width: 480px
.. |image38| image:: /images/bulletin-2018-1/bulletin-31_1.jpg
   :width: 700px
