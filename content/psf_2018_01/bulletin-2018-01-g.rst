Caractérisation de panneaux solaires à l’université de Ventiane au Laos
#######################################################################

:date: 2018-01-10
:category: Enseignement
:tags: 

**Association D2E** `https://d2elaos.com/ <https://d2elaos.com/>`__
(Université Paris Sud -Polytech et IUT de Cachan-).

Contacts :
**Jean Yves Le Chenadec** `jean-yves.lechenadec@u-psud.fr <mailto:jean-yves.lechenadec@u-psud.fr>`__\
et **Pascale Vareille** `pascale.vareille@u-paris-sud <mailto:pascale.vareille@u-paris-sud>`__

Dans le cadre du développement de la Faculté d’Ingénierie de
l’Université Nationale du Laos et la continuité des échanges entre
Paris-Sud et celle-ci, nous avons pour objectif l’installation d’un
laboratoire de recherche d’excellence sur l’étude et la gestion de
smart-grid en utilisant les énergies renouvelables. A terme le système
permettra l’interconnexion de systèmes de production photovoltaïque,
hydraulique et thermique. L’association D2E
(**Développement Énergie Emploi**) qui soutient le développement de
travaux pratiques à l’Université Nationale du Laos à Ventiane. Nous
avons été amenés lors de notre projet de fin d’études à concevoir un
procédé de relevé automatisé de caractéristiques de panneaux solaires
qui nous a permis la mise en place de travaux pratiques lors de notre
stage au Laos.

|image21|

Sa réalisation s’est effectuée en deux parties, la mise au point du
matériel (hardware) durant le projet dans le tutorat à eu lieu l’IUT
de Cachan et la mise au point du logiciel (software) qui s’est
effectuée lors de notre stage de fin d’étude au Laos. principe de
notre projet est de relever plusieurs mesures physiques sur un panneau
solaire à partir de différents capteurs. Avec un micro-contrôleur, le
Nucleo STM-F303K8, nous réalisons l’acquisition de toutes ces données,
l’automatisons, et transférons l’inclinaison, la température,
l’orientation, l’exposition au soleil et la caractéristique
courant-tension du panneau solaire directement sur un fichier Excel. Ce
fichier Excel nous permettra par la suite de définir les conditions
optimales de fonctionnement du panneau solaire au cours d’une journée.
Nous utilisons différents capteurs : • MPU9250 : magnétomètre,
température. L’électrification des villages étant un facteur de
croissance, nous voulons permettre aux chercheurs laotiens d’avoir
accès à des matériels et à des connaissances de pointe grâce à
**la création du premier laboratoire de recherche en génie électrique au Laos**
et grâce à des échanges de travaux pratiques.

+--------------+-------------+
|    |image19| |   |image20| |
+--------------+-------------+

*Carte électronique développée pour l’enregistrement des données et courbe caractéristique d’un panneau solaire.* 


.. |image19| image:: /images/bulletin-2018-1/bulletin-13_1.jpg
   :width: 350px
.. |image20| image:: /images/bulletin-2018-1/bulletin-13_2.jpg
   :width: 350px
.. |image21| image:: /images/bulletin-2018-1/bulletin-13_3.png
   :width: 700px
