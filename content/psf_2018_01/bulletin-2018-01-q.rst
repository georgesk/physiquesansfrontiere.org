Le smartphone: un mini-laboratoire mobile pour l’enseignement des sciences et la recherche dans le monde
########################################################################################################

 
:date: 2018-01-10
:category: Enseignement
:tags: technologie frugale

**Ulysse DELABRE**, `(ulysse.delabre@u-bordeaux.fr) <mailto:ulysse.delabre@u-bordeaux.fr>`__, Université de Bordeaux, Laboratoire LOMA
 
Les smartphones sont omniprésents dans la plupart des pays du monde
avec notamment des prix de plus en plus accessibles (le Freedom 251
coûtait 251 roupies en Inde en 2016 soit moins de 4 euros). S’ils
possèdent un grand nombre de capteurs (accéléromètre, magnétomètre,
capteurs de luminosité, …) pour améliorer leurs ergonomies et leurs
utilisations au quotidien, ces capteurs peuvent être avantageusement
exploités pour réaliser de véritables expériences scientifiques. Grâce
à l’utilisation d’applications gratuites telles que Physics Toolbox
Suite (`https://physics-toolbox- suite.fr.aptoide.com/
<https://physics-toolbox-suite.fr.aptoide.com/>`__) ou Phyphox
(`http://phyphox.org/ <http://phyphox.org/>`__) par exemple, il est
possible d’accéder à ces capteurs et d’enregistrer en temps réel leurs
mesures. A titre d’exemple, en utilisant les accéléromètres des
smartphones, il est possible d’enregistrer l’accélération selon les
trois axes du smartphone en fonction du temps pour réaliser des
expériences en mécanique (oscillation d’un pendule, chute libre,
référentiel tournant, …). Il est également possible d’utiliser les
capteurs de luminosité ou encore la caméra pour réaliser des
expériences en optique (variation de l’éclairement en fonction de la
distance, loi de malus pour la polarisation, …) sans oublier la
possibilité de transformer son smartphone en microscope pour observer
et mesurer des objets à l’échelle micrométrique. Par ailleurs, les
capteurs acoustiques peuvent être détournés pour réaliser des
expériences en acoustique comme par exemple mesurer la vitesse du son
par effet Doppler ou par résonance, en utilisant des applications qui
enregistrent le spectre d’une source sonore. Ceci permet d’envisager
les smartphones comme de véritables laboratoires scientifiques mobiles
pour tous. Cette démarche pédagogique a été pratiquée avec succès
auprès de centaines d’étudiants de l’université de Bordeaux. Très
puissants et de plus en plus sophistiqués, les smartphones peuvent
même être exploités pour réaliser des expériences en recherche
fondamentale collaborative comme il a été suggéré par plusieurs
articles scientifiques récents.  Avec la démocratisation de l’usage
des smartphones, ceux-ci constituent une réelle opportunité pour
faciliter et démocratiser l’enseignement des sciences à travers le
monde.
 
+------------------------------------+-----------------------------------+
|     |image34|                      |         |image36|                 |
+------------------------------------+-----------------------------------+
| Utilisation des accéléromètres du  | Spectre sonore dans un résonateur |
| smartphone pour réaliser des       | d’Helmholtz pour mesurer la       |
| expériences en mécanique. Les      | vitesse du son dans l’air.        |
| accélérations sont enregistrées    |                                   |
| selon x,y,z                        |                                   |
+------------------------------------+-----------------------------------+


.. |image34| image:: /images/bulletin-2018-1/bulletin-29_1.jpg
   :width: 160px
.. |image35| image:: /images/bulletin-2018-1/bulletin-29_2.png
.. |image36| image:: /images/bulletin-2018-1/bulletin-29_3.jpg
   :width: 540px
