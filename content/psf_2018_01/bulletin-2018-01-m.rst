Apprendre à façonner la lumière par holographie numérique aux élèves de France et du Pérou
##########################################################################################

:date: 2018-01-10
:category: Enseignement
:tags: optique

**MAXIME JACQUOT**:math:`^1`, **MIGUEL ASMAD VERGARA**:math:`^{1,2}`

:math:`^1`\ *Institut FEMTO-ST UMR CNRS 6174, Université de Bourgogne Franche-Comté, 25030 Besançon,*

:math:`^2`\ *France2 Sección Física, Departamento de Ciencias, Pontificia Universidad Católica del Perú, Apartado 1761, Lima, Perú.*

`maxime.jacquot@univ-fcomte.fr <mailto:maxime.jacquot@univ-fcomte.fr>`__

CONTEXTE ET OBJECTIFS DE L'EXPÉRIENCE
=====================================

Depuis 2015, et alors que des milliers d’évènements fleurissaient
autour du thème de la lumière à travers le monde avec l’année
internationale de la lumière, un travail a été réalisé dans ce même
cadre à la PUCP\ [#f1]_ à Lima au Pérou, pour monter une expérience
d’holographie numérique. Ce montage est à la base d’un cours de master
enseigné sous une approche par projet à l’UFC\ [#f2]_ (Besançon, France), du
master PICS\ [#f3]_, labellisé CMI\ [#f4]_.

En effet, à travers l’étude expérimentale et numérique d’un banc
d’holographie numérique, il est possible d’aborder des concepts
avancés d’optique physique comme l’interférométrie, la diffraction,
l’optique de Fourier, et le traitement tout optique de
l’information. Les applications de l’holographie couvrent des domaines
très variés comme la microscopie et le biomédical, la robotique, la
sécurisation des données ou encore la mise en forme de faisceaux
laser.

Les étudiants de master de physique appliquée de la PUCP ont bénéficié
de cet enseignement avec un enseignant chercheur de l’Université de
Franche-Comté. Ils ont ainsi réalisé des programmes de simulations
numériques par une approche de spectre angulaire d’ondes planes pour
ensuite les appliquer à diverse configurations du montage expérimental
d’holographie. L’acquisition d’un hologramme est réalisée sur un
capteur 2D CMOS de 5Mp grâce à un interféromètre de type Michelson,
qui est combinée à un modulateur spatial de lumière (SLM) bas
coût. Les objets étudiés sont de taille millimétrique. La restitution
en intensité et en phase de l’image est numérique. On montre ainsi que
ce procédé autorise la mesure de différence de trajets optiques, sans
recours à des lentilles, avec des résolutions atteignant la dizaine de
nanomètres. L’insertion d’un élément adressable (SLM) dans le montage
donne accès à une grande flexibilité à l’expérimentation pour deux
raisons principales : des objets de phase pure modifiable sont générés
grâce au SLM et une correction dynamique du front d’onde peut
s’appliquer. Dans ce contexte, ce banc d’holographie numérique
dynamique constitue un formidable outil pédagogique pour des étudiants
d’un niveau licence de Physique jusqu’à un niveau de fin de
master. Cette étude amène à des enseignements fortement
pluridisciplinaires dispensés sous forme d’un projet intégré. Ce
montage illustre aussi des expériences issues de recherches menées à
FEMTO-ST en holographie numérique [1] et en mise en forme spatiale de
faisceau complexe laser de type Bessel [2] ou accélérant [3].

Ce démonstrateur pédagogique a été réalisé dans le contexte de l’année
internationale de la lumière, dans le cadre du projet francomtois LUX,
et a été présenté lors de manifestations de vulgarisations tout au
long de l’année 2015\ [#f5]_. Le montage d’holographie numérique a aussi été
proposé à des étudiants de différents pays d’Amérique Latine (Mexique,
Pérou, Équateur, Bolivie) lors d’un atelier sur la métrologie optique
de l’école été OPTOANDINA\ [#f6]_ en novembre 2017 à Lima.

RÉFÉRENCES
----------

[1] P. Sandoz and M. Jacquot, "Lensless vision system for in-plane
positioning of a patterned plate with subpixel
resolution," J. Opt. Soc. Am. A 28, 2494-2500 (2011)

[2] L. Froehly, M. Jacquot, P. A. Lacourt, J. M. Dudley,
and F. Courvoisier, "Spatiotemporal structure of femtosecond Bessel
beams from spatial light modulators," J. Opt. Soc. Am. A \ **31**,
790-793 (2014)

[3] F. Courvoisier, A. Mathis, L. Froehly, R. Giust, L. Furfaro, P. A. Lacourt, M. Jacquot,
and J. M. Dudley, "Sending femtosecond pulses in circles: highly
nonparax-ial accelerating beams," Opt. Lett. 37, 1736-1738
(2012)


.. rubric:: Footnotes
.. [#f1] Pontificia Universidad Catholica Peruana (PUCP) - `http://www.pucp.edu.pe/ <http://www.pucp.edu.pe/>`__
.. [#f2] Université de Franche-Comté -  `http://www.univ-fcomte.fr/ <http://www.univ-fcomte.fr/>`__
.. [#f3] Master PICS en physique appliquée à la Photonique, aux micro&nano-technologies, à la métrologie temps fréquence et aux systèmes complexes) – A noter que ce master devient 100 % internationale à la rentrée 2018 avec des bourses d’études allouées pour des étudiants étrangers (ISITE UBFC, EUR EIPHI…) 
.. [#f4] Un CMI (Cursus Master en Ingénierie) est un parcours en 5 ans (Licence + master) renforcé et délivré par un réseau de 28 universités en France, le réseau FIGURE - `http://reseau-figure.fr/ <http://reseau-figure.fr/>`__  -
.. [#f5] `http://www.light2015.org/Home/About/Latest-News/October2015/lux.html <http://www.light2015.org/Home/About/Latest-News/October2015/lux.html>`__ 
.. [#f6] `http://congreso.pucp.edu.pe/optoandina/ <http://congreso.pucp.edu.pe/optoandina/>`__ 
