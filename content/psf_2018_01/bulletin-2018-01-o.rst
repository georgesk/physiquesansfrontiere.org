Mesure des variations de l’altitude de l’ionosphère par traitement numérique de signaux radiofréquences acquis par carte son
############################################################################################################################

:date: 2018-01-10
:category: Enseignement
:tags: technologie frugale

**Jean Michel FRIEDT**

La prolifération des modes de communication sans fil sur porteuse
radiofréquence – courte portée avec les réseaux locaux tels que Wifi
ou longue portée tel que la télévision numérique terrestre – offre une
opportunité́ d’observer l’environnement physique qui régit les
conditions de propagation de ces signaux. En effet, la disponibilité́
d’interfaces d’acquisitions accessibles à faible coût donne accès à
ces signaux, et la capacité́ à les exploiter n’est limitée que par la
capacité́ des auditeurs à extraire toute l’information introduite lors
de la propagation [1]. À titre d’exemple d’interfaces d’acquisition,
nombre de cartes son équipant les ordinateurs personnels permettent un
échantillonnage à 192 kHz, et donc d’analyser les signaux sur des
porteuses allant jusqu’à 96 kHz [2].

Parmi ces signaux, DCF77 émis depuis l’Allemagne porte une information
cadencée par des horloges atomiques et donc compatibles avec une
datation excessivement précise du temps de vol, notamment
représentatif des conditions ionosphériques [3, 4, 5]. De la même
façon, les récepteurs de télévision numériques terrestres basés sur un
convertisseur analogique-numérique RTL2832U, disponibles pour moins
d’une dizaine d’euros, permettent de sonder la gamme radiofréquence
d’une cinquantaine de MHz à plus de 1,6 GHz en sortie de démodulateur,
ou dans la gamme de 0 à 1 MHz en attaquant directement les
convertisseurs analogique-numériques sans passer par l’étage de
transposition de fréquence. En supposant que le signal de
synchronisation issu de récepteurs GPS (1 PPS) fournit une base de
temps de référence, nous étudions les fluctuations journalières à
saisonnières de l’ionosphère, couche de l’atmosphère à une altitude de
60 à 90 km qui réfléchit les ondes électromagnétiques émises en deçà
de 100 kHz. Un traitement logiciel des signaux acquis réduit la
complexité́ matérielle de l’expérience (et donc son coût) tout en
amenant la flexibilité́ et la stabilité́ du traitement numérique de
signaux. Nous avons ainsi démontré́ la capacité́ à mesurer les
variations jour-nuit de l’altitude de l’ionosphère avec une résolution
de 10 µs, et la stabilité́ de l’ionosphère pendant l’été́ pour devenir
instable en hiver. Toute mesure géophysique se doit de durer dans le
temps pour être statistiquement viable : l’utilisation de matériel
facilement disponible et dont la mobilisation n’handicape pas les
autres capacités d’expérimenter rend cette mesure compatible avec une
analyse sur une longue durée. Notre mesure dure ainsi depuis plus d’1
an sans interruption majeure (Fig. 1).

|image32|
 
*Figure 1 – Mesure de Novembre 2016 à fin-Janvier 2018 de la variation de temps de vol du signal 
émis par DCF77, représentatif de la variation d’altitude de l’ionosphère. Noter la stabilisation du 
signal en été́.*

Au-delà̀ de proposer une infrastructure faible coût de dissémination du
temps robuste, car indépendante de GPS, cette expérience permet
d’appréhender la physique des phénomènes électriques dans
l’atmosphère. D’un point de vue ingénierie, la dissémination du temps
impacte des domaines aussi variés que les transports (garantir que
deux trains partent de deux gares distantes à la même heure), datation
des transactions boursières ou toute activité́ sociale nécessitant une
synchronisation. Les compléments à ce premier développement visent à
étendre l’étude à d’autres fréquences (TDF à 162 kHz en France, LORAN
à 100 kHz en Angleterre) tout en observant les diverses directions
d’arrivées de ces signaux vers le récepteur, et donc des zones et
altitudes distinctes de l’ionosphère. L’environnement de traitement du
signal numérique GNU Radio facilite la prise en main des outils
nécessaires à aborder la SDR, sans nécessairement simplifier les
pré́-requis théoriques qui deviennent plus faciles a appréhender avec
les applications ludiques citées ci-dessus. Parmi les manifestations
regroupant les intervenants sur le sujet, chaque année les vidéos des
conférences GRCon \
`https://www.youtube.com/channel/UCceoapZVEDCQ4s8y16M7Fng <https://www.youtube.com/channel/UCceoapZVEDCQ4s8y16M7Fng>`__
et la session radio logicielle du FOSDEM
`https://fosdem.org/2018/schedule/track/software_defined_radio/ <https://fosdem.org/2018/schedule/track/software_defined_radio/>`__
sont mises en ligne pour permettre l’accès à un maximum d’auditeurs
n’ayant pu se rendre physiquement aux conférences.

Références :
------------

[1] T. Lavarenne, *Études expérimentales de différents types de modulations numériques (RFID, RDS et GSM)*, Bull.  Union des Physiciens (2017)

2] J.M. Friedt, *Mesure stroboscopique du champ de déplacement d’un diapason à quartz au moyen d’une carte son et d’une webcam*, Bull. de l’Union des Physiciens 999 (Dec. 2016)

[3] J.-M Friedt, C. Eustache, É. Carry, E. Rubiola, *Software defined radio decoding of DCF77 : time and frequency dissemination with a sound card*, Radio Science (Jan. 2018), à onlinelibrary.wiley.com/doi/10.1002/2017RS006420/ abstract

[4] J.-M. Friedt, C. Eustache, E. Rubiola, *Décodage logiciel du signal de transfert de temps par DCF77 : introduction à la radio définie par logiciel au moyen d’une carte son*, CETSIS 2017

[5] J.-M. Friedt, C. Eustache, E. Rubiola, *Monitoring the ionosphere altitude variation with a sound card : software defined radio processing of DCF-77 signals*, FOSDEM 2017, à https://fosdem.org/2017/schedule/speaker/jean_michel_friedt
et http://jmfriedt.free.fr/dcf77.mp4
 
.. |image32| image:: /images/bulletin-2018-1/bulletin-26_1.jpg
   :width: 700px
