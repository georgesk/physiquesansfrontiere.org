BULLETIN PsF 01/2018
####################

:date: 2018-01-10
:category: Bulletin
:tags: humour
      
Proverbe malien qui s’applique à la commission:
===============================================

**« on ne ramasse pas un caillou avec un seul doigt. »**

|image0|
 
*« Même dans les laboratoires individuels des scientifiques les plus importants, les résultats proviennent d’un effort commun »*

*Even in the individual labs of most leading scientists, the results are invariably born of a joint effort.*

**Illustration: Dominic McKenzie** 

Éditorial
=========

Ce bulletin est consacré à la description succincte de différentes
actions internationales orientées au soutien à l’enseignement de la
physique et de la recherche dans les pays en majorité à faibles
ressources, ainsi qu’à la description de projets d’actions. Sont
également présentés des projets de travaux pratiques, des avancées en
vulgarisation ainsi que des exemples concernant « l’open source » que
ce soit pour les logiciels où c’est maintenant bien établi que pour le
matériel (hardware) où cela commence à arriver.

Des exemples d’instrumentation scientifique à coût soutenable sont
également décrits. Ces actions ou ces projets d’actions reflètent les
contacts que nous avons eus au cours de ces deux dernières années.

Ce n’est bien sûr pas exhaustif et nous vous encourageons à diffuser
ce bulletin (si vous le jugez utile) et à nous faire part d’actions
dont vous êtes au courant pour les faire figurer dans un prochain
bulletin. La difficulté de la solidarité scientifique internationale
est due à la forte dépendance des actions à entreprendre au contexte
local et au fait qu’il n’y a pas de démarche universelle possible.

C’est l’un des buts de notre commission de faire partager les
expériences et si possible de développer des coopérations. Ne
disposant pas de moyens financiers, notre rôle est celui d’un
catalyseur et d’un agitateur d’idées. La diversité thématique et
géographique des actions décrites dans ce document par leurs
initiateurs est très importante. Il faut remarquer qu’il a un nombre
important d’actions impulsées par de petites associations. Cela tient
peut-être à l’importance des liens personnels dans la réussite des
coopérations et à l’importance de la proximité humaine « hiérarchique
» entre les deux côtés de la coopération.

Un des domaines qui me tient à cœur, c’est la
**disponibilité de l’instrumentation scientifique.**
L’initiative emblématique de Claude Lecomte pour la diffusion de la
cristallographie, notamment en Afrique, avec l’organisation de dons de
diffractomètres et l’organisation des Open Lab est à prolonger à
d’autres domaines de la physique. En Europe, des jeunes doctorants et
des jeunes chercheurs développent des alternatives à l’instrumentation
traditionnelle très coûteuse, à partir des développements rendus
possibles par l’explosion du numérique : plate-forme électronique
Arduino, micro-ordinateur Raspberry Pi, impression 3D (extrêmement
important pour les universités où il n’y a pas d’atelier de
mécanique), smartphones, etc, ainsi que par des approches nouvelles ;
« open source », science collaborative. Ce sont par exemple
l’association \ **Trend in Africa**, le \ **Gaudi Lab** et le **CERN**
qui a été à l’origine de l’ « open hardware licence » qui a contribué
à impulser le mouvement **GOSH** (Global Open Source Hardware) ainsi
que les départements de physique des universités de Physique de
Cambridge et Bristol. Cependant, si la disponibilité de
l’instrumentation est nécessaire, elle n’est pas suffisante car il
faut aussi développer une formation aux sciences expérimentales qui
soit ambitieuse et qui ne peut être décidée et mise en place que par
les institutions elles même. Il faut remarquer qu’il n’est pas
illusoire de penser que les développements issus de ces alternatives
pourront profiter à terme à nos pays développés car les moyens donnés
à l’éducation en sciences expérimentales ont tendance à stagner et
même à diminuer.

Grâce au développement du concept « Open Source » le
**partage du savoir** et de **l’intelligence** qu’il soit sous forme
de logiciel (software) ou d’équipements (hardware) est en bonne voie
et est une condition importante de la réduction des inégalités dans
l’appropriation des connaissances.

Il est difficile de faire comprendre aux décideurs l’importance
d’aider les pays aux ressources limitées, cette indifférence que l’on
rencontre également parmi nos collègues chercheurs (pas tous
heureusement !!) ne prend pas en compte la responsabilité historique
des pays européens dans l’évolution de beaucoup de pays en voie de
développement.

**« Le seul remède à l’indifférence c’est la mémoire » Liliana Segre**

Coopérer pour améliorer l’enseignement et la recherche et plus
généralement les conditions de formation est une obligation ainsi que
de bien former des techniciens ingénieurs, professeurs chercheurs
scientifiques qui seront importants pour l’avenir de leurs pays. C’est
aussi un moyen d’éviter au moins partiellement la fuite des cerveaux.,
si des postes adéquats sont créés, en particulier dans l’industrie.

Ce bulletin peut paraître disparate (sa mise en forme l’est en tout
cas !!), mais il cherche à informer (de manière la plus exhaustive
possible) sur les diverses formes de coopérations qui sont engagées,
et c’est aussi l’occasion de partager les expériences et de se
rapprocher des coordinateurs de ces projets pour ceux qui veulent se
renseigner plus avant.

Bonne lecture !!


François Piuzzi 

Les articles du bulletin:
=========================

- `L’initiative Afrique: ICTP /UNESCO/ IUCr workshop </articles/2018/janv./10/bulletin-2018-01-a/>`__
- `APSA Challenge Physique expérimentale Afrique </articles/2018/janv./10/bulletin-2018-01-b/>`__
- `Un Master de Physique en Haïti : atouts et défis </articles/2018/janv./10/bulletin-2018-01-c/>`__
- `Bref compte rendu de visite en République de Guinée </articles/2018/janv./10/bulletin-2018-01-d/>`__
- `État des lieux de la collaboration entre l’équipe CIML du labo PIIM de AMU (Marseille) et le département de physique de l’USTM (Franceville, Gabon) </articles/2018/janv./10/bulletin-2018-01-e/>`__
- `Projet d’échanges et collaborations scientifiques entre les villes de Grenoble et de Sfax (Tunisie) dans le cadre de leur jumelage </articles/2018/janv./10/bulletin-2018-01-f/>`__
- `Caractérisation de panneaux solaires à l’université de Ventiane au Laos </articles/2018/janv./10/bulletin-2018-01-g/>`__
- `Développement d’un microscope à sonde locale à Beni Mellal (Maroc) </articles/2018/janv./10/bulletin-2018-01-h/>`__
- `Améliorer l’enseignement de la physique à l’Université au Mali </articles/2018/janv./10/bulletin-2018-01-i/>`__
- `Éthiopie : Projet d’un concours « Science et Technologie » 2018: Technologies et Développement Durable en Éthiopie </articles/2018/janv./10/bulletin-2018-01-j/>`__
- `Conférences ASESMA et Logiciel Open Source Quantum Expresso </articles/2018/janv./10/bulletin-2018-01-k/>`__
- `Amérique latine : rapport sur l’école Optoandina 2017 </articles/2018/janv./10/bulletin-2018-01-l/>`__
- `Apprendre à façonner la lumière par holographie numérique aux élèves de France et du Pérou </articles/2018/janv./10/bulletin-2018-01-m/>`__
- `NanoMada </articles/2018/janv./10/bulletin-2018-01-n/>`__
- `Mesure des variations de l’altitude de l’ionosphère par traitement numérique de signaux radiofréquences acquis par carte son </articles/2018/janv./10/bulletin-2018-01-o/>`__
- `Groupe de travail « TRAVAUX PRATIQUES » </articles/2018/janv./10/bulletin-2018-01-p/>`__
- `Le smartphone: un mini-laboratoire mobile pour l’enseignement des sciences et la recherche dans le monde </articles/2018/janv./10/bulletin-2018-01-q/>`__
- `Un oscillo quatre voies, pour une poignée de Roupies </articles/2018/janv./10/bulletin-2018-01-r/>`__
- `Vulgarisation : "La Physique Autrement" </articles/2018/janv./10/bulletin-2018-01-s/>`__
- `L'instrumentation à coût soutenable </articles/2018/janv./10/bulletin-2018-01-t/>`__

  
.. |image0| image:: /images/bulletin-2018-1/bulletin-2_1.jpg
   :width: 480px
