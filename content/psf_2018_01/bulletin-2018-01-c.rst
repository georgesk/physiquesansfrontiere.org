 
Un Master de Physique en Haïti : atouts et défis
================================================

:date: 2018-01-10
:category: Enseignement
:tags: 

**Dieuseul PREDELUS, ENS, Université d’État d’Haïti, Port-au-Prince ;**
**Jérôme PACAUD, Institut Pprime (UPR CNRS 3346),Université de Poitiers ;**
**Annick SUZOR-WEINER, Université Paris-Sud.**

Après le terrible tremblement de terre de 2010 qui a dévasté la
plupart des établissements d’enseignement supérieur en Haïti, de
nombreux étudiants ont bénéficié de bourses pour continuer leurs
études en France. Depuis 2015, nombre d’entre eux reviennent, master
ou doctorat en poche, et s'attellent à la reconstruction universitaire,
parfois dans des locaux encore provisoires.

C’est ainsi que plusieurs jeunes physiciens, enseignants à l’École
Normale Supérieure de l’Université d’État d’Haïti, y ont créé un
Master de Physique, parcours Énergie et Environnement. Ce programme
est développé en partenariat avec l’Université de Poitiers, de Lyon
(Lyon 1 et ENTPE), d’Orléans, de Liège et des Antilles. Ses objectifs
sont de trois ordres : formation d’enseignants de physique à tous
niveaux, formation de cadres qualifiés pour les entreprises dans le
domaine environnemental (énergie solaire, dépollution…), création d’un
laboratoire permettant de stabiliser sur place de jeunes chercheurs,
doctorants et post-doctorants, sur des thèmes d’intérêt local et avec
des instruments à coût abordable.

Plusieurs étudiants ont déjà été recrutés dans les entreprises ou
administrations à l’issue du M1. Dix étudiants ont pu faire un stage
de M2, certains en France (Poitiers, Lyon, Pointe-à-Pitre) et quatre
d’entre eux commencent une thèse, en lien avec un laboratoire
français.

Ce partenariat international bénéficie de plusieurs atouts :

- une grande appétence des étudiants pour une formation
  avancée en physique ; 
- surtout, le retour de jeunes docteurs très motivés et ayant conservé
  des liens forts avec des enseignants-chercheurs français, eux-mêmes
  très engagés dans cette collaboration, ainsi que leurs établissements ; 
- une bonne synergie entre les sources de financement, avec concertation
  et complémentarité : État haïtien, Ambassade de France en Haïti,
  Conférence des Présidents d’université (CPU) sur subvention du MESRI,
  Agence universitaire de la Francophonie (AUF). 
- don de livres (IHP, ENS Paris) et de matériel de TP d’électronique pour
  Licence et Master (IUT de Cachan), exemple à suivre ! Le transport vers
  Haïti est assez aisé.

|image18|

Par contre, il doit faire face à de nombreux défis :

- étudiants ayant de bonnes bases mais sans formation expérimentale,
  faute de matériel ; missions d’enseignants, tous bénévoles mais
  impliquant des frais de voyage et de séjour ; le transfert aux
  enseignants locaux se fera progressivement ;
- ouverture d’une École doctorale pour permettre la double inscription
  et la cotutelle, concomitante à la création du laboratoire. C’est en
  cours, avec déjà une convention de cotutelle de thèse signée entre
  l’Université de Poitiers et l’ENS de Port-au-Prince.
- Création d’un laboratoire : LS2E (laboratoire pour les sciences de
  l’Energie et de l’Environnement), avec 3 axes : **Énergies
  alternatives, Pollution environnementale et Réchauffement climatique,
  Valorisation des déchets (économie circulaire)**. Tout est à faire,
  mais les équipes sont constituées et ces thèmes sont choisis pour être
  en prise avec les besoins du pays, et il sera fait appel aux bailleurs
  nationaux et internationaux.

Ce premier Master de physique, déjà suivi par un Master de
mathématiques, ouvre de nouvelles perspectives aux sciences de base en
Haïti, en irriguant l’enseignement mais aussi la recherche et le
développement. Les collaborations internationales sont indispensables
pour soutenir cet effort, et fructueuses pour tous, et d’une grande
richesse humaine.
 
.. |image18| image:: /images/bulletin-2018-1/bulletin-7_1.jpg
   :width: 700 px
