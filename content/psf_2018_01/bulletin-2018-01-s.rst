Vulgarisation : "La Physique Autrement"
#######################################

:date: 2018-01-10
:category: Enseignement
:tags: vulgarisation


**Julien BOBROFF** `(julien.bobroff@u-psud.fr) <mailto:julien.bobroff@u-psud.fr>`__
 
Dans l'équipe "La Physique Autrement", nous développons différents
outils de vulgarisation et d'enseignement de la physique moderne. Pour
cela, nous (des enseignants-chercheurs de l'Université Paris-Sud et du
CNRS) collaborons avec des designers, des illustrateurs et des
vidéastes. Les outils ainsi produits peuvent être de simples petites
animations (par exemple sur la physique quantique :
`www.toutestquantique.fr) <http://www.toutestquantique.fr/>`__, des
expériences (par exemple sur la lévitation supraconductrice), ou
des projets plus originaux à la frontière entre science et
design. Toutes ces productions, qui existent en français et en
anglais, sont libres de droit et peuvent être utilisées par exemple
pour organiser des petites animations ou des ateliers, pour faire des
expositions, ou juste pour le plaisir de découvrir certains aspects de
la physique et des recherches récentes sous un jour nouveau.

Vous pouvez retrouver l'ensemble des projets sur le site
`www.vulgarisation.fr <http://www.vulgarisation.fr/>`__ dans la
rubrique Productions. A titre d'exemple, notre tout dernier projet,
"Atome Lumière Matière" donne à voir la physique atomique et celle de
la matière sous un jour nouveau, à la fois via de petites animations
simples et élégantes, et via des bandes dessinées qui racontent les
recherches actuelles.

Contact : `www.vulgarisation.fr  <http://www.vulgarisation.fr/>`__
Julien Bobroff, Laboratoire de Physique des Solides, Université Paris-Sud
et CNRS 

|image39|

Capture d’écran d’un des thèmes vidéo consacré à
la dualité ondes particules, il est à signaler que les 
textes accompagnant les vidéos peuvent être choisi en version française
ou anglaise. 

.. |image39| image:: /images/bulletin-2018-1/bulletin-32_1.jpg
   :width: 700px
