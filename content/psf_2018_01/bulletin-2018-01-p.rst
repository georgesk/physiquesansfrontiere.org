Groupe de travail « TRAVAUX PRATIQUES »
#######################################

:date: 2018-01-10
:category: Enseignement
:tags: technologie frugale

|image33|
*Sarasvati , déesse indienne de la connaissance tient en main son smartphone, ce qui doit beaucoup l’aider pour la connaissance !*

Réaliser des travaux pratiques pour les sciences expérimentales reste
un défi pour beaucoup d’universités africaines car les instruments
nécessaires sont coûteux et le nombre nécessaire est important car le
nombre d’étudiants est très important, sans parler des locaux.

La commission Physique sans Frontières pense qu’il est nécessaire de
travailler et de réfléchir sur des travaux pratiques plus frugaux et
dont surtout les équipements peuvent être produits localement.

**Un groupe de travail sera créé, vous pouvez vous y joindre**
(contacter `piuzzifr@gmail.com <mailto:piuzzifr@gmail.com>`__).
Cette réflexion aura aussi des répercussions sur nos pays développés
en proposant des TP moins coûteux et plus faciles à mettre en place.


La Physique avec les smartphones et l’application aux travaux pratiques
-----------------------------------------------------------------------

Le smartphone est déjà en lui-même un instrument scientifique, car il
contient un nombre de capteurs et de générateurs de signaux important
et il a les possibilités de traiter les signaux, de les transférer et
de géolocaliser (GPS) les mesures. Son prix est encore rédhibitoire,
mais il commence à se répandre rapidement en Afrique et les anciennes
versions sont moins chères !! On peut extrapoler et penser que d’ici 5
à 10 ans la possession d’un smartphone sera démocratisée.

Il y a cependant d’autres problèmes, comme pour certaines marques la
facilité ou non de le programmer pour traiter les données ou de
connecter des instruments et des capteurs (autres que ceux étant liés
ou développés par la marque). Cependant nous pensons qu’il sera
important pour l’enseignement des sciences, car il permet la
réalisation de mesures et leur traitement.
 
.. |image33| image:: /images/bulletin-2018-1/bulletin-28_1.jpg
   :width: 480px
