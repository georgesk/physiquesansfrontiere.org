
État des lieux de la collaboration entre l’équipe CIML du labo PIIM de AMU (Marseille) et le département de physique de l’USTM (Franceville, Gabon)
###################################################################################################################################################

:date: 2018-01-10
:category: Recherche
:tags: 

**Caroline Champenois, Marie Houssin et Martina Knoop (Aix Marseille Université -CNRS)**

L’Université des Sciences et Techniques de Masuku (USTM) est la seule
université scientifique du Gabon. Toutes les disciplines scientifiques
(Agronomie, Biologie, Chimie, Géologie, Informatique, Mathématiques et
Physique) y sont représentées. Avant 2010, la formation en Physique
s’arrêtait au niveau Licence.

Sollicités par un ancien doctorant de son équipe, enseignant dans le
département de Physique de l'USTM, des membres de l'équipe CIML du
laboratoire PIIM (AMU) a participé à l’élaboration du Programme d’un
Master de Physique et – invitées et soutenues financièrement par les
instances ministérielles du Gabon (PAI) - ont assuré plusieurs
enseignements depuis son ouverture :

- M1, Physique atomique et moléculaire : Caroline Champenois (60h par an
  sur 3 ans entre 2010 et 2013)
- M1, Physique des lasers : Marie Houssin (60h par an sur 3 ans entre
  2010 et 2013)
- M2 Applications des lasers, Martina Knoop (3 fois 30h
  entre 2011 et 2014)
  
Le M2 requiert un long stage de recherche. Un étudiant de la promotion
2010-11, Marius Kamsap, est venu effectuer une partie de son stage à
l’Université de Provence (en cotutelle avec son département d’origine)
et a pu continuer en doctorat au sein d'AMU, financé par le CNES et la
région PACA en décembre 2012. Sa soutenance a eu lieu en
décembre 2015.  Lors de sa thèse, Marius Kamsap s’est aussi formé à la
diffusion scientifique et à l’enseignement universitaire grâce aux
missions d’enseignement d’un contrat doctoral et à un poste d’ATER
qu’il occupe à AMU au cours de l’année universitaire 2015-2016. Marius
Kamsap est maintenant recruté à l’USTM en tant qu’enseignant-chercheur
du département de physique de l’USTM et assure des cours et des TP. Il
poursuit son activité de recherche qu’il a initié pendant sa période
d’ATER au sein d’une collaboration avec l’équipe CIML de PIIM. Ses
travaux reposent sur des simulations numériques qu’il peut réaliser de
façon autonome avec son propre ordinateur et sont en relation forte
avec les expériences menées ou pouvant être menées dans le futur au
sein de notre équipe.
 
En ce qui concerne l’animation scientifique, à l’occasion de l’éclipse
solaire du dimanche 3 novembre 2013, visible au Gabon, et dans le
cadre de la collaboration entre l’AMU et l’USTM, les départements de
physique d’AMU et de l'USTM à Franceville et en collaboration avec
d'écomusée de Franceville, ont organisé toute une semaine
d’animations autour de la lumière. Elles ont eu lieu sur le site de
l’USTM à destinations des écoliers et lycéens (environ 200 élèves) et
le dimanche sur la place centrale de Franceville à destination du
grand public sous la forme d’un fête de la science (plus de 2000
personnes). Les étudiants du Master de physique de l'USTM ont été
sollicités et très impliqués dans la réussite de ce projet. Ce projet
a été porté par Prof Marie Houssin (AMU) et Prof Alain-Brice Moubissi
(USTM), et financé par AMU, la Société Française de Physique et
l'USTM. 3 chercheuses et enseignantes-chercheuses d'AMU y ont
activement participé.

Le département de physique de l’Université de Masuku et l’équipe CIML
du laboratoire PIIM de l’université d’Aix-Marseille souhaitent
profiter du retour de Marius Kamsap au sein de l’USTM pour mettre en
place une collaboration en recherche, pérenne. Elle se traduira par
des travaux communs et l’encadrement de stages de master en cotutelle
sur les sujets concernés. Nous espérons ainsi créer un vivier de
potentiels doctorants qui viendraient faire une partie ou la totalité
de leur thèse dans le groupe français pour enrichir leur formation en
leur permettant une pratique expérimentale complémentaire de
l’approche par la simulation qui pourra être développée au Gabon. En
effet, les expériences impliquées nécessitent un environnement
technique que l’USTM ne peut assurer pour l’instant mais permettent
aux étudiants de se former à l’usage et la maîtrise d’outils tels que
les sources lasers, et à la démarche de la confrontation à
l’expérience.

L’équipe marseillaise développe une expérience d’excitation cohérente
à plusieurs photons, sur un nuage d’ions piégés. La complexité du
système atomique impliqué ainsi que le grand nombre de degrés de
liberté contrôlant l’expérience nécessitent la mise en place de
nombreuses simulations numériques pour identifier les paramètres
optimum. Depuis son stage de master, Marius Kamsap manipule
régulièrement les outils de telles simulations et son année en tant
qu’ATER a été l’occasion de poser les bases des simulations futures
qui permettent déjà un échange régulier et sur le long terme autour
des enjeux de cette expérience. Cette thématique s’inscrit dans un
cadre plus vaste de la spectroscopie, nécessitant des techniques et
méthodologie avancées, et qui permettront à long terme d’envisager le
développement d’un projet expérimental multidisciplinaire à Masuku.
