Bref compte rendu de visite en République de Guinée
###################################################


:date: 2018-01-10
:category: Enseignement
:tags: 

**Michel SPIRO (président SFP)**

Cette visite du 13 au 17 Novembre 2017 fut organisée par l’école
polytechnique du côté français et par les autorités de République de
Guinée et l’Université Gamal Abel Nasser de Conakry du côté
Guinéen. Nous avons eu de nombreux contacts avec les autorités, les
enseignants très motivés et dévoués, notamment en physique, et de très
nombreux étudiants enthousiastes, atmosphère qui contrastait avec
l’extrême dénuement des laboratoires.

Nous avons convenu, entre autres : 

1. D’agir pour ouvrir plus largement le lycée français de Conakry aux
   élèves guinéens et de les préparer à réussir l’entrée dans des écoles
   d’ingénieur dans les pays voisins plus avancés (Sénégal et Côte
   d’Ivoire), voire en France avec une garantie de poste important à
   responsabilité de retour dans leur pays. La République de Guinée est
   en manque criant d’infrastructures alors qu’elle est très riche en
   ressources à la fois minérales et humaines.
2. J’ai réussi à convaincre les enseignants en physique de monter une
   société guinéenne de physique, ce qu’ils ont fait, et je me suis
   engagé à explorer la possibilité que la Société Guinéenne de Physique
   en lien avec la Société Française de Physique puisse adhérer à IUPAP
   (International Union of Pure and Applied Physics), « l’union mondiale
   des physiciens », dans des conditions gagnant-gagnant pour les trois
   associations.

La république de Guinée est un des pays les plus pauvres au monde,
mais doté d’un potentiel de développement exceptionnel. C’est notre
devoir d’accompagner les jeunes et les enseignants en physique pour
qu’ils aient accès et participent aux développements scientifiques qui
se font partout dans le monde et puissent choisir le mode
développement qu’ils souhaitent.
