Éthiopie : Projet d’un concours « Science et Technologie » 2018: Technologies et Développement Durable en Éthiopie
##################################################################################################################

:date: 2018-01-10
:category: Concours
:tags: 


**Joseph Ben Gelloun**

L’Association pour la Promotion Scientifique en Afrique (APSA) a pour
projet, dans le cours 2018, d’organiser un concours Science et
Technologie (S&T) en Afrique. Ce projet de l’APSA vise à développer de
nouvelles stratégies pour la promotion de la recherche en S&T en
Afrique. En réponse aux défis environnementaux et climatiques auxquels
l'Afrique et le monde sont aujourd'hui confrontés, le projet met
l’accent sur les nouvelles technologies et les innovations pour le
développement durable (la production d'énergie propre, le recyclage,
le traitement de l'eau, etc). Il a aussi pour objet de mettre en
valeur et de soutenir les créateurs africains de technologies et
d’innovations.

Au cours de la dernière décennie, l'Afrique a fait de rapides et
notables progrès en matière de télécommunication et de finance. Avec
l'utilisation des smartphones et d'Internet, de nombreuses
transactions ont été simplifiées voire démystifiées donnant une
nouvelle impulsion aux économies locales à croissance rapide. Les S&T
ont également été introduites et développées en Afrique par le biais
d’autres acteurs : les incubateurs technologiques et laboratoires
(AfriLabs1, Tech Hubs2, etc). En Juin 2016 sur le continent, il y
avait 173 de ces incubateurs, en 2018, on en compte 314.

Un examen plus attentif de ce prompt développement en Afrique montre
cependant qu'il y a place à amélioration. En effet, la présence de
divers organismes S&T ne correspond pas nécessairement à l'exposition
internationale de projets spécifiques. C'est ici que l'APSA avec ses
partenaires internationaux et en tant qu'association promouvant la
science en Afrique, pourrait avoir un rôle à jouer.

Parmi les pays africains qui se sont fermement engagés à promouvoir et
investir en S&T, l'Éthiopie peut être considérée comme unique. Les
laboratoires du Ministère des Sciences et Technologies, les Centres
STEM Synergy (14 centres couvrant le pays), les universités en S&T
abritent des jeunes scientifiques dont la maturité des projets est
frappante (générateurs de courants écologiques, des missions
robotiques, des portes intelligentes, etc). Le projet APSA Concours
S&T 2018 serait mené à succès dans ce pays en raison de ce contexte et
des possibilités qu'offre l'Éthiopie. Ainsi, la première édition du
Concours S&T de l’APSA sera organisée à Addis-Abeba, et sera ainsi
destinée aux scientifiques et entrepreneurs en herbe éthiopiens et
africains résidant en Éthiopie.

Outre la promotion des S&T en Éthiopie et plus généralement en
Afrique, le Concours S&T à deux principaux axes d’action. Le premier
axe est la co-organisation avec des partenaires locaux d’une fête de
la science pendant laquelle on décernera 3 prix pour les meilleures
innovations sur le développement durable. Le prix comprendra un
soutien financier et un séjour de recherche avec un mentorat dans un
laboratoire étranger. Le second axe consiste à rassembler des experts
locaux et internationaux qui examineront les projets pré-sélectionnés
et leur fourniront des critiques et revues utiles concernant la
qualité scientifique et technologique, l’impact pour développement
durable et leur potentiel socio-économique en vue de l’amélioration de
ces projets.

Les projets en compétition répondront aux critères suivants:
l’innovation, la défense de l’environnement et le développement
durable (quels sont les enjeux environnementaux ou de développement
durable auxquels s’attaque le projet?), la pédagogie (est-il possible
de produire une version simplifiée et peu coûteuse du prototype pour
des séances de formation pratique dans d'autres laboratoires, y
compris les écoles secondaires?), le critère social et économique (à
quel problème social le projet apporte-t-il une réponse? Qui pourrait
être intéressé par le projet?).

Le Concours S&T de l'APSA: Technologie et Développement Durable en
Éthiopie aura les impacts suivants :

* la stimulation des innovations et de l’esprit d’entreprise,
* la promotion de la contextualisation de la recherche pour le
  développement durable,
* l’établissement de ponts entre des institutions orientées S&T au
  niveau local et international,
* la consolidation des plus petits écosystèmes S&T les rendant visibles
  au niveau national et international,
* la création de start-up pour des prototypes excellents et à fort potentiel.

L'APSA croit aussi que le
succès d’un tel concours résonnera au-delà de l'Éthiopie, avec un
impact à plus grande échelle pour la S&T en Afrique.
