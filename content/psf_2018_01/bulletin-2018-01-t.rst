L'instrumentation à coût soutenable
###################################

:date: 2018-01-10
:category: Instrumentation
:tags: optique, chimie, biologie, humour

**François PIUZZI**

La disponibilité de l’instrumentation est un élément essentiel du
développement des sciences expérimentales et un maillon nécessaire
pour la réalisation de la caractérisation de divers problèmes
sociétaux, environnement, santé, énergie… Diverses ONG et divers
laboratoires ont commencé à travailler sur des projets
d’instrumentation à coût soutenable.
**Cette démarche ne peut évidemment pas concerner tous les domaines**
**de la physique et notamment n’est pas applicable aux grands instruments.**
Divers facteurs ont contribué à faire évoluer la situation vers une
baisse des coûts.

Les domaines pour lesquels des instruments scientifiques ou des
équipements de laboratoire d’un bon niveau pour un coût soutenable ont
été développés sont (de manière non exhaustive) :

- la *microscopie* : Fly Pi microscope (Trend in Africa)- et Flexure
  microscope (Université de Cambridge), 
- *le déplacement de gouttes de liquide par impulsion électrique* :
  (Opendrop Gaudi lab), 
- *télémétrie :* mesure de l’altitude de la ionosphère (Jean Michel
  Friedt) 
- *spectrométrie* : spectomètres visibles et Raman, (Eduardo Montoya, etc…) 
- *Qualité de l’eau :* turbidimètres, colorimètres,(Josuah Pearce) 
- *micro caractérisation de dépôts* : balance à quartz, 
- *manipulation de cellules (biologie):* pince laser (Gaudi Lab), 
- *nano sciences* : capteurs papier pour la santé (MIT groupe de Whitesidees,
  MIT Little Devices lab), 
- *matériel de laboratoire pour la chimie et la biologie :* centrifugation,
  agitation de solutions d’échantillons, etc…(Thaïlande) 
- *lévitation de gouttes de liquide par ultrasons* : Université de Bristol 
 
**Dans un prochain bulletin nous donnerons une description plus avancée de ces instruments avec les liens correspondants**.
|image40|

L’initiative Global Open Source Hardware (GOSH) : C’est une
organisation qui rassemble des membres dans plusieurs continents et
réfléchit à des instruments à coût soutenable. Des réunions annuelles
de « brain strorming » ont été organisées en 2016 au CERN et en 2017
au Chili. L’édition 2018 aura lieu en Afrique au Ghana ce sera
**Africaosh 2018**

D’autres applications ont été développées grâce au
**détournement de technologie** (reingeneering) et au recyclage de
composants (surtout basés sur la photonique) développés pour des
périphériques informatiques :

- *Détournement des imprimantes jet d’encre* : fabrication de bandelettes
  pour la détection de maladies
- *Détournement des lecteurs de CD ou DVD :* comme dans le cochon tout est bon !!
  **Cela fera l’objet d’une présentation complète dans le prochain bulletin**.
  
**Biblio :**

**Articles sur le développement , réflexions sur la philanthropie, etc… François Piuzzi**

**PHILANTHROPIE :**

Article du Monde : **Nord-sud "Repenser la philanthropie"** : l'écrivain Uzodinma 
Iweala a prononcé le discours inaugural de la conférence organisée sur
ce thème :

« il-faut-repenser-la-philanthropie-pour-l-afrique-comme-une-reparation-de-son-pillage » 
des extraits en sont proposés sur le site du Monde 
`http://abonnes.lemonde.fr/afrique/article/2017/11/10/il-faut-repenser-la-philanthropie-pour-l-afrique-comme-une-reparation-de-son-pillage_5213127_3212.html?xtmc=philanthropie&xtcr=1 <http://abonnes.lemonde.fr/afrique/article/2017/11/10/il-faut-repenser-la-philanthropie-pour-l-afrique-comme-une-reparation-de-son-pillage_5213127_3212.html?xtmc=philanthropie&xtcr=1>`__

Une version en anglais est également disponible : 
http://abonnes.lemonde.fr/afrique/article/2017/11/10/reparations-as-philanthropy-radically-rethinking-giving-in-africa_5213130_3212.html

Il est également intéressant pour la problématique du développement
d’écouter le discours du président ghanéen en réponse au président
Emmanuel Macron.

**Traduction du discours prononcé par Nana Akufo-Addo président du Ghana face au président français Macron**

"J’espère que le commentaire que je m’apprête a faire ne vas pas
offenser la personne qui a posé la question ainsi que les gens
présents dans cette salle. Je pense que l’on fait une erreur
fondamentale sur cette question. On ne peut pas continuer à faire des
politiques pour nous, dans nos pays, dans nos régions, sur notre
continent sur la base du soutien que le monde occidental, la France ou
l’UE voudrait bien nous donner. Ça ne va pas marcher, ça n’a pas
marché hier et ça ne marchera pas demain.  Notre responsabilité est de
tracer la voie par laquelle on pourra développer nos nations nous
mêmes.  Ce n’est pas correct pour un pays comme le Ghana, 60 ans après
les indépendances, d’avoir encore son budget de la santé et de
l’éducation financé par la générosité et la charité des contribuables
Européens. On devrait être maintenant capable de financer nos besoins
basiques nous mêmes. Et si nous devons considérer les prochaines 60
années comme une période de transition, une transition à partir de
laquelle on pourra se tenir debout de nous mêmes, notre préoccupation
ne devrait pas être ce que le contribuable français décide de faire
pour nous, quelque soit la simplicité qu’ils ont en France, ils sont
les bienvenus, on apprécie les interventions du contribuable français
à travers les actions que leur gouvernement fait à notre endroit. On
ne va pas cracher sur une aide.  Mais ce continent, avec tout ce qui
arrive est toujours le réservoir d’au moins 30% des plus importants
minéraux du monde. C’est le continent des vastes terres fertiles. Ce
continent a la plus jeune population de tous les continents au
monde. Donc il y a une énergie nécessaire, il y a le dynamisme, on l’a
déjà constaté. Ces jeunes gens qui ont montré beaucoup d’endurance et
d’ingéniosité en traversant le Sahara, trouvant des solutions pour
traverser la méditerranée avec des bateaux de fortunes. Toute cette
énergie, nous la voulons ici dans nos pays travaillant pour le
développement.  Et nous allons avoir ces énergies au service de nos
pays si nous mettons en place des systèmes qui montrent aux jeunes que
nos pays regorgent d'opportunités pour eux, qu’il y a encore de
l’espoir ici.
Le phénomène de migration est aujourd'hui présenté comme si c’était
quelque chose de nouveau. Il n’y a rien de nouveau à propos du
mouvement de populations. C’est aussi vieux que le monde, les
mouvements de populations ont toujours été liés aux mêmes causes :
l’échec de la patrie d’origine à procurer des opportunités et donc
l’on va voir ailleurs. Pour ceux qui connaissent l’histoire du 19 ème
siècle en Europe, ils savent que le plus grand mouvement de
populations s’est fait à cette époque, ces mouvements provenaient
essentiellement de l’Italie et de l’Irlande. Des vagues après des
vagues, des générations d’Italiens et d’Irlandais quittaient leurs
pays pour rechercher le paradis Américain parce que l’Irlande et
l’Italie ne fonctionnaient pas pour eux. Aujourd’hui on n’entend plus
parler de cela. Les jeunes Italiens et Irlandais restent aujourd’hui
dans leur pays respectifs. Nous voulons que les jeunes Africains
restent en Afrique. Et cela veut dire que nous devons nous débarrasser
de cette mentalité de dépendance, cette mentalité qui nous emmène à
nous demander ce que la France peut faire pour nous. La France fera ce
qu’elle a à faire pour son propre bien et si cela coïncide avec nos
intérêts, “tant mieux”, comme disent les français. Mais notre
principale responsabilité en tant que leaders, citoyens, c’est de
réfléchir à ce que nous devons développer pour nos propres pays.  Où
toutes les institutions fonctionnent correctement, cela va nous
permettre d’avoir la bonne gouvernance, une gouvernance responsable
qui rend compte et qui s’assure que l’argent mis à la disposition des
leaders est utilisé dans l'intérêt de l’État (du peuple) et non pour
les intérêts de ces leaders. Un système qui permet une diversité, qui
permet au peuple de s’exprimer librement et qui contribue à ancrer la
volonté du peuple et les intérêts du public. Le continent Africain
devrait être en mesure de donner de l’aide à d’autres endroits si l’on
se base sur les immenses ressources que nous avons. Nous avons
beaucoup de richesses. Et dans notre propre pays le Ghana, nous avons
besoin d’une mentalité qui nous fait prendre conscience que nous
pouvons y arriver. D’autres l’ont fait avant nous. On peut aussi le
faire, dès lors que nous avons cette mentalité, nous verrons que cela
va libérer notre potentiel. La Corée, Singapour, la Malaisie, ces pays
ont eu leur indépendance dans la même période que nous, on nous dit
même que au temps de l'indépendance, le revenu par habitant du Ghanéen
était supérieur a celui de la Corée. Aujourd’hui, la Corée fait partie
du monde développé.  C’est pareil pour la Malaisie et
Singapour. Qu’est ce qui s’est passé ? Pourquoi ont-ils fait cette
transition ? Et 60 ans après notre indépendance, nous sommes toujours
à ce point là. Voila les questions essentielles qui devraient être
notre préoccupation, en tant qu’Africains, en tant que Ghanéens. Et
non… quand je le dis c’est avec beaucoup de respect pour le président
français. Je pense que la coopération avec la France est quelque chose
que j’apprécie, je suis…. Tu sais, un grand ami de la France. Je suis
Francophile. Je n’ai donc pas de difficultés avec ça. Mais je parle de
notre propre motivation, de que ce nous devons faire pour mettre nos
pays au travail afin que nous puissions créer les conditions qui
permettront à nos jeunes d’abandonner ces efforts hasardeux pour se
rendre en Europe. Ils n’y vont pas parce qu’ils en ont envie, ils y
vont parce qu’ils pensent qu’ils ne peuvent pas trouver des
opportunités dans nos pays. Donc ceci devrait être notre préoccupation
première.  Et je pense avec ça…, si nous changeons nos mentalités,
cette mentalité de dépendance, cette mentalité qui dépend de l’aide et
de la charité, nous verrons que dans les décennies à venir, une
nouvelle race de jeunes africains verra le jour. Et cette nouvelle
mentalité africaine, dont on parlait à l’indépendance sera une réalité
de notre temps. Et c’est pourquoi, je dis que j’espère que je n’ai pas
contourné la question. Mais c’est cela ma pensée. Et c’est la raison
pour laquelle, j’ai adopté pour slogan de ma présidence :
“**Nous
voulons construire un Ghana au delà de l’aide au développement, un
Ghana qui est indépendant, qui se prend en charge, qui est capable
d’être debout tout en construisant sa propre destinée”. Nous pouvons
le faire, si nous avons la bonne mentalité pour le faire.**"


*Texte traduit par Diagaunet Dodie*
`(Mediapart https://www.mediapart.fr/tools/print/721658) <https://www.mediapart.fr/tools/print/721658>`__
 
Appel d’Abidjan :
-----------------

**L'appel d’Abidjan, lancé à l’occasion du cinquième sommet Union
Africaine – Union Européenne, demande que des engagements pris il y a
37 ans pour le développement de la science sur le continent soient
enfin respectés.**

Les 55 États de l’Union Africaine et les 28 de l’Union Européenne
seront représentés cette semaine en Côte d'Ivoire pour discuter des
relations entre les deux continents. Le cinquième sommet réunissant
les deux Unions s’ouvre en effet le 29 novembre à Abidjan. Des
scientifiques africains ont saisi cette occasion pour faire parler de
la recherche scientifique sur le continent, et interpeler leurs
gouvernements en publiant un texte intitulé
`Appel d’Abidjan <https://www.ird.fr/content/download/295480/4560611/version/1/file/Appel+Abidjan+Version+Finale.pdf>`__,
largement ouvert à la signature.

Cet appel demande surtout une chose simple : que soient respectés les
objectifs du "plan d’action de Lagos", adopté en 1980 par
l'Organisation de l'Unité Africaine de l'époque. A savoir que les
États membres consacrent 1% de leur PIB à la recherche
scientifique. Le même chiffre figure d'ailleurs dans
`les Objectifs de Développement Durable <http://www.un.org/sustainabledevelopment/fr/>`__,
adoptés en septembre 2015 par l'Assemblée générale des Nations
Unies. En Afrique, la dépense moyenne des pays est aujourd’hui
inférieure à 0,5%.
`Le Kenya, numéro 1 sur le continent, plafonne à 0,8%. <http://uis.unesco.org/apps/visualisations/research-and-development-spending/>`__

"La situation est désastreuse"
------------------------------

Outre cet investissement, les scientifiques demandent à leurs
dirigeants « une vision africaine ». En d’autres mots, que les
différents organes de recherche du continent se concertent et
échangent pour devenir plus compétitifs sur la scène internationale.

Ils motivent ces demandes par la situation de la recherche en Afrique,
bien moins vigoureuse que dans le reste du monde. "*La situation de
la recherche africaine est désastreuse, avec moins de 100 publications
par an dans les grandes revues internationales, contre 3.000 pour
l'Europe*", explique à l’AFP Daouda Aïdara, président de
`l’Académie des sciences, des arts, des cultures d’Afrique et des diasporas africaines <https://fr.wikipedia.org/wiki/Acad%C3%A9mie_des_sciences,_des_arts,_des_cultures_d%27Afrique_et_des_diasporas_africaines>`__.
Selon le `dernier rapport sur la science de l’Unesco <https://fr.unesco.org/Rapport_UNESCO_science/chiffres>`__,
les publications africaines représentent 2,6% des publications
mondiales.

Pour convaincre les décideurs, il faudra néanmoins réussir à attirer
les scientifiques de toute l’Afrique. Une grande majorité des 60
premiers signataires sont originaires de l'Afrique francophone, et la
Tunisie est le seul pays d’Afrique du Nord. L’Appel d’Abidjan va
devoir convaincre plus largement pour avoir un réel impact lors de ce
sommet Union Africaine – Union Européenne.

`Anthony Audureau <http://www.afriscitech.com/index.php/auteurs/224-anthony-audureau>`__,
`http://www.afriscitech.com/ <http://www.afriscitech.com/>`__

Pour finir, un peu d’humour dans un monde de sciences
-----------------------------------------------------

|image41|
source Chapatte NZZ AM SONNTAG Zurich 
                              

.. |image40| image:: /images/bulletin-2018-1/bulletin-33_1.jpg
   :width: 700px
.. |image41| image:: /images/bulletin-2018-1/bulletin-37_1.jpg
   :width: 700px
