BULLETIN PsF 09-10/2018
#######################

:date: 2018-10-21 12:00
:category: Bulletin
:tags: concours, projets, nouvelles
       
**(Septembre octobre 2018)**

Table des matières
==================

- **I** `ACTIONS ET INTERVENTIONS (mai-septembre) </articles/2018/oct./21/bulletin-2018-10-a/>`__
- **II** `PROJETS 2019 : à discuter lors de notre prochaine réunion </articles/2018/oct./21/bulletin-2018-10-b/>`__
- **III** `NOUVELLES </articles/2018/oct./21/bulletin-2018-10-c/>`__
