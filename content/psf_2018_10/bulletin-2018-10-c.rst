NOUVELLES
#########

:date: 2018-10-21
:category: Rencontres
:tags: concours, vulgarisation, technologie frugale

**1) Projet européen** chapeauté par **l’université de Pise** et qui
comprend un concours d’instrumentation pour le bio médical.
http://ubora-biomedical.org/news/

Les universités africaines concernées proviennent d’Afrique de l’est, un
projet similaire pour l’Afrique de l’ouest et l’Afrique centrale serait
à monter par une université française volontariste.

**2)** Un article intéressant sur un projet initié par l’université
**d’Oxford** sur la manière extrêmement frugale de fabriquer des
équipements localement:

https://theconversation.com/a-new-way-to-equip-africas-science-labs-get-students-to-build-their-own-100792

**3)PHYSLAB** une structure de l’université de Lahore au Pakistan qui
développe des expériences pour la vulgarisation de la physique :

https://www.physlab.org/experiment/steering-paramagnetic-leidenfrost-drops-in-an-inhomogeneous-magnetic-field/

Le scientifique à la tête de cet institut est **Sabieh Anwar** (très
reconnu au Pakistan) sabieh@gmail.com

https://www.youtube.com/watch?v=_9-QTp0ONk0&feature=youtu.be

|image1|\ **4)** François Piuzzi a rencontré **Tarek Loubani**, jeune
palestinien réfugié au Canada et actuellement à l’université de Waterloo
(Ontario), il développe des équipements en source ouverte pour le
médical, comme le stéthoscope imprimé en 3D montré dans la figure
ci-dessous. Article dans **Plos one** : *Validation of an effective, low
cost, Free/open access 3D-printed stethoscope*
https://doi.org/10.1371/journal.pone.0193087 March 14, 2018 voir plus de
projets dans https://github.com/GliaX

**5-Vulgarisation :**

Très belle expérience de démonstration pour l’éducation qui pourrait
servir pour les « Fêtes de la Science » : **Expérience I\ nduction**
https://www.youtube.com/watch?v=sENgdSF8ppA

**6- Open TP initiative**: Opération lancée par Julien Bobroff et
Frédéric Bousquet à l’Université Paris Sud http://opentp.fr/en/card/ qui
est une nouvelle méthode pour concevoir des travaux pratiques
directement par les étudiants.

+----------+-----------+
| |image2| | |image3|  |
+----------+-----------+


http://opentp.fr/en/tuto/a-geiger-counter-simulator/

**7- Informations diverses :**

https://www.aps.org/publications/apsnews/201807/international.cfm\ `July
2018 (Volume 27, Number
7) <https://www.aps.org/publications/apsnews/201807/index.cfm>`__
“Supporting Physics Education in Ethiopia” By Prof ABEBE KEBEDE

La `Society of Amateur Radio
Astronomers <http://www.radio-astronomy.org/>`__ (SARA) est en
partenariat avec le Stanford Solar Center
http://solar-center.stanford.edu/SID/obtaining/ pour distribuer les
détecteurs SuperSID pour détecter les éruptions solaires en surveillant
les perturbations induites dans la ionosphère. Un programme a été
construit pour les lycées et les détecteurs peuvent être demandés à
`SARA SuperSID <http://www.radio-astronomy.org/contact/SuperSID>`__

**Application (2013) par Ahmed Ammar, Hassen Ghalila (Laboratoire de
Spectroscopie Atomique, Moléculaire et Applications Faculté de Science
de Tunis – Université El Manar (TUNISIA) : New numerical tool SIDLab to
monitor Sudden Ionospheric Disturbances (SID)**

`Et dans le même domaine :
https://www.youtube.com/watch?v=kdeFGNL7cD4 <https://www.youtube.com/watch?v=kdeFGNL7cD4>`__\ **DIY
Laser magnetometer**

**L’université Stanford** a lancé un cours « Design for Extreme
affordability » (que l’on pourrait traduire par conception pour une
extrême accessibilité)
`https://extreme.stanford.edu/ <https://extreme.stanford.edu/%20>`__
pour former les étudiants à concevoir des projets innovants pour les
pays en développement.

.. |image1| image:: /images/bulletin-2018-10/image2.png
   :width: 250px
.. |image2| image:: /images/bulletin-2018-10/image3.png
   :width: 350px
.. |image3| image:: /images/bulletin-2018-10/image4.png
   :width: 350px
