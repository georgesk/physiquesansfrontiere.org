ACTIONS ET INTERVENTIONS (mai-septembre)
########################################

:date: 2018-10-21
:category: Rencontres
:tags: concours, conférences

**A- Concours « APSA Challenge Physique Expérimentale Afrique » (remise
des prix le 8 décembre 2017)**

Deux articles sur cette opération conjointe entre **APSA
(**\ Association pour la Promotion Scientifique de l’Afrique) **et
Physique sans frontières** qui s’est déroulée en 2017 ont été publiés :

**Reflets de la physique :58, 38-39**

**EPN : 49/3, 2018   04-05**

**B-Conférence YASE (Toulouse) :** **PsF** est intervenu dans la table
ronde «  `Faire de la recherche expérimentale en Afrique, quels
moyens? <https://www.yase-conference.eu/index.php/fr/programme/8-pages-fr/204-faire-la-recherche-experimentale-en-afrique-quels-moyens>`__ »,
avec une présentation sur « \ **l’importance des équipements en « source
ouverte »**. En coopération avec **Arouna Darga** nous avons présenté
quelques applications avec des instruments à coût soutenable lors de la
soirée de clôture. Des informations sur cette conférence peuvent être
obtenues sur le site https://afriscitech.com/fr/

**C- Université de Yammassoukroh (Côte d’Ivoire) :** François Piuzzi a
été formateur pour une vingtaine de doctorants de huit pays africains
(dont deux anglophones). Cela a eu lieu dans le cadre du réseau AFSIN
(African Spectral Imaging Network) financé par la coopération
internationale suédoise (ISP). Laboratoire de photonique du professeur
**Jérémie Zoueu**. Il y avait la présence d’un autre intervenant Mikkel
Brydegaard chercheur de l’université de Lund qui a apporté un Lidar qui
sera utilisé pour une thèse portant sur la détection des habitudes des
moustiques femelles. |image0|

Figure participants à la formation et Lidar (en haut à droite)

**D-** Réunion annuelle de la commission C13 « Physics for
Development »  de la International Union of Physicist and Applied
Physicist (**IUPAP)**: François Piuzzi y a participé en tant que
représentant français. Quelques décisions intéressantes ont été prises :

-  Création d’un groupe « \ **Sustainable scientific instrumentation** »

-  Décision de créer un site internet (avec la coopération de l’ICTP)
   pour regrouper les informations sur l’instrumentation en source
   ouverte, les travaux pratiques « soutenables », les nouveaux outils,
   les logiciels en accès libre, etc.. (utopie ?)

-  Décision de créer un groupe pour la préparation du centenaire de
   l’IUPAP (en 2022)

**Ajith Kumar** créateur du système **« experimental eyes «** à New
Dehli a présenté son dispositif qui comprend une interface sur laquelle
on construit des circuits électroniques, reliée en USB a un ordinateur
qui est transformé en un oscilloscope [STRIKEOUT:(]\ par le logiciel.
C’est un système très intéressant pour le lycée et les TP des premières
années d’université. Il est programmable en Python (libre accès). Pour
plus d’informations vous pouvez vous référer au bulletin de
janvier-février où se trouve l’article de Georges Khaznadar (Professeur
de Physique au lycée de Dunkerque) qui en est le promoteur en France.

**E-** Formation de Professeurs de physique de classes préparatoires à
la **science frugale** (récupération de composants pour le montage d’un
microscope, détournement de technologie, utilisation de Arduino et de
Raspberry Pi pour l’activation des différents types de moteurs récupérés
- Stage LIESSE) à l’IOGS par **Mejdi Nciri** et François Piuzzi.

**F-** Rencontre avec la présidente de la SFO madame **Pascale Nouchi**
et le bureau de la SFO. Des coopérations pourront être envisagées.

**G-** Rencontre avec **Michel Azemar** président de **Chimistes sans
Frontières**
`www.chimistessansfrontières.fr <http://www.chimistessansfrontières.fr>`__.
L’association a été crée en 2018 et est domiciliée à la Fondation de la
Maison de la Chimie. Nous avons évoqué des pistes pour des actions
communes notamment dans le domaine de l’instrumentation. Ils ont édité
un tryptique de présentation pour présenter leurs motivations, ce que
nous devrions faire également (un volontaire ?).

**H-** PsF s’associe à l’APSA pour l’organisation de la troisième
édition des Rencontres des Jeunes Chercheurs Africains en France (6-7
décembre IHP Paris). Ces rencontres s'adressent aux jeunes scientifiques
originaires de pays d’Afrique subsaharienne venus effectuer en France
tout ou partie de leurs études supérieures en \ **sciences
mathématiques, physique fondamentale et appliquée, informatique.** Elles
sont en priorité destinées aux doctorants, qu’ils soient accueillis dans
le cadre d’une thèse en cotutelle, dans le cadre d’un doctorat français
ou d’un post doctorat. Plusieurs scientifiques africains, sources
d’informations et de contacts, feront le voyage pour participer à ces
journées. Le site pour le programme et l’inscription est :
 `rjcaf.scienceafrique.fr <http://rjcaf.scienceafrique.fr/>`__. La date
limite d’inscription est le 30 octobre 2018.

**G-** Un message de Claude Lecomte (Développement de la
cristallographie en Afrique – Open Labs) concernant l’état de ses
actions en Afrique :

- Equipement d’un diffractomètre monocristal à l’Université de Dschang
  (Cameroun)

- Envoi d’un équipement au Bénin

- Organisation du congrès PCCr2 au Ghana

- L’opération d’équipement de l’université d’Abidjan en diffractomètres
  est terminée et les deux diffractomètres fonctionnent et sont utilisés !

.. |image0| image:: /images/bulletin-2018-10/image1.jpg
   :width: 3.74306in
   :height: 2.28611in
