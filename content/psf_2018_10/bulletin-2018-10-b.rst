PROJETS 2019 : à discuter lors de notre prochaine réunion
#########################################################

:date: 2018-10-21
:category: Débat
:tags: 

A. Création des ateliers « Physique sans Frontières » pour directement
   travailler sur l’instrumentation et les TP avec des étudiants et
   maître de conférence d’origine africaine et des personnes intéressées
   par le développement d’instrumentation en source ouverte. Les lieux
   qui pourraient être envisagés sont ; ESPCI, Jussieu, CRI.

B. Concours « Challenge Physique expérimentale Afrique », 2\ :sup:`ème`
   édition en 2019. La PsF pourrait s’associer à l’APSA pour le
   lancement du concours.

C. Participation au congrès général de la SFP (Nantes début Juillet
   2018), il faut choisir les intervenants à la session réservée à la
   commission en fonction des priorités et des projets.

