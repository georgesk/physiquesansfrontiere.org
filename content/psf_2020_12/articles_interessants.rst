ARTICLES INTERESSANTS (POUR NOTRE COMMISSION) DE PHYSICS TODAY (AMERICAN INSTITUTE OF PHYSICS) ET DE L’APS (AMERICAN PHYSICAL SOCIETY)
######################################################################################################################################

:date: 2020-12-19 12:01
:category: Annonces
:tags: Afrique, vulgarisation, optique

 
1) **Black voices in Physics** que l’on peut traduire (mal) par les voix
   noires en physique, (envoyé par **Michael Steinitz**), remarquons que
   l’un portraits concerne Sekazi Mintwa, président de la
   commission C13 (Physics for Development) de la IUPAP |br|
   https://physicstoday.scitation.org/do/10.1063/PT.6.4.20201022a/full/
2) Les mathématiques pour caractériser l’épidémie : |br|
   https://physicstoday.scitation.org/doi/10.1063/PT.3.4614 |br|
   Une traduction en français, portugais et espagnol peut être obtenue en
   utilisant `www.DeepL.com <http://www.deepl.com/>`__

|image3|

NOUVELLES DE L’APS :
--------------------

Premier planétarium au Kenya construit avec une structure en bambou
(envoyé par **Michael Steinitz**)

|image4| |br|
Source : D. C. Owen the travelling Telescope

https://physics.aps.org/articles/v13/169?utm_campaign=weekly&utm_medium=email&utm_source=emailalert

**Un exemple à faire connaitre à d’autres pays africains, qui permet la vulgarisation en astronomie et est très apprécié des familles.**
|clearboth|

ARTICLE DU JOURNAL DE L’APS « PHYSICS » SUR « LA DECOLONISATION DE LA PHYSIQUE »,
---------------------------------------------------------------------------------

en AFRIQUE DU SUD (envoyé par **Michael Steinitz**):
**How to decolonize South African Physics** ?

*South African researchers explain why and how their country’s physics should be untethered from its colonial past.*

Des chercheurs d’Afrique du Sud expliquent pourquoi et comment la physique
de leur pays doit être dissociée de son passé colonial.

https://physics.aps.org/articles/v13/178?utm_campaign=weekly&utm_medium=email&utm_source=emailalert


UN SECOND ARTICLE SUR LA PHYSIQUE EN AFRIQUE DU SUD :
-----------------------------------------------------

https://physics.aps.org/articles/v13/176

LA LETTRE AFRIQUE de l’APS :
----------------------------

- 12/18/2020 - `African Physics Newsletter: December 2020 <http://eepurl.com/hl3aQf>`__
- 08/28/2020 - `African Physics Newsletter: August 2020 <http://eepurl.com/hbKayv>`__
- 05/28/2020 - `African Physics Newsletter: June 2020 <http://eepurl.com/g3_0qX>`__
- 04/09/2020 - `African Physics Newsletter: April 2020 <http://eepurl.com/gYzp6f>`__

https://www.womeninscienceinafrica.com/film ce lien se trouve sur la
lettre APS de décembre

|hr|

UNE INFORMATION DANS LE DOMAINE DE LA CHIMIE PHYSIQUE : SUIVI D’UNE REACTION CHIMIQUE PAR DES IMPULSIONS FEMTOSECONDE DE RAYONS X COURTS :
------------------------------------------------------------------------------------------------------------------------------------------

https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.125.226001

|image5|
|clearboth|

|hr|

**GRAVITE ZERO** : Un doctorant camerounais **Patrice Désiré Dongo** de
l’Université Libre de Bruxelles (groupe de Carlo Iorio) a participé à
des expériences en Zéro G à bord d’un Airbus de l’ESA.

|image6|
|clearboth|

|hr|

Partenariats :
--------------

**IUPAP (commission C13) :** Avec **Ajith Kumar** (New Delhi) nous commençons
à créer un répertoire de l’instrumentation créée avec le paradigme en
**source ouverte** (open source). De manière à centraliser l’information,
nous aurons besoins d’experts et de rédacteurs.

**CERN et LIBAN : Demande d’un ingénieur du CERN, Martin Gastal**, pour
héberger un financement participatif pour participer à un apport de
matériel ce calcul pour les scientifiques au LIBAN (Fundraiser for
Lebanon), le CERN ne peut pas l’ héberger. Nous avons posé la question
au bureau pour savoir si la SFP accepterait que Physique sans
Frontière héberge ce financement participatif mais il manquait des
données que nous allons fournir en contactant Martin Gastal.

**CHIMISTES SANS FRONTIERES (Michel Azémar) :Travaux pratiques de chimie en réalité virtuelle**

- La chimie peut apporter une aide importante au développement des
  pays émergeants. La formation des jeunes en est une condition
  cruciale. Notre expérience et celle de nos collaborateurs témoignent
  de l’effet accrocheur de l’enseignement par l’apprentissage actif
  ("la main à la pâte"), ou la démarche par investigation et
  l’enseignement de la science expérimentale. Pourtant, cette méthode
  extrêmement efficace n’est pas à la disposition des établissements
  les plus pauvres, ce qui diminue le niveau de formation des
  populations défavorisées ainsi que leur mobilité sociale.
- L’ambition de **Chimistes sans frontières (ChSF)** serait donc de
  développer l’usage de la **réalité virtuelle (VR)** à destination
  pédagogique dans le domaine des travaux pratiques (TPs) de chimie et
  ainsi d’apporter une expérience formatrice aux élèves dans des
  écoles (collèges/lycées) démunies, n’ayant pas les moyens de
  construire et d’entretenir un laboratoire. Une première enquête
  montre qu’il existe peu d’exemples de laboratoires virtuels pour
  l’éducation au niveau requis. Un tel projet a reçu des réponses très
  positives des enseignants au Sénégal, Congo (RC), Côte d’Ivoire,
  Gabon, Tunisie, Maroc et Madagascar, qui apporteront leur
  collaboration pour obtenir un outil pédagogique adapté à la
  communauté locale.
- Un groupe de travail composé de bénévoles et d’étudiants de l’Ecole
  polytechnique et de l’Ecole Nationale Supérieure de Chimie de
  Montpellier a confirmé la faisabilité et l'attractivité de ce projet
  et a établi le cahier des charges pour la réalisation d’un
  prototype. Des organisations/sociétés spécialisées sont consultées
  pour une réalisation rapide.
- Ce prototype permettra de valider le concept, de susciter des
  améliorations et d’engager les collaborations avec les pays demandeurs.

|hr|

INSTRUMENTATION ET TECHNOLOGIE :
--------------------------------

**Nous suivons toujours l’actualité de l’Open Flexure Microscope (Richard Bowman fondateur) :**

Nous vous avons déjà parlé dans le bulletin du microscope
« **Open Flexure Microscope** » initialement développé à l’Université de
Cambridge par un groupe de jeunes doctorants autour de Richard
Bowman. C’est pour nous l’exemple emblématique de ce qui peut être
fait avec une coopération vertueuse basée sur la source ouverte (Open
source) et l’apport de l’impression 3D, pour un développement dans les
pays à faibles ressources. La réussite a été amplifiée grâce à
l’université de **Cambridge**, celle de Bath ainsi que de la
« **Royal Academy of Engineering** », de la « **Royal Society** » et de
l’ Engineering and Physical Sciences Council, qui ont financé et
financent le FabLab « **STIClab** »(https://sticlab.co.tz/index) en
**Tanzanie** ce microscope est donc fabriqué et maintenant distribué en
Afrique.

À quand une action semblable en France ? Universités, organismes de
recherche et IRD pourraient s’en inspirer !

Ce microscope a évolué et un article récent sur ces évolutions a été
publié sur **MagPi** le magazine officiel du Raspberry Pi
https://magpi.raspberrypi.org/articles/openflexure-microscope

L’image suivante montre le nouveau modèle de l’**open flexure microscope**
en action sur la **détection de la malaria dans les globules rouges**.

|image7|
|image8|
|clearboth|

On peut mieux voir l’évolution du microscope sur l’image suivante, le
réglage x, y, z est automatisé grâce à l’adjonction de moteurs pas à
pas. La recherche de globules rouges infectés par la malaria est donc
maintenant automatisée.

|image9|
|clearboth|

**NOUVEAUTE RASPBERRY PI - UN ORDINATEUR DANS UN CLAVIER**

https://www.youtube.com/watch?v=ZSvHJ97d8n8&feature=emb_logo
(Information issue du site Futura science ):

|image10|
« D'un point de vue matériel, cette version intègre un processeur ARM
Cortex-A72 quadricœur à 1,8 GHz, 4 Go de
`mémoire vive <https://www.futura-sciences.com/tech/definitions/informatique-ram-575/>`__,
du `Bluetooth <https://www.futura-sciences.com/tech/definitions/informatique-bluetooth-1865/>`__
5.0 et du
`Wi-Fi <https://www.futura-sciences.com/tech/definitions/internet-wi-fi-1648/>`__
802.11ac, mais un port Ethernet. Pour la connectique,
deux ports micro-HDMI, deux connecteurs
`USB <https://www.futura-sciences.com/tech/definitions/informatique-usb-2011/>`__
3.0 et un USB 2.0. Disponible déjà en anglais, allemand, français,
italien et espagnol, ce clavier sort aussi dans une version à 100
dollars (environ 100 euros) qui inclut une souris, un câble HDMI et
surtout
`Raspberry <https://www.futura-sciences.com/tech/actualites/informatique-raspberry-pi-4-star-mini-pc-revient-charge-76585/>`__
OS pré-installé sur une carte micro-SD. Ce sera suffisant pour
naviguer sur
`Internet <https://www.futura-sciences.com/tech/definitions/internet-internet-3983/>`__,
regarder des vidéos et la
`bureautique <https://www.futura-sciences.com/tech/definitions/informatique-bureautique-522/>`__.
Et bien sûr, programmer ».

|clearboth|

**LIDAR (LIGHT DETECTION AND RANGING.- détection de la lumière et télémétrie) :**

Beaucoup d’équipements incluent maintenant un LIDAR pour la détection
d’obstacles (mais également pour d’autres raisons). Ainsi le LIDAR
est -il présent dans les aspirateurs robots, l’Iphone 12, certaines
tablettes, etc... Nous recherchons un volontaire pour un article sur
le LIDAR dans le quotidien, n’hésitez pas à nous contacter.

+------------------------------------------------------------+
| |image11|                                                  |
+------------------------------------------------------------+
| *Exemple d’une application du LIDAR avec une tablette.*    |
+------------------------------------------------------------+

|hr|

**MICROSCOPE UC2 (YOU SEE TOO) :**

|image12|
Publié le 25 novembre dans **Nature communications** un article sur un
microscope low cost aux faux airs de LEGO : Il s’appele UC2 pour You
See Too !!

https://www.nature.com/articles/s41467-020-19447-9
|clearboth|

|hr|

RÉCUPÉRATION DE MATÉRIEL :
--------------------------

Récupération d’un microscope **LEICA** grâce à notre vice-présidente
**Odette Fokapu**, nous allons vérifier son fonctionnement et ensuite lui
trouver un labo d’accueil, le transport sera à la charge du labo
d’accueil.

|hr|

VULGARISATION
-------------

**CHRISTOPHE DAUSSY** Professeur à Paris XIII (Université Sorbonne Nord),
il est impliqué dans l’association « **Atouts science** »
(http://www.atouts-sciences.org/). Il avait produit et distribué
gratuitement plusieurs centaines d'exemplaires d'un kit pédagogique le
"**Lightbox**" à destination des collèges, lycées et associations
(https://youtu.be/txcINx4Bn0Q).

https://www.youtube.com/watch?v=txcINx4Bn0Q

|image13|
Le financement a été assuré par un Labex : « First TF Temps Fréquence ».
Christophe est intervenu à l’université de Bambey (Sénégal).

|clearboth|

Contenu du kit :

|image14|
|clearboth|

VULGARISATION (EXPERIENCE ARAGO FRESNEL) :
------------------------------------------

Article trouvé dans le dernier numéro de la revue « Photoniques
» -From the last edition of the « Photoniques” journal (n° 104) “The
Arago Fresnel experiment “ (en anglais /English):

https://www.photoniques.com/articles/photon/pdf/2020/05/photon2020104p21.pdf

les articles de « Photoniques » sont en accès libre pour le moment
profitez en !!!

|hr|

Télescope UNISTELLAR + science citoyenne – développé en France !!
-----------------------------------------------------------------

https://www.futura-sciences.com/sciences/actualites/astronomie-evscope-unistellar-revolution-astronomes-amateurs-68117/#xtor=EPR-17-[HEBDO]-20201214-[ACTU-L-eVscope-d-Unistellar--une-revolution-pour-les-astronomes-amateurs]

prix en France : 3000 € https://unistellaroptics.com/

|hr|

CLASSEMENT DES UNIVERSITÉS AFRICAINES
-------------------------------------

Publié par l’agence ECOFIN : « UniRank, un annuaire et moteur de
recherche international de l'enseignement supérieur, a publié son
`classement 2020 <https://www.4icu.org/top-universities-africa/>`__
des 200 meilleures universités en Afrique.  Seules 6
universités d’Afrique subsaharienne francophone font partie du top 200 ».

Le classement dépendant des critères pris en compte (restons
prudents), voici celui des universités d’Afrique subsahélienne
francophones :

+----+----------------------------------------+--------------+---------------+
|    | Universités                            | Pays         | Classement    |
|    |                                        |              | Afrique       |
+====+========================================+==============+===============+
| 1  | Université Cheikh Anta Diop            | Sénégal      |  33           |
+----+----------------------------------------+--------------+---------------+
| 2  | Université Gaston Berger               | Sénégal      |  113          |
+----+----------------------------------------+--------------+---------------+
| 3  | Université Ouaga I Joseph Ki-Zerbo     | Burkina      |  152          |
|    |                                        | Faso         |               |
+----+----------------------------------------+--------------+---------------+
| 4  | Université Abomey-Calavi               | Benin        |  154          |
+----+----------------------------------------+--------------+---------------+
| 5  | Université de Lomé                     | Togo         |  169          |
+----+----------------------------------------+--------------+---------------+
| 6  | Université de Dschang                  | Cameroun     |  177          |
+----+----------------------------------------+--------------+---------------+

.. |image3| image:: /images/bulletin-2020-12/image003.jpg
   :width: 450px

.. |image4| image:: /images/bulletin-2020-12/image004.jpg
   :width: 450px

.. |image5| image:: /images/bulletin-2020-12/image005.jpg
   :width: 450px
	   
.. |image6| image:: /images/bulletin-2020-12/image006.jpg
   :width: 450px
	   
.. |image7| image:: /images/bulletin-2020-12/image007.jpg
   :width: 450px
	   
.. |image8| image:: /images/bulletin-2020-12/image008.jpg
   :width: 450px
	   
.. |image9| image:: /images/bulletin-2020-12/image009.jpg
   :width: 450px
	   
.. |image10| image:: /images/bulletin-2020-12/image010.jpg
   :width: 450px
	   
.. |image11| image:: /images/bulletin-2020-12/image011.jpg
   :width: 450px
	   
.. |image12| image:: /images/bulletin-2020-12/image012.jpg
   :width: 450px
	   
.. |image13| image:: /images/bulletin-2020-12/image013.jpg
   :width: 280px
	   
.. |image14| image:: /images/bulletin-2020-12/image014.jpg
   :width: 450px
	   
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
   
