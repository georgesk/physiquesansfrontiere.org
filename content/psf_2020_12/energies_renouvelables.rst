ÉNERGIES RENOUVELABLES ET ENVIRONNEMENT
#######################################

:date: 2020-12-19 12:01
:category: Open access
:tags: optique, technologie frugale, environnement, Afrique
       
SUNCALOR
========

four solaire avec un suivi de trajectoire solaire pour parabole.
http://www.suncalor.com/

|image18|

Ce qui est intéressant dans ce dispositif, c’est le mécanisme de suivi
de trajectoire solaire qui parait simple et économique et pourrait
être récupéré pour des applications scientifiques. Le système est
breveté mais l’inventeur autorise l’usage des plans, fournis sur le
site. A noter que la parabole est celle d’une antenne satellite et
présente donc un intérêt certain pour l’économie circulaire. Nous
avons envoyé cette information à plusieurs chercheurs en Afrique.

|image19|
|image20|
|clearboth|
Plan des pièces nécessaires préparé pour la découpe par laser

|hr|

ONG EMERGI (Hollande)
=====================

|image21|

    Au Liberia, Emergi travaille sur un projet de tricycles
    électriques. Spécialisée dans les énergies renouvelables, la
    start-up néerlandaise a lancé une campagne de crowdfunding pour
    lever 10 000 € nécessaires à la fabrication des prototypes. Plus
    de 50% de la cagnotte est déjà obtenue.

« Nous avons mené une étude de marché approfondie auprès des
conducteurs de kekehs et d'autres acteurs des énergies renouvelables
au Liberia, et nous sommes convaincus que ces véhicules électriques
amélioreront à la fois le coût de possession (de 4 000 euros à 2 500
euros par an) et l'expérience du conducteur, tout en réduisant les
émissions de CO2, », affirme Emergi.

En plus d’être une réponse aux pénuries récurrentes de carburant au
Liberia, l’entreprise estime que la mise sur le marché des kekehs
électriques entraînera une meilleure inclusion des femmes conductrices
dans ce secteur qui reste fortement misogyne.

Selon Emergi, les **kekehs** électriques qui seront introduits sur le
marché comprendront des innovations visant à augmenter les revenus et
la sécurité des conducteurs, notamment le paiement mobile, les caméras
de sécurité embarquées, le système de géolocalisation et les bornes de
recharge à énergie solaire.

*Source agence Ecofin*

|hr|

NOUVELLES DE L’UNIVERSITE DE CAMBRIDGE
======================================

    Welcome to Cambridge Global Challenges

    Cambridge Global Challenges is the Strategic Research Initiative
    (SRI) of the University of Cambridge that aims to enhance the
    contribution of its research towards addressing the Sustainable
    Development Goals (SDGs) by 2030, with a particular focus on the
    poorest half of the world’s population.


|image22|

Cambridge Global Challenges est l'initiative de recherche stratégique
(SRI) de l'Université de Cambridge qui vise à renforcer la
contribution de sa recherche à la réalisation des objectifs de
développement durable (SDG) d'ici 2030, en mettant particulièrement
l'accent sur la moitié la plus pauvre de la population mondiale.

https://www.gci.cam.ac.uk/cambridges-response-covid-19-oda-target-countries/ongoing-projects

|clearboth|

    Contribute to Cambridge's response to COVID-19 in developing countries

Contribuer à la réponse de Cambridge à la COVID 19 dans les pays en développement :

    “Join working groups that invite your collaboration, create a new
    working group and learn about available funding opportunities
    `here <https://www.gci.cam.ac.uk/cambridges-response-covid-19-oda-target-countries>`__.”

Rejoignez les groupes de travail qui vous invitent à collaborer, créez
un nouveau groupe de travail et renseignez-vous sur les possibilités
de financement disponibles.


.. |image18| image:: /images/bulletin-2020-12/image018.jpg
   :width: 450px
	   
.. |image19| image:: /images/bulletin-2020-12/image019.jpg
   :width: 450px
	   
.. |image20| image:: /images/bulletin-2020-12/image020.jpg
   :width: 450px
	   
.. |image21| image:: /images/bulletin-2020-12/image021.jpg
   :width: 450px
	   
.. |image22| image:: /images/bulletin-2020-12/image022.jpg
   :width: 450px
	   
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
   
