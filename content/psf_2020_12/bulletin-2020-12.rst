Bulletin Physique sans Frontières (déc 2020)
############################################

:date: 2020-12-20 12:01
:category: Bulletin
:tags: nouvelles


ÉDITORIAL:
==========

Cette épidémie semble vouloir continuer et cette seconde vague nous
frappe, pas tous de manière égale d’ailleurs. Il est en effet
surprenant de voir que pour l’Afrique, l’effet de l’épidémie à été
beaucoup moins grave que prévu. Les raisons ne sont pas connues,
cependant ont peut citer la jeunesse de la population, le grand nombre
d’épidémies auxquelles la population a déjà été exposée ainsi que les
traitements à base de quinine largement utilisés pour lutter contre le
paludisme. Cela implique que les recherches en infectiologie doivent
être amplifiées.

Par contre, l’arrêt ou le quasi arrêt de l’épidémie en Chine, Corée,
Taiwan semble surtout dû à des mesures strictes d’isolement des
personnes contaminées. La bonne nouvelle est cependant la
disponibilité de vaccins qui nous permettre de revenir à une vie
normale dans quelques mois avec un peu de chance !

Pour beaucoup de pays d’Afrique ainsi pour que d’autres pays à faible
ressources c’est surtout le ralentissement économique qui aura un
impact très important. On peut s’attendre à ce que les universités
soient touchées par des restrictions de financements.

Ce sont des moments graves où nous devons tous faire preuve de
solidarité. La physique appliquée peut apporter des outils pour
caractériser certains aspects de l’épidémie, comme la filtration des
aérosols par différents types de masques, la décontamination par
utilisation de rayonnement UV-C, etc...Cependant une nouvelle de
dernière minute redonne de l’espoir montrant qu’en ces temps
d’individualisme sacralisé la solidarité est encore possible. La
fondation de l’OSA (Optical Society of America) a lancé un programme
de fabrication de dispositifs de décontamination de masques N95 par
UV-C en impliquant de jeunes étudiants avec comme cible les hôpitaux
des pays à faibles ressources.  L’organisme a également fourni trente
bourses à des étudiants au, Bangladesh, Brésil, Ethiopie, Ghana,
Kenya, Mexique et Sénégal. Vous trouverez plus de détails dans la
suite de ce bulletin avec les liens vers les articles originaux.


**François Piuzzi**

Les articles de ce bulletin :
=============================

- `Le projet “Turning Sun Into Water” récompensé par la Chancellerie des Universités ! <{filename}/psf_2020_12/turning_sun_into_water.rst>`__
- `Appel d'offres <{filename}/psf_2020_12/appel_offres.rst>`__
- `Articles intéressants <{filename}/psf_2020_12/articles_interessants.rst>`__
- `Enseignement pour les sciences expérimentales en temps de Covid <{filename}/psf_2020_12/experimentation_temps_covid.rst>`__
- `Colloques <{filename}/psf_2020_12/forum_aps.rst>`__
- `Covid et Physique <{filename}/psf_2020_12/covid_physique.rst>`__
- `Énergies renouvelables & Environnement <{filename}/psf_2020_12/energies_renouvelables.rst>`__

|hr|

CADEAU DE FIN D’ANNÉE
=====================

photos des champignons bioluminescents découverts récemment dans le
Nord Est de l’Inde par une équipe indo-chinoise. (source : the Indian
Express), ce ne sont pas des effets psychédéliques suite à des
ingestions mais plus prosaïquement des effets dus à la bioluminescence
de certaines molécules, ici la luciférase, (la même molécule qui est
responsable de la luminescence des lucioles).

+-------------------------+----------------------------+
|  |image23|              |    |image24|               |
+-------------------------+----------------------------+
| |image25|                                            |
| |clearboth|                                          |
| Art et sciences : en l’occurrence il s’agit de Art   |
| et Physique.                                         |
+------------------------------------------------------+

.. |image23| image:: /images/bulletin-2020-12/image023.jpg
   :width: 225px
	   
.. |image24| image:: /images/bulletin-2020-12/image024.jpg
   :width: 225px
	   
.. |image25| image:: /images/bulletin-2020-12/image025.jpg
   :width: 450px
	   
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
   
