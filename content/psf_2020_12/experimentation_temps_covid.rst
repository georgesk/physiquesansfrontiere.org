ENSEIGNEMENT POUR LES SCIENCES EXPERIMENTALES EN TEMPS DE COVID
###############################################################

:date: 2020-12-19 12:01
:category: Enseignement
:tags: covid, vulgarisation

Article de **Jérome Randon** « Des activités à la maison pour développer
les compétences scientifiques » (envoyé par Michel Azémar).

Je vous inclue ci-dessus l’introduction de son article qui est importante :

    « La période de confinement que les établissements d’enseignement
    supérieur viennent de vivre a conduit à différentes formes de
    continuité pédagogique dont les modalités restaient à
    l’appréciation de chaque établissement. Si la continuité
    relationnelle a globalement pu être assurée, les formations ont
    été tellement modifiées dans leur forme que l’équivalence
    pédagogique de ces nouvelles modalités doit encore être évaluée en
    profondeur pour juger de l’efficacité de la formation distancielle
    qui a été mise en œuvre. Quels que soient les résultats des études
    à venir, cet évènement doit aussi être une invitation à une
    réflexion de fond sur les objectifs des formations et les
    modalités pédagogiques proposées afin de les atteindre. Par
    exemple, en tant qu’enseignant, nous rencontrons souvent des
    difficultés pour former nos étudiants à la démarche scientifique,
    démarche que nous avons du mal à travailler au regard du faible
    temps qui peut être consacré aux activités expérimentales dans les
    établissements de formation. Deux articles très récents de
    l’ActualitéChimique proposaient de développer des outils pour
    l’analyse physico-chimique à l’aide de microcontrôleur type
    Arduino, et soulignaient le très faible coût associé à ces objets
    (moins de 10 € pour un colorimètre à trois longueurs d’onde). Si
    des activités expérimentales conduisant à l’élaboration et à
    l’usage de ces outils peuvent être proposées dans les
    établissements de formation en présence des enseignants, ne
    pourrait-on pas envisager d’élaborer ces outils et de les utiliser
    en l’absence d’enseignant, voire même dans d’autres lieux, tout en
    menant une véritable démarche scientifique d’investigation avec
    des produits de la vie courante et des outils à très faible coût ?
    Avec des matériels et des produits qui sont aisément disponibles
    et permettent d’opérer dans des conditions de sécurité où les
    risques sont minimisés, s’ouvrent de nouvelles opportunités
    pédagogiques orientées autour d’une pédagogie de projet mobilisant
    nombre de concepts pertinents pour le chimiste. »

Jérome Randon a également publié deux articles sur l’Actualité
Chimique, sur
« **Construire un colorimètre et évaluer l’incertitude des méthodes de dosage par étalonnage** »
et « **Repenser l’enseignement des sciences analytiques par la construction et l’évaluation d’instruments** ».

Il faut aussi aller voir son site :

http://arduino-enseignement-chimie.univ-lyon1.fr/workspaces/136817/open/tool/home#/tab/-1

EXPÉRIENCES DE CHIMIE A FAIRE CHEZ SOI :
========================================

https://www.futura-sciences.com/sciences/dossiers/chimie-experiences-chimie-faire-chez-soi-1561/#xtor=EPR-17-[HEBDO]-20201214-[DOSS-Experiences-de-chimie-a-faire-chez-soi]

