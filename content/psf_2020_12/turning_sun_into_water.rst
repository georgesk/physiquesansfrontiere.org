Le projet “Turning Sun Into Water” récompensé par la Chancellerie des Universités !
###################################################################################

:date: 2020-12-19 12:01
:category: Annonces
:tags: Afrique
:author: Arouna Darga

|image1|

Bravo à Monsieur Simon Meunier, docteur passé par le laboratoire
Geeps : il vient de recevoir le prix de la Chancellerie des
Universités pour sa thèse portant sur le développement d’une
méthodologie de conception optimale de systèmes photovoltaïques de
pompage d’eau pour les communautés rurales, combinant les aspects
techniques, économiques, environnementaux et sociétaux : application
au village de Gogma au Burkina Faso

L’objectif de la thèse était de développer une méthodologie de
conception optimale des systèmes de pompage photovoltaïques (PVWPS)
qui permette de déterminer les dimensionnements du PVWPS et ses
positions dans le village qui maximisent l’impact positif sur le
développement socio-économique et minimisent le coût sur cycle de vie
du PVWPS.

|image2|

Simon, initialement ingénieur de l'Institut d'Optique Graduate School
et diplômé d'un MSc Sustainable Energy Futures de l'Imperial College
de Londres ,est recruté à CentraleSupélec en qualité de Maître de
Conférences à compter du 1er décembre 2020.

Le projet, financé par une campagne de financement participatif a été
réalisé avec Matthias Heinrich, étudiant en sciences de l'ingénieur à
l’École Normale Supérieure de Rennes, avec le concours de la startup
`Burkinabè DargaTech <http://www.dargatech.com/>`__.
Le projet s'appelle Turning Sun Into Water et il
permet aux 700 habitants de Gogma de s'alimenter en eau potable, tout
en permettant aux chercheurs de travailler sur les données. Plus
d’informations sur le projet :

- https://www.kisskissbankbank.com/fr/projects/turning-sun-into-water
- https://www.helloasso.com/associations/eau-fil-du-soleil

.. |image1| image:: /images/bulletin-2020-12/image001.jpg
   :width: 450px

.. |image2| image:: /images/bulletin-2020-12/image002.jpg
   :width: 450px

