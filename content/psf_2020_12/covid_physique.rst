COVID et PHYSIQUE : l’exemple vertueux et solidaire de la FONDATION OSA
#######################################################################

:date: 2020-12-19 12:01
:category: Open access
:tags: covid

\ 

      (trouvé grâce à la Newsletter de la **Société italienne de  Physique**
      – « Prima Pagina »- qu’ils en soient remerciés)
      http://www.primapagina.sif.it/article/1206

Il s’agit d’une opération menée par la fondation OSA pour trouver une
solution pour décontaminer les masques pour les pays qui éprouvent des
difficultés à en avoir en quantité suffisante.

Le texte d’introduction en anglais :

A NETWORK OF OPTICS STUDENTS TACKLES N95MASK SHORTAGE C. 30-11- 2020-
---------------------------------------------------------------------

“Doctors and nurses rely on respirator masks to protect them from
COVID-19. These masks are designed to be discarded after every patient
visit. The onslaught of the pandemic challenged supply chains and left
healthcare workers in short supply of an essential tool: the N95
mask. This shortage impacted some the world's wealthiest nations, but
took an exorbitant toll on low-income countries. The one-time-use
equipment became something worn for weeks or months at a time.

A solution was needed to safely extend the use of N95's quickly and
inexpensively. Enter optics!

Thomas Baer, executive director of the Stanford Photonics Research
Center (SPRC), USA, and 2009 President of TheOptical Society (OSA),
and Lambertus Hesselink, Stanford University, USA, designed a simple
and economical way to decontaminate N95's using ultraviolet (UV)
light. Their prototype is a high throughput UV-C decontamination
chamber with the potential to expose over 5,000 N95's per day at the
generally accepted fluence levels necessary for decontamination of
coronaviruses.

|image17|
|clearboth|
OSA Student members in Brazil assembling their chamber in the
lab. Photo courtesy of the OSA Foundation.

The construction of the chambers and their efficacy was published in
OSA's **Applied Optics**, but Baer and Hesselink wanted to do more. They
wanted a way to get actual chambers into the hands of healthcare
workers in limited-resource settings. Baer and Hesselink turned to the
OSA Foundation (OSAF) and our network of student chapters to build
these“ do-it-yourself” chambers and deliver them to hospitals in
need. There was an inspiring response from our optics students and
their advisors. The OSAF has distributed 30 grants to chapters in
countries such as
**Bangladesh, Brazil, Ethiopia, Ghana, Kenya, Mexico and Senegal**.
Grant requirements included sourcing components,
budgeting, building, testing, and and – most importantly –
**partnering with a local hospital** to install and operate the
chambers. These students are gaining invaluable experience working
directly with administrators and doctors,
**explaining the UV-C decontamination process** and creating
partnerships between the hospitals and their chapters.

OSAF hopes others will be inspired by this effort and explore building
their own chambers (**all information is open-source**) or reaching out to
local healthcare providers to determine how best to partner and bring
forth needed solutions. The optics and photonics community is
well-positioned to leverage our science to make an immediate and
long-term impact.

**Auteur: Chad Stark** – Executive director of the OSA Foundation. The OSA
Foundation (OSAF) serves as the philanthropic arm of The Optical
Society (OSA).OSAF's vision is a thriving, robust, and collaborative
optics and photonics community that inspires and empowers the next
generation of leaders in the field. OSAF's mission is to inspire
promising individuals to pursue careers in optics and photonics that
lead to ongoing support and an enduring passion for the community.”

Addendum : https://www.covidppeguide.com/uvc-decon

Articles publiés dans **Applied Optics** :

- Article experimental https://www.osapublishing.org/ao/fulltext.cfm?uri=ao-59-25-7585&id=437635
- Article modelisation : https://www.osapublishing.org/ao/fulltext.cfm?uri=ao-59-25-7596&id=437636
  

.. |image17| image:: /images/bulletin-2020-12/image017.jpg
   :width: 450px
	   
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
   
