Appel d'offres :
################

:date: 2020-12-19 12:01
:category: Annonces
:tags: Afrique, technologie frugale

Les appels d’offre et appels à projets suivants viennent d’être publiés :
-------------------------------------------------------------------------

I. **Semaine des jeunes talents scientifiques africains** du 7 au 12 février 2021 Cité des Sciences Faisant suite à l’appel à candidatures.

https://bj.ambafrance.org/Appel-a-candidatures-semaine-des-jeunes-talents-scientifiques-africains

II. **Première édition au CNRS d'un appel à projet avec l'Afrique subsaharienne**,
    lancé par l'INP (Institut de Physique), le texte de l'appel est accessible par le lien suivant : |br|
    https://international.cnrs.fr/campagne-cnrs/ |br|
    pays ciblés: Bénin, Burkina Faso, Cameroun, Ethiopie, Ghana, Kenya, Nigéria, Ouganda, Sénégal, Tanzanie mais les autres pays peuvent être accessibles. |br|
    Pour ces projets, il faut être en contact avec un chercheur CNRS

III. **https://miti.cnrs.fr/appel-projet/sciences-frugales/**
     c’est un appel conjoint CNRS IRD. Il faut appartenir à un laboratoire
     CNRS ou IRD pour soumettre un projet. |br|
     Nous avons depuis plus d’une dizaine d’année fait la promotion de
     l’instrumentation et de la science frugale (en particulier avec
     l’exposition « Science Frugale » à l’Espace des sciences Pierre Gilles
     de Gennes à L’ESPCI) sans succès. Les méthodes utilisées sont
     caractérisées par le paradigme de la source ouverte (open source) et
     en général aboutissent à des diminutions drastiques du coût des
     instruments. |br|
     Nous nous réjouissons donc qu’il y ait maintenant un appel d’offre
     conjoint « CNRS—IRD » sur les sciences frugales.

.. |br| raw:: html

  <br/>
