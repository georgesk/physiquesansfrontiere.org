FORUM ON INTERNATIONAL PHYSICS (APS)
####################################

:date: 2020-12-19 12:01
:category: Rencontres
:tags:

|image15|

PHYSICS MATTERS — On-line Colloquia Series ...
==============================================
    \... as a “Physics for Development” initiative in COVID times. The
    APS Forum on International Physics (FIP) is pleased to announce
    the PHYSICS MATTERS on-line colloquia series. Please visit the
    website for full details.

L’APS va délivrer une série de conférences en ligne dans le domaine de
« Physique pour le développement » en temps de COVID.

POLARQUEST – A sailing expedition to measure cosmic rays (and not only) beyond the Arctic Circle
================================================================================================

|image16|
Une expedition sur voilier pour la mesure des rayons cosmiques (mais
pas que..) au dessus du cercle polaire.(L’archipel du Svabald) par
Poala Catapano et Luisa Cifarelli.

A two-voice lecture will be delivered by **Paola Catapano** (CERN, Geneva
Switzerland – Head of the Audiovisual Production Service for the CERN
Communications Group and Project Leader of Polarquest) and Luisa
Cifarelli (University of Bologna, Italy – FIP Chair and past President
of the Centro Fermi, Rome, Italy). It will be interleaved by pieces of
a riveting documentary on the expedition. The whole duration will be
about 90 mn.

Lien : https://www.aps.org/units/fip/activities/video.cfm

|clearboth|
|hr|

.. |image15| image:: /images/bulletin-2020-12/image015.jpg
   :width: 450px
	   
.. |image16| image:: /images/bulletin-2020-12/image016.jpg
   :width: 450px
	   
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
   

