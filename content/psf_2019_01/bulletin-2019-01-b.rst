Atelier Instrumentation à coût soutenable à la Escuela Politecnica Nacional de Quito
####################################################################################

:date: 2019-01-30
:category: Instrumentation
:tags: optique

(12-14 décembre)

Cet atelier de restitution vient terminer le projet « microscopie sans
lentille » initié par l’association Puya de Raimondi.  Il a été
financé à hauteur de 18000 € qui outre les fonds propres de
l’association, a regroupé des subventions provenant de plusieurs
fondations et organismes ; Fondation de la Maison de la Chimie,
Département du Val de Marne, Comité des Relations Internationales et
des Jumelages de Cachan, Relations Internationales du CEA, Fondation
CFM.

|image5|

L’atelier a été organisé par le professeur Cesar Costa à la Escuela
Politécnica Nacional de Quito du 12 au 14 décembre 2018. C’est dans
son laboratoire qu’a été développé le prototype du
microscope. Différents problèmes dont les délais dans l’obtention des
subventions, les délais dans la livraison des matériels commandés
(cameras web cam de précision) et dans la coordination ont fait que ce
projet ambitieux qui concernait trois universités de trois pays andins
(Bolivie, Equateur, Pérou) n’est pas arrivé à son terme au bout de
deux ans. Quelques mois supplémentaires seront nécessaires pour
pouvoir terminer le prototype et le reproduire. L’atelier a donc porté
en plus du projet microscope sur d’autres projets d’équipements pour
le diagnostic dans la santé à coût soutenable. Ces projets sont en
majorité basés sur la source ouverte (open source) et utilisent les
développements rendus possibles par le numérique : fabrication
additive par impression 3D, détection d’image avec web cam ou
smartphone, l’utilisation de cartes du système Arduino, etc…

|image6|

Les étudiants provenant de quatre pays (13 de l’Equateur, 4 de
Colombie, 3 de Bolivie et 2 du Pérou) après avoir assisté à quelques
séances plénières ont pu se former par petits groupes à la conception
et la construction d’instruments principalement dans le domaine des
diagnostics de santé. Ceux qui ne provenaient pas de Quito ont reçu
des aides pour le voyage et pour l’hébergement.  Le professeur Mirko
Zimic (Universidad Privada Cayetano Heredia) spécialiste du diagnostic
de maladies infectieuses et le professeur Omar Omarchea (Universidad
Privada de Cochabamba) spécialiste de l’optique ont été les principaux
formateurs.  Pour les pays andins les instruments de diagnostic de
maladies infectieuses (type ELISA) coutent très cher et travailler
pour une alternative à coût soutenable est pertinent. En effet, la
tuberculose est devenue à nouveau un fléau pour la Bolivie et le Pérou
(et les autres pays andins) et sa détection à des coûts réduits (pour
la généraliser) une priorité.

.. |image5| image:: /images/bulletin-2019-01/image5.jpg
   :width: 350px
.. |image6| image:: /images/bulletin-2019-01/image6.jpg
   :width: 350px

