BULLETIN PsF 01/2019
####################

:date: 2019-01-30 12:00
:category: Bulletin
:tags: humour

Bulletin Physique sans Frontières (janvier 2019)
================================================

Bonne année à tous et pour la solidarité scientifique internationale

Un peu d’humour ci-dessous pour commencer l’année (dessinateur
Chapatte – Suisse)

.. raw:: html

	 <!-- format spécial pour forcer le retour à la ligne après -->
	 <span style="display: table-cell;">
	 
|image0|

.. raw:: html
	 
	 </span>
	 
Actualités
==========

**Une très bonne nouvelle**, la Société Française d’Optique (SFO) vient
de créer la commission « Optique sans Frontières » qui nous sera étroitement
associée.

Les 3émes Rencontres des Jeunes Chercheurs Africains en France
--------------------------------------------------------------

(Institut Henri Poincaré-IHP) 6-7 décembre 2018, organisées par l’APSA
(Association pour la promotion des Sciences en Afrique - www.scienceafrique.fr
à laquelle vous pouvez adhérer) et cinq membres de la commission
Physique sans frontières ont participé à l’organisation
`Lire la suite ... </articles/2019/janv./30/bulletin-2019-01-a/>`__

Atelier Instrumentation à coût soutenable à la Escuela Politecnica Nacional de Quito
------------------------------------------------------------------------------------

Cet atelier de restitution vient terminer le projet « microscopie sans
lentille » initié par l’association Puya de Raimondi
`Lire la suite ... </articles/2019/janv./30/bulletin-2019-01-b/>`__

Débat sur l’augmentation des frais d’inscription à l’université
---------------------------------------------------------------

Nous aimerions connaître vos réactions sur cette augmentation (master
de 243 à 3770 €, doctorat de 380 à 3770 €)) qui suscite beaucoup de
réactions
`Lire la suite ... </articles/2019/janv./30/bulletin-2019-01-c/>`__


Nouvelles
=========

Polémique sur l’édition scientifique
------------------------------------

Une vidéo pas récente mais très bien faite et qui résume bien le
problème :

Video # datagueule sur l’édition scientifique

.. raw:: html

	 <iframe width="560" height="315" src="https://www.youtube.com/embed/WnxqoP-c0ZE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Le site d'actualités de l'université Paris Sud a publié un article sur
l'aide apportée à la renaissance  de la Licence et au développement
d’un Master de Physique à l'Ecole Normale Supérieure (ENS) de
Port-au-Prince et à l'aide apportée par Physique Frontières qui en
fait en a été le catalyseur:

http://www.actu.u-psud.fr/fr/international/actualites-2018/l-iut-de-cachan-installe-une-chaine-de-solidarite-avec-haiti.html

Science Afrique
---------------

Inscription au bulletin trimestriel sur l’Afrique de l’American
Physical Society (APS) : c’est important pour ceux d’entre nous qui
sont en relation avec des pays africains.

https://aps.us19.list-manage.com/subscribe?u=63e42c583930d9f7a8b637982&id=47beedc3f3

Technologie
-----------

Adaptation directe de lasers sur une imprimante 3D pour gravure et découpe :

https://www.3dnatives.com/endurance-lasers-interview-08012019/


Solidarité
----------

Programme de formation en Science et Technologie pour réfugiés
(Program offers students a refuge in STEM)

https://www.symmetrymagazine.org/article/program-offers-students-a-refuge-in-stem

Conférences
-----------

Afrique (Ghana)
  La cristallographie : un outil pour le développement durable en
  Afrique - Crystallography, a tool for sustainable development in
  Africa

  http://www.pccrafrica.org/
  http://www.pccrafrica.org/afls.html
  
Népal
  Atelier “Femmes en Physique” au Népal - Nepalese school for women in
  physics RCWIPN-2019:

  http://nswip.org.np/category/conference.

Amérique latine

Mexique
  XLI Colloquium Spectroscopicum Internationale (CSIXLI) and the Ist
  Latin‐American Meeting on Laser Induced Breakdown Spectroscopy
  (LAMLIBS) 9-14 juin 2019 à
  Mexico.
  
  http://www.csi2019mexico.com/index.php/programme/sessions-information

Nouveaux Sponsors (en nature) :
-------------------------------

Quantel lasers (Lumibird)
  fourniture de miroirs pour lasers Nd :YAG (projet Bolivie).
  
Optosigma Europe :
  optiques, matériel pour l’optique qui seront fournis sur projets.

Prochain bulletin
-----------------

Dans le prochain bulletin nous publierons un article sur le concours
panafricain pour « Technologie et innovations pour le développement
durable » organisé par l’APSA (Joseph ben Jelloul) avec des collègues
éthiopiens (madame Mariamawit Yonathan Yeshak) dont la remise des prix
a eu lieu à Addis Abeba le 8 décembre. Il y a eu 110 candidats
provenant de l’ensemble de l’Afrique.

.. raw:: html

	 <div style="color: red; margin:1em;">
	 
N’hésitez pas à nous faire part de vos nouvelles

.. raw:: html

	 </div>

.. |image0| image:: /images/bulletin-2019-01/chapatte.jpg
   :width: 450px

