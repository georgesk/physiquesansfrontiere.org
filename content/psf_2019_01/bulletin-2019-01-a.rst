Les 3émes Rencontres des Jeunes Chercheurs Africains en France
##############################################################

:date: 2019-01-30
:category: Rencontres
:tags: conférences

(Institut Henri Poincaré-IHP) 6-7 décembre 2018, organisées par l’APSA
(Association pour la promotion des Sciences en Afrique - www.scienceafrique.fr
à laquelle vous pouvez adhérer) et cinq membres de la commission
Physique sans frontières ont participé à l’organisation :

Le professeur Jérémie Zoueu de l’Institut National Polytechnique
Houphouët-Boigny,(Côte d’Ivoire) a donné une présentation très
intéressante sur « Imagerie multispectrale et diagnostic
microscopique » concernant les travaux effectués dans son laboratoire
à Yamousoukroh.


Outre les doctorants et post doctorants africains plusieurs
professeurs d’université et maitres de conférences étaient
présents. Les trois tables rondes ont été très suivies (domaines :
Changement climatique en Afrique : enjeux scientifiques, Que faire
avec des études scientifiques en Afrique ?  Enseignement supérieur et
recherche en Afrique : quelles attentes ?)

+----------------------+--------------------------+
|   |image1|           |         |image2|         |
+----------------------+--------------------------+

Trois doctorants du Laboratoire Kastler Brossel (LKB), Rodrigo
Cortinas, Gauthier Depambourg, Valentin Métillon (voir images
ci-dessous) sont venus faire une démonstration de travaux pratiques
sur la Physique Quantique nous les remercions d’avoir développé ce TP
et d’avoir pris le temps de venir en faire la démonstration. Une vidéo
devrait suivre ainsi qu’un document didactique.

+----------------------+--------------------------+
|   |image3|           |         |image4|         |
+----------------------+--------------------------+

J’en profite pour parler de quelques physiciens doctorants avec
lesquels j’ai discuté, je m’excuse auprès des autres car il n’y avait
que trop peu de temps pour discuter avec tous les physiciens.

Aliou  Sy (Sénégal) :
  il est en post doc au LAAS (CNRS Toulouse) et travaille dans le
  domaine de l’optique fondamentale (modulation de la lumière). Il
  veut s’impliquer pour améliorer l’enseignement de la physique dans
  son pays.

Maimounia Diouf (Sénégal) :
  elle effectue sa thèse à l’université Aix-Marseille dans le domaine
  du dépôt en couche mince. Elle développe un dispositif à coût
  soutenable et a reçu une subvention de la CE pour organiser un
  atelier sur ce domaine au Sénégal. Dave Lollman spécialiste du
  domaine se mettra en contact avec elle.

Mama Sangaré (Mali) :
  première femme à avoir soutenu une thèse de physique au Mali, elle
  est actuellement au laboratoire Hubert Curien (Institut d'Optique,
  Université Jean-Monnet, CNRS Saint-Etienne)" où elle travaille
  maintenant comme assistante sous la direction de Pierre Chavel. Son
  projet serait de faire de l'imagerie d'échantillons de plantes à des
  fins phytosanitaires au Mali en "microphotographie". Son séjour a
  été soutenu par l’ambassade de France à Bamako.

Urbain Niangoran (Côte d’Ivoire) :
  il effectue un post doctorat à l’université de Toulouse en
  détachement dans une entreprise qui développe un réacteur pour la
  fabrication de la Spiruline. Une anecdote qui montre que le monde de
  la Spiruline est impitoyable ; son entreprise vient d’être détruite
  par un incendie provoqué par le PDG d’une entreprise concurrente !

.. raw:: html

	 <div style="color: red; margin:1em;">
	 
Pour les prochaines éditions des rencontres, l’APSA, réfléchit à
ajouter aux domaines des mathématiques et de la physique, ceux de la
biologie et de la chimie. Cela demandera par contre à augmenter le
nombre de jours des rencontres et conduira donc à des frais
d’organisation plus élevés.

.. raw:: html

	 </div>

.. |image1| image:: /images/bulletin-2019-01/image1.jpg
   :width: 350px
.. |image2| image:: /images/bulletin-2019-01/image2.jpg
   :width: 350px
.. |image3| image:: /images/bulletin-2019-01/image3.jpg
   :width: 350px
.. |image4| image:: /images/bulletin-2019-01/image4.jpg
   :width: 350px
