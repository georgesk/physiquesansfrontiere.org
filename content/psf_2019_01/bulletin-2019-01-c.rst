Atelier Instrumentation à coût soutenable à la Escuela Politecnica Nacional de Quito
####################################################################################

:date: 2019-01-30
:category: Débat
:tags: université

Nous aimerions connaître vos réactions sur cette augmentation (master
de 243 à 3770 €, doctorat de 380 à 3770 €)) qui suscite beaucoup de
réactions, même si probablement elle ne va pas toucher ceux d’entre
vous qui êtes déjà en cours de doctorat. En effet, cette mesure
administrative ne prend pas en compte le problème des transferts
d’argent (en général limités à un certain niveau), la dévaluation qui
touche la monnaie de certains pays (ex : Tunisie), la discrimination
qu’elle introduit entre familles à hauts revenus et revenus
moyens. Par ailleurs, certains des étudiants travaillent pour payer
leurs études, ce qui les amène d’une part à les interrompre
provisoirement et d’autre part à rencontrer des difficultés pour les
payer car les salaires n’augmentent hélas pas au même rythme que les
frais d’inscription. Certaines universités (Lyon par exemple)
n’appliqueront pas cette mesure.

.. raw:: html

	 <div style="color: red; margin:1em;">
	 
Faites-nous part de vos réactions et de votre sentiment

.. raw:: html

	 </div>
