BULLETIN PsF 02/2019
####################

:date: 2019-03-01 12:00
:category: Bulletin
:tags: 

Bulletin Physique sans Frontières (février 2019)
================================================

Le bulletin de ce mois est presque entièrement constitué d’informations que j’ai considérées suffisamment intéressantes pour vous en faire part.

Culture scientifique :
----------------------

Femmes et tableau périodique
https://www.nature.com/articles/d41586-019-00287-7

Interview de Gerard Mourou prix Nobel de Physique 2018 (The conversation)
-------------------------------------------------------------------------

https://theconversation.com/conversation-avec-gerard-mourou-prix-nobel-de-physique-2018-104338

Vulgarisation :
---------------
livre de Julien Bobroff « mon grand Mécano Quantique » (The conversation)

.. raw:: html

	 <!-- format spécial pour forcer le retour à la ligne après -->
	 <span style="display: table-cell;">
	 
|image0|

.. raw:: html
	 
	 </span>
	 

https://theconversation.com/zero-zero-toujours-zero-la-plus-belle-decouverte-de-la-physique-quantique-111482

Lancement du premier bulletin de l’American Physical Society (APS) consacré à la physique en Afrique
----------------------------------------------------------------------------------------------------

https://gallery.mailchimp.com/63e42c583930d9f7a8b637982/files/0a83cec3-8189-4c79-9f6f-a69e79fc51ea/1._APN_1st_issue_Editorial_APS_Kirby.01.pdf

Recyclage déchets plastiques :
------------------------------

https://www.youtube.com/watch?v=c9gL5QGUEhI&feature=youtu.be

Feuilleton de l’augmentation des droits d’inscription en master et doctorats des étrangers extracommunautaires :
----------------------------------------------------------------------------------------------------------------

Tout d’abord une bonne nouvelle, cette augmentation a été très récemment
annulée pour les doctorats, les doctorants étrangers exemptés de la hausse
des frais d’inscription à l’université:
https://www.lemonde.fr/societe/article/2019/02/24/les-doctorants-etrangers-exemptes-de-la-hausse-des-frais-d-inscription-a-l-universite_5427527_3224.html

Lettre ouverte à Cédric Villani :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

https://blogs.mediapart.fr/lionel-schwartz/blog/100219/frais-d-inscription-des-etudiants-etrangers-lettre-ouverte-cedric-villani

https://blogs.mediapart.fr/eric-fassin/blog/250219/mais-pourquoi-diable-augmenter-les-frais-d-inscription-des-etudiants-extra-europeens

Problématique de l’eau au Sahel :
---------------------------------

Surveillance des nappes phréatiques (Burkina Faso) projet à financement
participatif suivi par **Arouna Darga** qui est le co-directeur de la
start-up Burkinabé DargaTech. Il est aussi chercheur et enseignant
dans le domaine de l’énergie solaire à l’Université Pierre et Marie Curie
(France), représentant du Africa Network for Solar Energy (ANSOLE)
en France,
https://www.helloasso.com/associations/eau-fil-du-soleil/collectes/turning-sun-into-water

L’exploitation de l’eau des nappes phréatiques à l’aide de forages équipés
de pompes manuelles (type India Mali) ou solaires est un moyen de choix
pour pouvoir disposer d’eau « propre » dans beaucoup de pays du Sahel.
Cependant cela demande une surveillance de ces nappes phréatiques.
Le projet [Turning sun into warter](https://www.helloasso.com/associations/eau-fil-du-soleil/collectes/turning-sun-into-water)
qui demande un financement participatif a été créé dans ce but.

Concours Orange Fablabs :
-------------------------

En fin 2018 s’est terminé le concours "I Make 4 My City" de la Fondation
Orange, il a réuni cette année 16 équipes issus de 8 pays. L’AUF annonce
que le Lisungi Fablab (RDC) a remporté le Grand prix du jury avec son
projet intitulé le "lavabo intelligent", une solution numérique pour
réduire les risques de contamination liées à l'usage des robinets de
lavabo.
https://www.auf.org/afrique-centrale-grands-lacs/nouvelles/actualites/lisungi-fablab-vainqueur-concours-international-i-make-4-my-city/

Colonisation et réchauffement climatique (en anglais) :
-------------------------------------------------------

https://www.theguardian.com/environment/2019/jan/31/european-colonization-of-americas-helped-cause-climate-change

Conférence de Tarek Loubani
---------------------------

**Tarek Loubani**  Médecin palestinien exerçant au Canada (Professeur à
l’université de Western Ontario London Canada) donnera une conférence
suivie d’un débat à l’ENS ULM salle Dussane   le 7 mars 2019 (18H-20H) :
**“Delivering Health Care under Fire in Gaza : The problems and the promise”**

Le Dr Loubani développe des équipements à coût soutenable pour la médecine : stéthoscopes imprimés en 3D, mesureur d’oxygénation, otoscope, tourniquet, etc… https://glia.org/stethoscope/

|image1|


.. |image0| image:: /images/bulletin-2019-02/bobroff-mecano.jpg
   :width: 450px

.. |image1| image:: /images/bulletin-2019-02/loubani-stethoscope.jpg
   :width: 700px

