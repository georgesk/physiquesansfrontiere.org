NOUVELLES
#########

:date: 2021-03-30 12:01
:category: Annonces
:tags: nouvelles

I- Le projet ELABORE
====================

Le projet **ELABORE** du professeur **Emmanuel Maisonhaute** qui a été soumis
à l’appel d’offre conjoint CNRS – IRD « Sciences Frugales » a été
accepté. **Physique sans Frontières**, **Chimistes sans Frontières** et
**Puya International** y sont associées. Le projet consiste à développer un
dispositif à coût abordable basé sur l’électrochimie pour des
applications analytiques. Parmi les applications envisagées : la
détection de faux médicaments et la détection de métaux lourds dans
l’eau. Le développement s’effectuera en coopération avec des
scientifiques béninois. Le projet a une durée de deux ans. Nous
espérons tous qu’il sera un succès.

|image4|
Il est basé sur le développement d’un potentiostat à bas coûts en
coopération avec **Rémy Campagnolo**,
nouveau président de l’association Puya International. En bref, le
potentiostat est un dispositif «électronique permettant de générer les
tensions nécessaires pour les électrodes». Ce dispositif comme on peut
le voir sur l’image précédente est miniaturisé, ce qui permettra des
applications de terrain. De même sur cette illustration, les rôles des
différents partenaires du projet sont précisés.

|hr|

II - Arouna Darga et François Piuzzi à SOLCIS
=============================================

Rencontre avec **Daniel Lincot** (ex-directeur scientifique de l’Institut
Photovoltaïque d'Île de France - IPVF) au siège de la nouvelle
structure qu’il développe dans les anciens locaux du Laboratoire de
l’accélérateur linéaire à l’université d’Orsay. Cette structure dont
le nom est **SOLCIS** (Solaire Citoyen et Solidaire), s’occupera de
développements dans le domaine du solaire. Daniel a été intéressé par
notre approche de formation avec notamment les « ateliers de Physique
sans Frontières », ce qui après signature d’un accord devrait
permettre l’organisation de nos formations dans ses locaux.

|hr|

III- Information transmise par Dave Lollman (Vice_président)
============================================================

Le deuxième Colloque sur les Objets et Systèmes Connectés (COC'2021)
se déroulera les 29, 30 et 31 mars prochains en distanciel à partir de
Marseille. Le programme définitif est désormais disponible ici :
https://coc2021.sciencesconf.org Il sera organisé en distanciel dans
un environnement virtuel et immersif en 3D "Calypso 3D"
(https://immersive-colab.fr/calypso3d).

|hr|

IV - Fundraising for Lebanon
============================

Information transmise par **Martin Gastal** CERN

An article has been published in the CERN Bulletin :
http://bulletinserv.cern.ch/emails/archive/551/

https://home.cern/fr/news/news/knowledge-sharing/cms-launches-initiative-support-lebanese-colleagues?utm_source=Bulletin&utm_medium=Email&utm_content=2021-03-25E&utm_campaign=BulletinEmail

Il y a un site dédié pour le projet qui contient des explications pour
faire des donations.
*We also created a dedicated website to support the Fundraiser and explain how to make a donation*.

http://fundraiser-lebanon.web.cern.ch/

|hr|

V - Appel provenant de l’École Polytechnique Fédérale de Lausanne (EPFL)
========================================================================

Cet appel concerne le programme « Excellence in Africa » – Programme
100 doctorats pour l’Afrique : lancement de l'appel à propositions 2021.

Le `centre EXAF <https://africancitiesjournal.us18.list-manage.com/track/click?u=3d2e0d7edf9a799f431eb2a2e&id=2ed0dc9862&e=5c377be2e7>`__
amorce aujourd’hui un nouveau programme, appelé « 100
doctorats pour l’Afrique », dans le cadre de l’initiative
**Excellence in Africa** conjointement lancée par
l’`UM6P <https://africancitiesjournal.us18.list-manage.com/track/click?u=3d2e0d7edf9a799f431eb2a2e&id=f656828f23&e=5c377be2e7>`__
(Université Mohammed VI Polytechnique au Maroc) et par
l’`EPFL <https://africancitiesjournal.us18.list-manage.com/track/click?u=3d2e0d7edf9a799f431eb2a2e&id=07073a3ba4&e=5c377be2e7>`__
(École polytechnique fédérale de Lausanne, en Suisse).

Le programme 100 doctorats pour l’Afrique offrira à des diplômé-e-s de
master en sciences et en ingénierie l’opportunité de réaliser une
thèse de doctorat en Afrique. Les bénéficiaires de ce programme seront
supervisé-e-s conjointement par un-e professeur-e d’une institution
africaine et par un-e professeur-e de l'EPFL. Les doctorant-e-s
accompliront leurs études doctorales et leurs travaux de recherche
dans leur institution en Afrique, avec également de courtes périodes à
l'EPFL et à l'UM6P (cours d'été et de printemps, ateliers et
conférences).

Le premier appel à candidatures est désormais ouvert, avec une
**échéance** fixée au **20 avril 2021**. C’est avec un grand plaisir que nous
recevrons les candidatures des diplômé-e-s de master africain-e-s
affichant un parcours académique excellent et ayant le soutien d’un
professeur ou d’une professeure au sein de leur institution en
Afrique.

Nous vous invitons à partager cette information sur vos réseaux et
parmi vos contacts. Vous trouverez
`ici <https://africancitiesjournal.us18.list-manage.com/track/click?u=3d2e0d7edf9a799f431eb2a2e&id=a7de3541df&e=5c377be2e7>`__
un flyer que vous pouvez faire
circuler dans vos réseaux et ici les directives de
candidature. N’hésitez pas à nous contacter pour plus d’informations
(exaf@epfl.ch).

|hr|

VI -UNIVERSCIENCE et IRD
========================

**31 mars 2021, Forum Recherche innovations Africa2020 : Covid-19 : la riposte des nations africaines**

Nouveau temps de la saison Africa2020 à Universcience, le Forum de la
recherche et de l’innovation organisé en partenariat avec l’IRD,
abordera les stratégies africaines de mobilisation des institutions,
du monde de la recherche et des technologies face à la pandémie comme
les réponses apportées aux besoins des populations. Ouvert par N’Goné
Fall, commissaire générale de la saison Africa2020 et Bruno Maquart,
président d’Universcience, ce forum donnera la parole aux acteurs du
continent africain : jeunes entrepreneurs innovants, médecins,
chercheurs, virologues...

`Cliquez ici pour accéder au programme <http://com.mailing.universcience.fr/c/6/?T=MjQ3MTAyNTk%3AcDEtYjIxMDg5LWNkNDI0NDg3YTNmYjRmNTNiOGQwZDE2NjU0N2EyNzBm%3AbGFuZ2xhaXMuY2F0aGVyaW5lQGljbG91ZC5jb20%3AY29udGFjdC03YTdkY2Q3MjA1NTJlOTExYTk1YjAwMGQzYTQ1NDMwOC1kNDdlM2ZjNGM5MmE0MmQxOTMxY2UxMzgxMDg3NWYzZA%3AZmFsc2U%3AMg%3A%3AaHR0cHM6Ly93d3cuY2l0ZS1zY2llbmNlcy5mci9mci9hdS1wcm9ncmFtbWUvYW5pbWF0aW9ucy1zcGVjdGFjbGVzL2NvbmZlcmVuY2VzL2NvdmlkLTE5LWxhLXJpcG9zdGUtZGVzLW5hdGlvbnMtYWZyaWNhaW5lcy8_X2NsZGVlPWJHRnVaMnhoYVhNdVkyRjBhR1Z5YVc1bFFHbGpiRzkxWkM1amIyMCUzZCZyZWNpcGllbnRpZD1jb250YWN0LTdhN2RjZDcyMDU1MmU5MTFhOTViMDAwZDNhNDU0MzA4LWQ0N2UzZmM0YzkyYTQyZDE5MzFjZTEzODEwODc1ZjNkJmVzaWQ9MTgwOTYyNzMtMGM4OC1lYjExLWE4MTItMDAwZDNhMmM4ODJl&K=ywwnAfrwt9-FIL_BDtbHtQ>`__

|image5|
|clearboth|


.. |image4| image:: /images/bulletin-2021-03/image004.png
   :width: 475px

.. |image5| image:: /images/bulletin-2021-03/image005.png
   :width: 475px

.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
   
	   
      
