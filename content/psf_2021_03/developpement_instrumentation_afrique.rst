DÉVELOPPEMENT DE L’INSTRUMENTATION EN AFRIQUE
#############################################

:date: 2021-03-30 12:01
:category: Instrumentation
:tags: Afrique

Conception d’un dispositif pour la surveillance cardiaque à distance
====================================================================

\... **au Cameroun par Arthur Zang qui a reçu pour cela le prix Rolex en 2016**.

https://elpais.com/elpais/2021/01/20/eps/1611160552_328022.html
Traduction de l’article de EL Pais :

Le Cardio Pad est un appareil conçu par Arthur Zang, lauréat du Prix
Rolex 2014, qui permet aux travailleurs de la santé de procéder à des
examens cardiaques dans un pays qui connaît une grave pénurie de
cardiologues.

|image21|

Le Cameroun est un pays de plus de 26 millions d'habitants avec
seulement 50 cardiologues.  Dans un continent, le continent africain,
où 22 % des décès annuels sont causés par des problèmes cardiaques,
l'accès aux soins médicaux est rendu encore plus difficile par les
distances à parcourir pour se rendre à l'hôpital. La plupart de ces
spécialistes au Cameroun sont concentrés dans ses deux grandes villes,
Douala et Yaoundé, où les patients doivent se déplacer pour être
examinés. Dans de nombreux cas, ils sont incapables d'effectuer ces
voyages ou, lorsqu'ils le font, il est trop tard.

L'ingénieur en informatique Arthur Zang a pris conscience de ce
problème lorsqu'un cardiologue lui a présenté un défi : créer un
appareil portable, fiable et facile à manipuler pour effectuer des
examens cardiaques dans les zones rurales du pays, sans que le patient
ou un spécialiste ait à parcourir des centaines de kilomètres. "Le
premier problème était qu'il n'y avait pas de spécialiste de cette
technologie en Afrique", explique Zang. "J'ai dû m'instruire à la
maison, en utilisant des logiciels éducatifs gratuits sur Internet",
poursuit-il. Avec son équipe, il a conçu le logiciel et le matériel de
ce dispositif révolutionnaire. De la taille d'une tablette numérique
classique, et connecté à des électrodes, il permet au personnel de
santé non spécialisé en cardiologie d'effectuer différents tests, dont
des électrocardiogrammes, et de les envoyer immédiatement via Internet
pour qu'ils soient examinés par un cardiologue. Avec une autonomie de
sept heures, il peut également effectuer des tests dans des endroits
qui n'ont pas de réseau électrique.

En 2016, le Cardio Pad a finalement été mis en vente et 200 unités
sont maintenant utilisées dans 200 |image22| 
hôpitaux au Cameroun et aux Comores. Ce projet a valu à Zang de
figurer parmi les lauréats des Prix Rolex à l'initiative 2014, une
reconnaissance par laquelle la firme suisse met en avant et soutient
des initiatives qui améliorent la vie des gens dans le monde entier.
"Le problème de la pénurie de spécialistes est général, donc je pense
que cet appareil a le potentiel pour être utilisé partout dans le
monde", dit Zang, qui vise à faire utiliser son Cardio Pad dans plus
de 100 hôpitaux à travers le Cameroun pour le moment.
|clearboth|
Zang teste un patient à l'aide du Cardio Pad. ©Rolex

|image23|
Le prochain obstacle auquel il a dû faire face était le financement.
"J'ai dû demander à ma mère de solliciter un prêt auprès de la
banque", se souvient l'ingénieur camerounais. Une fois les barrières
tombées, Zang a pu créer un prototype, qu'il a appelé le **Cardio Pad**.

*Traduit avec www.DeepL.com/Translator (version gratuite) et corrigé par François Piuzzi*
|clearboth|
|hr|

TUNISIE
=======

**La Tunisie lance son premier satellite**. La Tunisie est le premier
pays du Maghreb et le sixième en Afrique à lancer un satellite
fabriqué à 100 % localement ; il est destiné à l’Internet des objets
connectés, explique
`L’Orient-Le Jour <https://www.lorientlejour.com/article/1256159/la-tunisie-1er-pays-du-maghreb-a-lancer-un-satellite-fabrique-100-localement.html>`__.
La fusée russe a décollé du Kazakhstan.



.. |image21| image:: /images/bulletin-2021-03/image021.jpg
   :width: 310px
	   
.. |image22| image:: /images/bulletin-2021-03/image022.jpg
   :width: 250px
	   
.. |image23| image:: /images/bulletin-2021-03/image023.jpg
   :width: 250px
	   
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
