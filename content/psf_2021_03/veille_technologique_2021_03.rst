VEILLE TECHNOLOGIQUE
####################

:date: 2021-03-30 12:01
:category: Annonces
:tags: technologie frugale

DÉTECTEUR DE CO2 : Élaboré par le professeur Pierre Carles de Jussieu
=====================================================================

|image19|

Le professeur Pierre Carles a réalisé ce dispositif qui permet de
suivre la concentration de CO2. Tous les renseignements nécessaires à
sa réalisation se trouvent dans le fichier Github sur le site
Instructables.
https://www.instructables.com/CO2-Monitoring-As-an-Anti-Covid19-Measure/

|hr|

EXPÉRIENCE À FAIRE À LA MAISON
==============================

VISUALISATION DE CRAQUELURES DANS DE L’HYDROGEL.
**Expérience à faire à la maison** avec une caméra vidéo,
des miroirs inclinés, un éclairage
arrière pour produire des vues en 3D des craquelures en formation dans
de l’hydrogel immergé.

|image20|
|clearboth|
https://physics.aps.org/articles/v14/29?utm_campaign=weekly&utm_medium=email&utm_source=emailalert

|hr|

SONOLITHOGRAPHY : Une nouvelle technique ?
==========================================

https://www.youtube.com/watch?v=aRCVCQp-fno&t=2s

Jenna M. Shapiro et al, Sonolithography: In‐Air Ultrasonic Particulate
and Droplet Manipulation for Multiscale Surface Patterning, Advanced
Materials Technologies (2020).
`DOI: 10.1002/admt.202000689 <http://dx.doi.org/10.1002/admt.202000689>`__

|hr|

RÔLE POSSIBLE DE LA PHOTONIQUE POUR AUGMENTER LES RENDEMENTS POUR L’AGRICULTURE
===============================================================================

Des LEDs, bleues, rouge, UV pour l’agriculture (Pays Bas). Des
recettes ayant pour ingrédient les longueurs d’ondes de la lumière et
des installations seront créés dans d’autres pays (que la Hollande ).
L’action **Grow** a été développée en partenariat avec l’Université de
Wageningen et la Rabobank. C’est un mélange entre l’art et la science
et en plus cela a l’air d’améliorer les rendements des cultures !

https://www.theguardian.com/environment/2021/mar/08/shining-through-dutch-artist-paints-farming-in-a-new-light-to-boost-crops-aoe

Ligth recipes for growing crops: forty new light recipes and
installations will be created in other countries, once Covid
allows. Grow was developed in partnership with
`Wageningen University <https://www.wur.nl/en/wageningen-university.htm>`__
and `Rabobank <https://www.rabobank.com/en/home/index.html>`__

Crédit : Daan Roosegaarde/Studio Roosegaarde



.. |image19| image:: /images/bulletin-2021-03/image019.jpg
   :width: 470px
	   
.. |image20| image:: /images/bulletin-2021-03/image020.jpg
   :width: 470px
	   
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
