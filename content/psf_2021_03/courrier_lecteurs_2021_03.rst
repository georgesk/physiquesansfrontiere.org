COURRIER DES LECTEURS
#####################

:date: 2021-03-30 12:01
:category: Débat
:tags: optique

Nous avons reçu des commentaires et précisions de deux de nos membres
Pierre Chavel et Jean Michel Friedt au sujet de deux informations
publiées sur le dernier Bulletin (Janvier -Février). Les voici !

Hologrammes ? Une mise au point de Pierre Chavel
================================================

Un article du bulletin de janvier-février signalait par un lien sur
YouTube les commentaires d'une association concernant une "pyramide
3D" de présentation d'images virtuelles, qui peut être mise en œuvre
de façon spectaculaire. Un point d'interrogation dans le titre, que
nous reprenons ici, mettait en doute la nature holographique de ces
images : en effet, elles n'ont rien d'holographique ! Le vocabulaire
n'appartient à personne. Le four de mon boulanger se trouvait naguère
dans un fournil, vieux mot français parfaitement spécifique, ou dans
un atelier, maintenant le four est dans un laboratoire : le
vocabulaire a perdu en précision, mais le pain n'a pas changé.

    Pour en venir à notre sujet, aucune ambiguïté sur l'antériorité, sans
    Dennis Gabor le mot "hologramme" n'aurait pas existé au départ (1948).

Donc, les physiciens se sont laissé emprunter l'usage du mot
"hologramme" pour désigner une technique d'illusions d'optique à base
de lames de verre orientées à 45° environ de la direction
d'observation par le public, et qui peut suivant l'objet qui se
réfléchit sur la glace former des images soit 2D (écrans vidéo, avec
l'avantage de la transmission vidéo possible) soit 3D (scène réelle
convenablement éclairée dans une salle à côté). Cela existait
largement au XIXe siècle, c'était connu au XVIe mais la qualité des
glaces à l'époque ne permettait que des réalisations peu
spectaculaires. La technique est désignée sous différents noms, on
trouve notamment "fantôme de Pepper" (Pepper ghost) d'après un des
principaux promoteurs du XIXe siècle (réf 1). On en trouve un bon
exemple au château de Vaux-le-Vicomte (momentanément fermé pour la
raison que tout le monde sait ; du moins cette attraction était-elle
présente il y a une dizaine d'années, y est-elle toujours ?)

Même s'ils peuvent ressentir une frustration compréhensible, les
physiciens n'ont pas les moyens d'empêcher l'usage du mot hologramme à
propos d'une technique qui, vu de très loin, présente une certaine
similarité avec l'invention originale. En revanche, il leur appartient
de bien faire la distinction et de la rappeler à chaque
occasion. Notre collègue Yvon Renotte, de l'Université de Liège,
trouvant illégitime l'emprunt du mot "hologramme", mène de façon très
pédagogique une campagne permanente à ce sujet (réf 2, on pourra aussi
consulter, entre autres, la réf 3).

Une autre réalisation remarquable mérite d'être mentionnée ici : aux
Olympiades de Physique France (SFP/UdPPC), en 2019-2020, une équipe du
lycée Jacques Cartier de Saint-Malo avait commencé par reproduire
l'illusion "fantôme de Pepper" et utilisé le mot "holographie". Rendue
attentive à l'ambiguïté et conseillée par un chercheur de l'ENSSAT de
Lannion, elle a eu le mérite de développer un banc d'holographie très
astucieux et de montrer au jury à la fois de "faux" hologrammes -
vrais fantômes de Pepper - et de "vrais" hologrammes au sens original,
appliqués à la métrologie. La lecture de la référence 4 est vivement
recommandée.

Références :
------------

1. voir le bon article Fantôme-de-Pepper sur Wikipedia français et la page correspondante en anglais
2. https://dailyscience.be/13/01/2020/non-les-hologrammes-ne-se-produisent-pas-sur-scene/
3. `https://www.plv-hologramme.fr/post/hologramme-définition <https://www.plv-hologramme.fr/post/hologramme-définition>`__
4. https://www.olymphys.fr/public/fichiers/memoires/27-eq-W-memoire-Comment%20utiliser%20l'holographie%20de%20precision%20pour%20caracteriser%20une%20deformation%20%20.pdf

|hr|

Précisions et commentaires de Jean Michel Friedt sur la caractéristique « architecture ouverte que j’avais associée à **Arduino** et **Raspberry Pi**

Jean Michel Friedt : Raspberry Pi, une architecture ouverte ?
=============================================================

La mode actuelle de caser l’acronyme « ouverte » (open) à toutes les
sauces ne doit pas cacher la réalité des fonctionnalités offertes par
une technologie (voir sur ce point l’excellent article de Harald Welte
concernant l’initiative américaine d’ouvrir les technologies
implémentant la 5G à
`https://www.ntia.gov/files/ntia/publications/harald_welte_-_sysmocom_02102021.pdf <https://www.ntia.gov/files/ntia/publications/harald_welte_-_sysmocom_02102021.pdf>`__
qui réfère au « open-washing » sur le sujet. A titre d’illustration,
les plates-formes Rapsberry Pi, et en particulier la dernière Raspberry
Pi4, est tout sauf ouverte : absence de schéma (et donc évidemment du
circuit routé), documentation laconique et fausse, extraits de codes
binaires incorporés sans en informer l’utilisateur lors du démarrage
de la carte (First Stage Bootloader) et pour la gestion du
co-processeur graphique... Le premier point nous handicape
actuellement dans un développement autour de la version embarquée de
la Raspberry Pi4 – Compute Module 4 – pour laquelle seules les
interfaces sont documentées, comme le ferait tout circuit propriétaire
dont le vendeur veut garder les fonctionnalités secrètes. Le second
point a été identifié alors que nous avions choisi, à notre désarroi à
la pratique, la Raspberry Pi4 comme plateforme pédagogique à
distribuer aux étudiants en cette année d’enseignement à
distance. Tout se passe bien tant que nous restons sur des niveaux
d’abstraction élevés tel qu’exécuter GNU Radio pour du traitement de
signaux radiofréquences sur la plateforme embarquée (« On ne compile
jamais sur la cible embarquée », Hackable Avril-Juin 2021, à
http://jmfriedt.free.fr/hackable_buildroot.pdf) mais pour ce qui est
d’enseigner le développement de pilotes à l’interface entre le
matériel et le système d’exploitation, la documentation laconique et
fausse (transparents 14 et 15 de
http://jmfriedt.free.fr/master1ese1_10_2021.pdf) anéantit toute
perspective pédagogique sur cette plateforme concernant la
programmation bas-niveau : il suffit de s’en convaincre de feuilleter
les quelques 164 pages de documentation du composant Broadcom BCM2711
(https://datasheets.raspberrypi.org/bcm2711/bcm2711-peripherals.pdf)
au coeur de la Raspberry Pi4 par rapport aux 4040 pages du manuel du
STM32MP157 disponibles à
https://www.st.com/resource/en/reference_manual/dm00327659-stm32mp157-advanced-armbased-32bit-mpus-stmicroelectronics.pdf.

Jean Michel Friedt : la bibliothèque Arduino
============================================

Notre reproche à l’encontre de la bibliothèque Arduino ne porte pas
tant sur son ouverture incontestable qui permet de justement en
identifier les défauts, que par la faible volonté de ses utilisateurs
d’en appréhender le fonctionnement en vue d’en dépasser les
limitations. Nous avions argumenté dans
http://jmfriedt.free.fr/cetsis2017_arduino.pdf (CETSIS 2017) que cette
bibliothèque est peu efficace et illustrions l’exemple d’étudiants qui
voulaient augmenter la puissance de calcul de la plateforme numérique
au lieu d’exploiter un code efficace en s’affranchissant des lenteurs
de la bibliothèque Arduino. Appréhender les ressources nécessaires (de
puissance de calcul, de consommation énergétique, d ‘encombrement voir
de coût) sont des éléments décisifs du succès d’un projet
d’instrumentation embarquée. Une compréhension des mécanismes
sous-jacents à la génération du code exécuté sur la plateforme
embarquée est ainsi incontournable. Les ressources d’enseignement sur
les microcontrôleurs habituellement utilisés pour exécuter Arduino
sont disponibles à http://jmfriedt.free.fr/#l3ep


.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
