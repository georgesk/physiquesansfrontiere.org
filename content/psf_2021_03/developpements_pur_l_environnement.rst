DÉVELOPPEMENTS POUR L’ENVIRONNEMENT
###################################

:date: 2021-03-30 12:01
:category: Environnement
:tags: nouvelles

NOUVEAUX OUTILS POUR L’AGRICULTURE
==================================

    New tools for agriculture

Un article publié sur le site de la BBC sur l’impact de nouveaux
outils numériques sur l’agriculture au Bénin et notamment les **drones**.

https://www.bbc.com/news/business-56195288

"Une ferme aura besoin de différents types de robots", explique le
professeur Chowdhary. "Certains seront très petits... d'autres seront
grands, peut-être même aussi grands qu'une moissonneuse- batteuse. Il
y aura un système autonome qui coordonnera cette équipe de robots,
leur indiquant ce qu'ils doivent faire pour accomplir différentes
tâches." En plus des robots, le professeur Chowdhary affirme que les
drones seront de plus en plus utilisés.

"Les drones sont vraiment bons pour couvrir beaucoup d'espace",
dit-il. "Ils peuvent aller quelque part et pulvériser quelque chose,
ou prendre une photo, très rapidement".

Les partisans de la technologie dans l'agriculture notent également
que ces innovations peuvent être utilisées au profit du monde en
développement. TechnoServe https://www.technoserve.org, par exemple,
est une organisation américaine de développement international à but
non lucratif qui utilise la télédétection, la cartographie par drone,
l'apprentissage automatique et les données satellitaires, dans le but
de stimuler la production de noix de cajou dans la nation
ouest-africaine du Bénin.

|image6|

Les noix de cajou représentent 8 % des recettes d'exportation du
pays. TechnoServe aide les agriculteurs à savoir où planter leurs
arbres et à augmenter la quantité et la qualité de leurs
récoltes. L'organisation a déjà prévu de reproduire le projet dans
toute l'Afrique de l'Ouest et au Mozambique. Le directeur de
TechnoServe, Dave Hale, explique qu'il est possible "d'identifier [les
sites pour] les exploitations de noix de cajou avec un haut niveau de
précision". "[Et] grâce à des pratiques agricoles améliorées, les
agriculteurs augmentent alors leur productivité et leurs revenus."

À l'échelle mondiale, la pandémie de coronavirus, et les rayons vides
des supermarchés à divers points de fermeture, ont accru les
inquiétudes concernant les pénuries alimentaires. Certaines
entreprises technologiques se tournent vers la technologie pour aider
à apaiser ces craintes.

*Traduit avec www.DeepL.com/Translator (version gratuite)*

|hr|

CONSTRUCTION D'ÉCOLES PAR IMPRESSION 3D
=======================================

Projet d’une construction d’une école à Madagascar par impression 3D grâce
à l’ONG « **Thinking huts** »

https://www.zmescience.com/science/an-ngo-is-building-the-worlds-first-3d-printed-school-in-madagascar/?goal=0_3b5aad2288-c9ac827ea7-242800609

Pourquoi parler de construction d’école ici ? Cette action s’inscrit
dans la relance de l’éducation pour les pays à faibles ressources, ce
qui passe en particulier par la réalisation rapide de bâtiments
fonctionnels avec un coût soutenable et un meilleur respect de
l’environnement. C’est l’ONG « Thinking Huts » qui a lancé cette
action. Il faudra attendre sa réalisation pour juger de son intérêt.

« L'architecte du projet, Amir Mortazavi, a expliqué à Fast Company
les avantages de l'approche de l'impression 3D - gain de temps et
réduction des coûts. "Nous pouvons construire ces écoles en moins
d'une semaine, y compris les fondations et tous les travaux
d'électricité et de plomberie que cela implique. Quelque chose comme
cela prendrait généralement des mois, voire plus", a-t-il fait valoir.

Selon M. Mortazavi, les nœuds peuvent être combinés pour former des
groupes de pièces qui agrandissent l'espace ou rester des pièces
individuelles à des fins éducatives. "Nous pouvons avoir des salles de
classe pour différents groupes d'âge, des laboratoires scientifiques,
des laboratoires informatiques, des logements pour les enseignants,
pour les étudiants, des nœuds de musique, des salles de beaux-arts",
a-t-il expliqué. Le premier nœud sera construit en collaboration avec
l'université.

Comme d'autres bâtiments imprimés en 3D, l'école sera construite à
l'aide d'un processus mécanique qui produit des couches lisses de
matériau semblable à du béton qui durcit pour former la structure, y
compris des espaces pour les services publics, les fenêtres et les
portes. La construction sera ainsi beaucoup moins coûteuse qu'avec les
méthodes conventionnelles, ce qui permettra également de résoudre les
difficultés locales liées à la recherche de main-d'œuvre qualifiée
. L'école a été conçue par le Studio Mortazavi, un cabinet
d'architecture basé à San Francisco et à Lisbonne, en alliance avec
Thinking Huts. L'ONG a spécifiquement choisi Madagascar pour le projet
pilote, parmi d'autres pays, en raison de la stabilité de ses
perspectives politiques, de son potentiel en matière d'énergies
renouvelables et de son besoin d'infrastructures éducatives
supplémentaires. »

L'école sera située sur le campus d'une université à Fianarantsoa, une
ville du centre-sud de Madagascar. Elle a une conception modulaire
semblable à un nid d'abeille, avec des nœuds qui peuvent être reliés
entre eux - chacun comprenant une chambre avec deux salles de bain et
un placard.  Un seul nœud sera construit pour l'instant pour l'école
et d'autres pourront être ajoutés ultérieurement.

|image7|
|clearboth|
Crédit: Studio Mortazavi/Thinking Huts.

|image8|
L'imprimante a été fournie par Hyperion Robotics, une entreprise
finlandaise spécialisée dans les solutions d'impression 3D pour le
béton armé. Les murs seront constitués de couches d'un mélange de
ciment spécial qui, selon Thinking Hats, dégage moins
d'émissions. Quant au toit, aux portes et aux fenêtres, ils seront
fabriqués localement. L'ensemble de la construction peut être réalisé
en moins d'une semaine.

*Traduit avec www.DeepL.com/Translator (version gratuite)*.
|clearboth|

.. |image6| image:: /images/bulletin-2021-03/image006.png
   :width: 300px
	   
.. |image7| image:: /images/bulletin-2021-03/image007.png
    :width: 470px
    :alt: photo de « nœuds »

.. |image8| image:: /images/bulletin-2021-03/image008.jpg
   :width: 300px
	   

.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
