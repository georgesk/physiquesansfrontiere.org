COVID ET PHYSIQUE
#################

:date: 2021-03-30 12:01
:category: Annonces
:tags: covid

**Avancées de la recherche (en l’occurrence c’est plutôt Physico Chimie et COVID)**\ :
La détection de la COVID 19 à l’aide d’un dispositif basé
sur l’électrochimie, les nanosciences et le smartphone a fait l’objet
d’un projet Européen Horizon 2020, le projet **CORDIAL** (voir les
partenaires sur l’illustration suivante). Remarquons que c’est en
partie grâce à la miniaturisation que l’électrochimie a vu ses
applications se développer avec les possibilités de mesures sur le
terrain.

|image9|

https://www.whatsupdoc-lemag.fr/article/de-lille-marseille-des-chercheurs-font-le-pari-des-tests-covid-sur-smartphone

|image10|
|clearboth|
|image11|
|clearboth|
Mode d’emploi du dispositif

Étude de l’efficacité de filtration de gouttelettes des masques COVID
=====================================================================


L'impact des gouttelettes sur la surface du masque est enregistré à 20
000 images par seconde. Ces images de séquence temporelle de l'impact
des gouttelettes sur un masque à une, deux et trois couches montrent
que le nombre total de gouttelettes qui traversent est nettement plus
élevé pour le masque à une seule couche par rapport au masque à deux
couches, tandis qu'une seule gouttelette pénètre à travers le masque à
trois couches.

*Crédit : Basu et al, 2021 ; Traduit avec www.DeepL.com/Translator (version gratuite)*

https://medicalxpress.com/news/2021-03-three-layered-masks-effective-large-respiratory.html

|image12|
The droplet impacting on the mask surface is recorded at 20,000 frames
per second. These time sequence images of droplet impingement on a
single-, double-, and triple-layer masks show the total number count
of atomized droplets is significantly higher for the single-layer mask
in comparison with the double-layer mask, while only a single droplet
penetrates through the triple-layer mask.

*Credit: Basu et al, 2021*

|clearboth|

.. |image12| image:: /images/bulletin-2021-03/image012.jpg
   :width: 470px
	   

.. |image9| image:: /images/bulletin-2021-03/image009.jpg
   :width: 470px
	   
.. |image10| image:: /images/bulletin-2021-03/image010.jpg
   :width: 470px
	   
.. |image11| image:: /images/bulletin-2021-03/image011.jpg
   :width: 470px
	   
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
	   
