ENSEIGNEMENT ET VULGARISATION, CABINET DE CURIOSITÉS
####################################################


:date: 2021-03-30 12:01
:category: Enseignement
:tags: vulgarisation

Un dispositif de travaux pratiques à distance à Ouagadougou (Burkina Faso)
==========================================================================

    En provenance de l’AUF, |br|
    une interview du professeur **Zacharie Koalaga**
    
https://www.auf.org/nouvelles/actualites/un-dispositif-de-travaux-pratiques-distance-ouagadougou/?utm_source=email&utm_campaign=AUF__Fil_dactualit&utm_medium=email

Interview du Professeur Zacharie Koalaga, Directeur de l’Institut de
Formation Ouverte et à Distance (IFOAD) de l’Université Joseph
Ki-Zerbo. Il a présenté « LABOREM-Ouaga », un dispositif de travaux
pratiques et à distance que son établissement vient de mettre en place
avec le soutien de l’Agence Universitaire de la Francophonie et
l’Institut Universitaire de Technologie de Bayonne-Pays Basque
(France) de l'Université de Pau et des Pays de l'Adour.

|hr|

ILLUSIONS OPTIQUES
==================

https://www.theguardian.com/science/2021/mar/05/ship-hovering-above-sea-cornwall-optical-illusion

|image13|

« L’air chaud sur l’eau froide peut produire un mirage - en hauteur »

|image14|

Commentaire de Pierre Chavel : Malheureusement la qualité de l'image
publiée par le Guardian ne permet pas de valider s'il y a ou non une
ligne d'horizon très peu contrastée au-dessus du "mirage". Des données
topographiques suffisantes permettraient aussi de savoir où est
vraiment l'horizon, vu de ce point de vue. Il souhaite insérer le
commentaire suivant : . "la photographie originale permet-elle
d'assurer qu'il s'agit bien comme l'affirme l'article paru dans la
presse d'un effet de mirage supérieur exceptionnellement net ?"
|clearboth|
|hr|

EXPÉRIENCES REVISITÉES - MESURE DE LA GRAVITATION
=================================================

*Article du Monde, auteur David La Rousserie*

https://www.lemonde.fr/sciences/article/2021/03/31/l-infographie-peser-la-terre-avec-90-mg-d-or_6075105_1650684.html

|image15|
|clearboth|
|image16|
|clearboth|
|hr|

CABINET DE CURIOSITÉS
=====================

**HISTOIRE DE L’INSTRUMENTATION** (Instrument vieux de plus de deux
millénaires pour l’astronomie) : une petite partie de l’instrument
**antikythera (d’origine grecque)** avait été retrouvé dans l’épave d’un
navire. Au début les scientifiques ne comprenaient pas à quoi était
destiné cet instrument qui comprenait des engrenages et apparaissait
très sophistiqué pour son époque. Après de longues années de recherche
on commence à mieux comprendre et les scientifiques (UCL) ont même
réussi à reconstituer l’instrument complet (voir image) ce qui a fait
l’objet d’un article dans Nature (Scientific Reports) :
https://www.nature.com/articles/s41598-021-84310-w

https://www.theguardian.com/science/2021/mar/12/scientists-move-closer-to-solving-mystery-of-antikythera-mechanism

|image17|

|image18|
|clearboth|

Le mécanisme Antikythera qui date autour de 80 avant JC.

*The Antikythera mechanism is estimated to date back to around 80 BC*.

Photograph: X-Tek Group/AFP



.. |image13| image:: /images/bulletin-2021-03/image013.jpg
   :width: 470px
	   
.. |image14| image:: /images/bulletin-2021-03/image014.jpg
   :width: 370px
	   
.. |image15| image:: /images/bulletin-2021-03/image015.jpg
   :width: 570px
	   
.. |image16| image:: /images/bulletin-2021-03/image016.jpg
   :width: 570px
	   
.. |image17| image:: /images/bulletin-2021-03/image017.jpg
   :width: 570px
	   
.. |image18| image:: /images/bulletin-2021-03/image018.jpg
   :width: 570px
	   

.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
