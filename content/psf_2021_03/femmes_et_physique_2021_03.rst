FEMMES ET PHYSIQUE
##################

:date: 2021-03-30 12:01
:category: Enseignement
:tags: Afrique

**De « European Platform of Women Scientists (EPWS) »** : parution de son
livret de portraits de femmes scientifiques. EPWS a le plaisir
d'annoncer, le 11 février, « Journée internationale des femmes et des
filles en science », la parution de son livret de portraits de femmes
scientifiques, rassemblant les portraits publiés sur le site d'EPWS
entre 2018 et 2020. On y accède via :
https://epws.org/wp-content/uploads/2020/02/EPWS-Woman-Scientist-interview-2018-2020.pdf     

Un article sur l’intérêt des « camps » d’été de physique pour les
filles au niveau des lycées:
https://journals.aps.org/prper/pdf/10.1103/PhysRevPhysEducRes.17.010111
par Mike Williams,
`Rice University <http://www.rice.edu/>`__
et un article en PDF plus récent :
par Ericka Lawton et al, Improving high school physics outcomes for
young women, Physical Review Physics Education Research (2021).
`DOI: 10.1103/PhysRevPhysEducRes.17.010111 <http://dx.doi.org/10.1103/PhysRevPhysEducRes.17.010111>`__

*Source Le Monde 29 03 2021 David Larousserie*

.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
