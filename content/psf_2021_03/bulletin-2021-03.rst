Bulletin Physique sans Frontières (Mars 2021)
#############################################

:date: 2021-04-01 12:01
:category: Bulletin
:tags: nouvelles

|les_logos|


ÉDITORIAL:
==========

COVID sans Frontières par **Dave LOLLMAN**,
Vice-président Physique sans Frontières.

1er avril 2021 : j’ai envie de dire que c’est un poisson d’avril, mais
quand je regarde le passé, je me rends compte que je disais exactement
la même chose il y a un an, le 1 er avril 2020... Nous sommes encore
confinés, la situation sanitaire que nous impose Covid-19 n’épargne
personne. Elle n’a ni frontière, ni nationalité ; elle n’est ni
sectaire, ni politique et encore moins religieuse, mais elle est bien
présente partout sur nos 5 continents. Cette pandémie nous a au moins
enseigné une chose positive.  Main dans la main, nous essayons tous de
travailler ensemble et au-delà des frontières physiques, étatiques et
politiques pour la même cause : vaincre ce désastre, probablement plus
dévastateur que bien des guerres qu’ont connues nos peuples au niveau
sanitaire, économique, culturel et surtout social. Nous développons et
inventons des moyens scientifiques et socio-économiques tout en
partageant notre savoir-faire et en respectant nos valeurs
humaines. Nous apprenons à revivre en harmonie pour préserver la
dignité de l’humanité, indépendamment de nos origines, nos cultures et
nos éthiques, indépendamment de nos frontières qui nous ont jadis
cloisonnés par des conquêtes de territoires, entre autres. Notre
nouvelle arme : la communication en temps réel, les réunions
virtuelles, les cours à distance... En dépit de son lot de dangers
collatéraux, les fausses informations virales ou les insuffisances
d’informations, nous sommes désormais tous connectés bien au-delà des
frontières territoriales que nous nous sommes imposées. Un nouveau
monde connecté s’ouvre à nous, un monde où tout un chacun a une place
réelle et non virtuelle, la vie autrement... Bienvenue à l’ère sans
frontières !

Table des matières
==================

- `Nouvelles <{filename}/psf_2021_03/nouvelle.rst>`__
- `Développements pour l’environnement <{filename}/psf_2021_03/developpements_pur_l_environnement.rst>`__
- `Femmes et Physique <{filename}/psf_2021_03/femmes_et_physique_2021_03.rst>`__
- `Covid et Physique  <{filename}/psf_2021_03/covid_et_physique_2021_03.rst>`__
- `Enseignement et vulgarisation et cabinet de curiosités <{filename}/psf_2021_03/enseignemen_et_vulgarisation_2021_03.rst>`__
- `Veille technologique <{filename}/psf_2021_03/veille_technologique_2021_03.rst>`__
- `Développement de l’instrumentation en Afrique <{filename}/psf_2021_03/developpement_instrumentation_afrique.rst>`__
- `Actualités des universités Africaines <{filename}/psf_2021_03/actualite_universites_africaines.rst>`__
- `Courrier des lecteurs <{filename}/psf_2021_03/courrier_lecteurs_2021_03.rst>`__
- `Le coin de l’humour <{filename}/psf_2021_03/humour_2021_03.rst>`__
  

.. |image1| image:: /images/bulletin-2021-03/image001.jpg
   :width: 100px
      
.. |image2| image:: /images/bulletin-2021-03/image002.jpg
   :width: 100px
	   
.. |image3| image:: /images/bulletin-2021-03/image003.jpg
   :width: 100px

.. |les_logos| raw:: html

    <table style="background: rgba(255,255,255,0.8); border-radius: 30px;
                  border: 1px solid gray;">
       <colgroup><col width="20%"><col width="60%"><col width="20%"></colgroup>
       <tbody>
         <tr>
	   <td> <img src="/images/bulletin-2021-03/image001.jpg" style="width:100px;"> </td>
	   <td>
             <div style="font-size: 120%; font-weight: bold;
	                 text-align: center;">
	       Commission<br> “Physique sans Frontières »<br>
	       commune à la SFP et à la SFO
             </div>
	     <img src="/images/bulletin-2021-03/image003.jpg"
	          style="width:100px; float: left;">
	     <div style="color: slateblue; font-weight: bold;
	                 padding-top: 2em;">
	       « Le Savoir est une arme, l’ignorance nous désarme,
	       partageons le savoir ! »
	     </div>
	   </td>
	   <td> <img src="/images/bulletin-2021-03/image002.jpg" style="width:100px;"> </td>
	 </tr>
       </tbody>
    </table>
    
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
   
