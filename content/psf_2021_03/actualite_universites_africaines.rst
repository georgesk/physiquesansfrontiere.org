ACTUALITÉS DES UNIVERSITÉS EN AFRIQUE
#####################################

:date: 2021-03-30 12:01
:category: Enseignement
:tags: Afrique

À partir de Africa, Physics in Africa, and the African Physics Newsletter par James E. Gubernatis. Voices from African Students in COVID
========================================================================================================================================

**Les voix d’étudiants africains durant le COVID**

**Patrick Ning’I:**

Je m'appelle **Patrick Ning'i**, étudiant en dernière année de licence de
technologie en physique technique et appliquée à l'Université
technique du Kenya. Pendant cette période de fermeture, j'ai fait
plusieurs choses. Tout d'abord, j'ai utilisé cette période pour
terminer mon projet de fin d'études intitulé "The Interplay of Lattice
Distortion and Bands Near the Fermi Level in ATiO3 (A=Ca,Sr,Ba)". Je
compte présenter mon travail lors d'une prochaine conférence virtuelle
qui se tiendra en août. Ayant travaillé avec le code ab initio SIESTA
depuis un an maintenant, j'ai également passé beaucoup de temps à
enseigner à des étudiants juniors comment utiliser le système pour
étudier les propriétés des matériaux, telles que la thermoélectricité,
comme le montrent des composés tels que le ScF3. Lorsque cela est
nécessaire, je réalise également des tutoriels sur la façon d'utiliser
divers outils d'extension qui sont impliqués dans le processus de
modélisation et d'étude des matériaux, en utilisant des logiciels de
traçage standard à code source ouvert. En plus de me concentrer sur
mon travail de diplôme, j'ai également été activement engagé dans
plusieurs projets d'apprentissage automatique et de science des
données, car ce sont également mes domaines d'intérêt. J'ai travaillé
en collaboration sur un projet de segmentation du marché utilisant
l'apprentissage automatique, ainsi que sur un outil de suivi des
émotions sur Twitter utilisant le traitement du langage naturel. Je
réfléchis actuellement à la manière dont le traitement du langage
naturel peut être utilisé pour soutenir la recherche dans le domaine
de la science des matériaux. Une approche à laquelle j'ai pensé est de
créer un modèle personnalisé d'intégration des mots à l'aide de
données textuelles, pour différents articles de recherche en science
des matériaux. Cette idée a déjà été mise en œuvre. Néanmoins, je
pense que la reproduire serait une étape importante vers la création
d'un modèle similaire avec des capacités améliorées.

**Sr Mary Taabu Simiyu** Université de Nairobi, Kenya (maîtrise en
physique) est une religieuse catholique romaine de la congrégation des
Servantes du Saint Enfant Jésus et une étudiante en doctorat en
physique de la matière condensée à l'Université de Nairobi,
Kenya. Elle est lauréate du concours de la thèse de 3 minutes à
l'Université du Ghana en 2019, et des présentations entrepreneuriales
et orales à la Nelson Mandela African Institution of Science and
Technology en 2015. Elle se passionne pour le service des pauvres
depuis son enfance et pense que son projet sur la qualité de l'eau
aidera les personnes à faible revenu des pays en développement à
accéder à une eau saine. Sœur Mary a publié un article intitulé
"Application of An Organic Plant-Derived Binder in the Fabrication of
Diatomaceous Earth Waste-Based Membranes for Water Purification
Systems" dans la revue Materials ResearchSociety Advances, et ce
travail est toujours en cours. Dans ses travaux antérieurs, Sœur Mary
a travaillé sur "l'application de la spectroscopie Raman dans la
détection de l'aflatoxine B1 dans les grains de maïs et la
farine". Elle a fait des stages au BITRI, l'Institut de recherche et
d'innovation technologique du Botswana, et à l'Université de York.


.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
