|image27| |image28| Madagascar : Collaboration de la société Tropical Bio Trade et du Département informatique de l’Université d’Antananarivo
#############################################################################################################################################

:date: 2021-02-19 12:01
:category: Annonces
:tags: jumelages, Afrique

La société Tropicale Bio Trade est une jeune start-up qui a pour
vocation d’améliorer la condition de vie des agriculteurs dans les
pays en voie de développement. Elle envisage de développer des
activités qui prendront constamment en compte les aspects écologiques,
environnementaux, sociologiques et culturels des pays où elle souhaite
s’implanter. Tropical Bio Trade valorise les activités menées
localement en important en Europe certains produits locaux
transformés. Les fruits séchés constituent le premier pôle d’activité
envisagé.  Sujet du stage : Elaboration d’une base de données pour la
société Tropical Bio Trade Afin de mettre en place une chaîne de
production et de commercialisation des fruits secs en accord avec la
stratégie éco-responsable retenue par Tropical Bio Trade, un recueil
d’informations et sa structuration est nécessaire. L’objectif du stage
est d’implémenter une base de données avec une approche « Data-
Science » s’appuyant sur les différentes sources de données
disponibles (culture des fruits, mode de séchage, séchage artisanal,
séchage industriel, production annuelle, transport des fruits...). Une
interface Homme machine sera également développée pour une gestion
évolutive des données recueillies.

.. |image27| image:: /images/bulletin-2021-02/image027.jpg
   :width: 100px
	 
.. |image28| image:: /images/bulletin-2021-02/image028.jpg
   :width: 100px
	 
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
   
       
