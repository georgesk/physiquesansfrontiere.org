ENVIRONNEMENT
#############

:date: 2021-02-19 12:01
:category: Open access
:tags: environnement, technologie frugale

LE LOW TECH LAB ET LE NOMADE DES MERS ENVOYÉ PAR PHILIPPE AUBOURG
=================================================================

https://lowtechlab.org/fr

Initiatives visant à récupérer des développements à basse technologie
pour le développement local, l’exemple suivant concerne une technique
simple pour potabiliser l’eau , ce sont des récipients en céramique
fabriqués à partir de sciure de bois et d’argile. Les microporosités
permettent une filtration de l’eau efficace. (**ECOFILTRO Guatemala**).

|image11|
|clearboth|
|image12|

Le nomade des mers est un bateau qui sert de plateforme
d'expérimentation des low-tech, support **de promotion** et vecteur
**de diffusion**, le **Nomade des Mers** a vocation à devenir un éco-système
autonome exemplaire, porte-drapeau
**de l'innovation durable et solidaire**.

|clearboth|
|hr|

RÉCUPERATION DE MÉTAUX RARES DANS LES REBUTS D’ÉLECTRONIQUE
===========================================================

https://www.industrial-lasers.com/home/article/14175926/laser-processing-recovers-valuable-raw-materials

Il s’agit d’un traitement par laser développé par
l’**Institut Frauenhofer ILT** (Institute for Laser technology)
et qui a fait l’objet d’un projet européen, qui permet de récupérer
des composants contenant des éléments à haute valeur ajoutée dans les
téléphones portables et les cartes mères d’ordinateurs. Les éléments
ciblés sont :
**tantale, néodyme, tungstène, cobalt et gallium**.

Plusieurs kilos de composants, entre 96% et 98% du tantale, ont été
récupérés sur environ 1000 téléphones portables et plus de 800 cartes
mères de gros ordinateurs. Cet exemple a montré que nombre des
matériaux les plus importants contenus dans les appareils
électroniques peuvent être extraits aussi efficacement que l'équipe du
projet l'avait espéré, créant une nouvelle source de matière première
secondaire à forte teneur en matériaux réutilisables - nettement
supérieure à celle des concentrés de minerai de tantale proposés par
les fournisseurs de matière première vierge.

+-----------+-----------+
| |image13| | |image14| |
+-----------+-----------+

|hr|

RÉCUPERATION, RECYCLAGE, RÉUTILISATION (ÉCONOMIE CIRCULAIRE)
============================================================

Des vieux ordinateurs portables reconfigurés en ordinateurs pour les écoles :

+-----------+-----------+
| |image15| | |image16| |
+-----------+-----------+

https://hackaday.io/project/176450-the-x-pc
|hr|

TRÈS BEAU DÉTOURNEMENT DE TECHNOLOGIE ENVOYÉ PAR JEAN MICHEL FRIEDT (CNRS labo Femto Besançon)
==============================================================================================

Il a fait l’objet d’une thèse en Allemagne ! Et encore une fois il
s’agit du détournement de la technologie du lecteur de DVD.

https://loetlabor-jena.de/doku.php?id=projekte:dvdlsm:start&s[]=dvd

Cela concerne l’utilisation d’une grande partie d’un lecteur de DVD
pour en faire un microscope laser à balayage. Ce n’est pas à la portée
de tout le monde mais c’est intéressant, de plus le matériel de base
est quasi gratuit car les lecteurs de DVD sont actuellement jetés !!
Nous pouvons tous participer à l’économie circulaire !

|image17|

Il faut regarder les liens cités en fin de documents ; **Gaudi lab**,
**Instructable**, etc..

Rappelons que le lecteur de DVD a permis beaucoup de développements et
en particulier récemment des détecteurs d’**anticorps COVID** par un
groupe **taiwano danois** (ayant permis la constitution de la start up
Bluesense technology) et par un groupe de l’université de Valence
(Espagne) dont le responsable du groupe est **Maquiera**. (nous contacter
pour demander les documents).

Le lecteur de DVD est une valeur sûre, dommage qu’il soit maintenant
envoyé à la poubelle !
**Le manque de culture scientifique et technologique en est responsable !**

|clearboth|

.. |image11| image:: /images/bulletin-2021-02/image011.jpg
   :width: 450px
	   
.. |image12| image:: /images/bulletin-2021-02/image012.jpg
   :width: 225px
	   
.. |image13| image:: /images/bulletin-2021-02/image013.jpg
   :width: 225px
	   
.. |image14| image:: /images/bulletin-2021-02/image014.jpg
   :width: 225px
	   
.. |image15| image:: /images/bulletin-2021-02/image015.jpg
   :width: 225px
	   
.. |image16| image:: /images/bulletin-2021-02/image016.jpg
   :width: 225px
	   
.. |image17| image:: /images/bulletin-2021-02/image017.jpg
   :width: 450px
	   
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
   
