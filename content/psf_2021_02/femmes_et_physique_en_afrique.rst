FEMMES ET PHYSIQUE EN AFRIQUE
#############################

:date: 2021-02-19 12:01
:category: Enseignement
:tags: UNESCO

       
.. raw:: html

    <h3>Association Diasporeines Africa</h3>

|image18|

.. raw:: html

	 <div style="text-align: center; background: rgba(255,255,255,0.8);
	        width: 450px; height: 100px; margin-bottom: 20px;" >
	    <i>Odette Fokapu</i><br>
	    Présidente -Fondatrice de « Diasporeines Africa »<br>
	    (diasporeines.africa@gmail.com)<br>
	     https://www.lesdiasporeinesafrica.org/
         </div>


Qui sommes-nous ?
=================

**« Diasporeines africa »** est une association française loi 1901
domiciliée à Paris. Les membres sont les femmes actives de la
diaspora. Elle comporte également un grand nombre de sympathisants.


L’association a pour but de contribuer à une action plus globale
intitulée « Femmes et développement en Afrique », initiée par le
groupe ONG UATI-UISF partenaire officiel de l’UNESCO.
L’autonomisation des femmes en Afrique est toujours d’actualité. Y
parvenir grâce à l’emploi durable pour chacune nécessite des leviers
tel que le numérique.

« Diasporeines africa » a donc pour objectif de sensibiliser les
jeunes filles africaines aux métiers du numérique et de soutenir
l'entreprenariat numérique féminin en Afrique.

L’association mène trois niveaux d'actions en direction des jeunes
femmes africaines sur le continent.

1) **Compétition JFN (Jeunes Filles & Numérique) « Tests de connaissances en numérique » pour lycéennes.**
   Le but est de sensibiliser très tôt les jeunes filles africaines
   aux métiers du numérique. La finalité du concours est la création
   d’un club du numérique par les lauréates qui pourront ainsi
   réfléchir ensemble à des « Apps » dans le but d'apporter des
   solutions aux problématiques propres au continent africain.
2) **Offre de stages de 6 mois sur des projets incluant du numérique pour étudiantes de niveau Master**
   Le but est de motiver les jeunes filles à envisager une carrière
   dans les métiers du numérique. Les stages se déroulent dans les
   entreprises avec un co-encadrant universitaire. Elles peuvent ainsi
   acquérir une expérience professionnelle favorable à une recherche
   d’emploi dans le domaine. Notre démarche est de thématiser les
   stages par pays en fonction des collaborations que nous avons avec
   collègues scientifiques et/ou entreprises locales. Les thèmes
   actuellement en cours de réflexion sont orientés « Numérique &
   Smart Energie » pour le Burkina-Faso, « Data-Science » pour
   Madagascar, « e-agriculture » pour le Mali, « e-santé » pour la
   Côte d’Ivoire et « e-éducation » pour le Cameroun.
3) **Soutien à l'entrepreneuriat numérique aux femmes actives dans le domaine de l’informel..**
   L'inclusion économique des femmes grâce au numérique fait du chemin
   en Afrique comme partout ailleurs. Il s’agit ici de soutenir une
   activité existante et dont le numérique est à même d'apporter une
   réelle transformation. Le projet en cours de réflexion concerne la
   subvention d'une activité dans le domaine de la maintenance de
   matériel informatique.

|image19|


.. raw:: html

	 <div style="padding: 1em 2em;
	                font-style: italic; font-size: 140%;
			border-radius: 1em;
			border: 1px gray solid;
			background: rgba(255,255,255,0.5);
			">
		Jeune femme africaine, vous êtes intéressée par les
                métiers du numérique en Afrique, n'attendez pas,
                offrez-vous l'occasion d'y accéder en candidatant à un
                de nos projets « Numérique@Entreprenariat Féminin en
                Afrique »
		<br/>
		<br/>
		<p style="color: grey; font-weight: bold;">
		  Chaque femme peut s'affirmer, choisir son avenir,
		  et changer le monde.
		</p>
	 </div>


|hr|

Actions soutenues par « Diasporeines Africa » en 2021
=====================================================

|image20|

**Cameroun : Actions en partenariat avec Sci-Tech-Service et la SCP**

|clearboth|

1. **Formation de jeunes filles aux Travaux Pratiques de Physique et
   Outils du Numérique** :  la formation est dispensée par l’équipe de la
   plateforme Sci-Tech-Service sous la direction du Professeur Paul
   Woafo. Elle a débuté le 14 janvier 2021. Deux groupes d’étudiantes,
   un de niveau BTS et un de niveau Master suivent cette formation. Le
   programme, d’un volume de 35h comporte des TP d’électricité,
   d’électronique analogique et numérique, d’optique. Des maquettes
   réalisées lors du concours « Physique Expérimentale APSA » et
   fournies par l’APSA (Expeyes) sont largement exploitées. L’aspect
   utilisation d’instrumentation low-cost est bien mis en avant. Bien
   que basique, cette formation remplit un grand vide dans le cursus
   scolaire et universitaire de ces étudiantes. En effet, à cause des
   problèmes divers, la formation en travaux pratiques est très
   limitée dans l’enseignement secondaire et supérieure du Cameroun, y
   compris même dans certains établissements technologiques.  Les
   étudiantes qui suivent cette formation offerte par « Diasporeines
   Africa » sont donc des privilégiées.  Elles vont acquérir des
   connaissances pratiques et pourront également bénéficier des
   opportunités dans le domaine de la recherche en sciences
   expérimentales et technologiques.
   
+------------------------+
| |image21|              |
+------------------------+
| |image24|              |
+------------------------+

2. **Compétition JFN** : La première édition portée localement par notre
   partenaire Sci-Tech-Service aura lieu à Yaoundé le 14
   Août 2021. Une phase d’entrainement débutée en janvier permet aux
   jeunes filles de se préparer tout au long de l’année. Le programme
   d’exercices en mathématique et informatique, proposé par l’équipe
   Sci-Tect-Service suit le programme dispensé dans les classes de
   premières scientifiques du Cameroun. Pour assurer la promotion du
   concours, des « mini-compétions » ou « compétitions préparatoires »
   vont être menées tout au long de l’année dans 4 lycées
   pilotes. Plus d’informations sur ce lien https://www.lesdiasporeinesafrica.org/actualites

|image25|
|clearboth|

.. |image18| image:: /images/bulletin-2021-02/image018.jpg
   :width: 180px
	   
.. |image19| image:: /images/bulletin-2021-02/image019.jpg
   :width: 120px
	   
.. |image20| image:: /images/bulletin-2021-02/image020.jpg
   :width: 120px
	   
.. |image21| image:: /images/bulletin-2021-02/image021.jpg
   :width: 560px
	   
.. |image24| image:: /images/bulletin-2021-02/image024.jpg
   :width: 560px
	   
.. |image25| image:: /images/bulletin-2021-02/image025.jpg
   :width: 560px
	   
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
   
