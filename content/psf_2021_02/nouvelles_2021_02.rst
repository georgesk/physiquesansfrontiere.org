NOUVELLES
#########

:date: 2021-02-19 12:01
:category: Annonces
:tags: nouvelles

APSA
====

La nouvelle présidente de l’**APSA** (Association pour la Promotion
Scientifique) est **Annick Suzor Weiner** également membre de notre
commission. Nous lui souhaitons beaucoup de succès. Il faut rappeler
ici que Madame **Odette Fokapu** vice-présidente de notre commission est
également secrétaire de l’APSA.

|hr|

LE COLONIALISME ANGLAIS EN AFRIQUE ...
======================================

\... ses conséquence sur les études et sur la vie : un magnifique
article d’un zimbabwéen **Simukai Chigudu** à lire et à faire lire !!
C’est en anglais mais l’un d’entre nous pourrait le traduire. Ce
serait intéressant d’obtenir l’équivalent pour le colonialisme
français pour en comprendre les conséquences.

https://www.theguardian.com/news/2021/jan/14/rhodes-must-fall-oxford-colonialism-zimbabwe-simukai-chigudu

|hr|

FAKE NEWS EN SCIENCES
=====================

Animations de la Chaine Franco Allemande Arte :
https://www.arte.tv/fr/videos/081077-016-A/data-science-vs-fake/

|hr|

Membres de POsF : Que pouvez-vous faire pour nous ?
===================================================

- Nous sommes à la recherche de **parrainages** pour la prochaine
  compétition « **Test de connaissance numérique** » que nous organisons
  au Cameroun en 2021. Si votre organisme accepte de parrainer cette
  compétition il peut se mettre en contact avec l’association. Les
  logos des parrains seront mis sur l'affiche numérique du concours
  avec un lien attaché à ce logo pour donner accès à votre site.
- **Vous pouvez diffuser l’annonce de notre compétition sur votre site**.
- Vous pouvez faire une publication de nos offres de stage
- Vous pouvez apporter **votre contribution à l’encadrement des stages
  par vos collaborations locales établies**. Nous sommes également à la
  recherche de collègues universitaires locaux pour nos futures
  actions au Mali, au Togo, au Gabon, au Sénégal et en Côte d’ivoire.
- **Vous pouvez aussi nous aider par vos dons**.

|hr|

6th International Day of Women & Girls in Science Assembly
==========================================================

Témoignage d’une scientifique de l’université de Cambridge, **Rachel Oliver**
(science des matériaux) :

https://www.cam.ac.uk/thiscambridgelife/racheloliver?utm_campaign=newsletters&utm_medium=email&utm_source=279697_Research%20weekly%20bulletin%2019%2F02%2F2021&dm_i=6DCF,5ZTD,1EA1TD,P0V2,1

|hr|

GRANDS INSTRUMENTS EN AFRIQUE , SOURCE DE LUMIERE POUR L’AFRIQUE :
==================================================================

https://www.africanlightsource.org/afls-2-roadmap-training-and-mobility/


.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
   
  
