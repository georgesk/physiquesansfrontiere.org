IMPACT DU COVID SUR L’UNIVERSITE EN AFRIQUE : Le cas du Gabon
#############################################################

:date: 2021-02-19 12:01
:category: Débat
:tags: covid

\        
    Un témoignage du Dr. **Marius KAMSAP**,
    Maître Assistant CAMES, Enseignant Chercheur Département de Physique,
    Université des Sciences et Techniques de Masuku


Quelques chiffres sur la situation générale du Gabon en termes de
COVID 19 sur un an, soit de mars 2020 à ce janvier 2021 :

- 9740 cas positifs au COVID
- 9549 guérisons
- 66 décès
- 125 cas actifs

Au Gabon de façon générale et dans notre université (Université des
Sciences et Techniques de Masuku), nous avons été fermés pendant 6
mois. Depuis la reprise des activités pédagogiques, nos enseignements
se passent pour la plupart en présentiel. Nos structures ne nous
permettent pas de limiter les effectifs dans les salles de
classes. Notre plus gros effectif est de 200 étudiants en première
année dans un amphi de 500 places. On peut se dire que la situation
n’est pas dramatique par rapport à d’autres Universités.

- Certains enseignements s’effectuent en ligne
- Nous exigeons le port des masques par les enseignants et les étudiants
- Nous avons des points de lavage des mains partout dans l'Université
- Dans certaines structures un dispositif de prise de température est
  mis en place mais dans notre université il y en avait il y a quelques
  mois mais en ce moment on n'en a plus.
- Nous essayons tant bien que mal de respecter les mesures de distanciation physique

Voilà en résumé ce que je peux dire concernant la situation de notre université.

    Marius Kamsap


.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
   
  
