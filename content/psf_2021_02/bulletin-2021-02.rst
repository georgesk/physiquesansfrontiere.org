Bulletin Physique sans Frontières (Fév 2021)
############################################

:date: 2021-02-20 12:01
:category: Bulletin
:tags: nouvelles


ÉDITORIAL:
==========
Les biens communs : vers une société du partage en temps de pandémie ?
----------------------------------------------------------------------

La disponibilité des vaccins est devenue une question
géopolitique. Les pays les plus riches ont obtenu une grande quantité
de vaccins. Les autres pays doivent se battre pour acheter des doses
auprès de sources alternatives.


Il y a aussi un problème technologique, puisque pour certains vaccins,
la température de conservation est très basse (- 80°). Cela signifie
que seuls les pays développés disposent des structures appropriées
pour gérer le stockage des vaccins. Cela constitue également un autre
biais qui rend les choses difficiles pour les pays à faibles
ressources.

En tant que scientifiques, nous devrions promouvoir le vaccin
développé pour les pandémies mondiales comme un bien commun ou
partageable. C’est la meilleure manière d’arrêter rapidement la
pandémie dans le monde entier.

N’oublions pas que la physique appliquée à un rôle important à jouer
que nous avons déjà abordé lors de précédents bulletins.

Happy new Chinese year :
------------------------

|image1|


Les articles de ce bulletin :
-----------------------------

- `Appel d'offres <{filename}/psf_2021_02/appel_offres.rst>`__
- `Nouvelles <{filename}/psf_2021_02/nouvelles_2021_02.rst>`__
- `Covid et Physique Appliquée <{filename}/psf_2021_02/covid_physique.rst>`__
- `Enseignement, vulgarisation <{filename}/psf_2021_02/enseignement_vulgarisation.rst>`__
- `Environnement <{filename}/psf_2021_02/environnement_2021_02.rst>`__
- `Femmes et Physique en Afrique <{filename}/psf_2021_02/femmes_et_physique_en_afrique.rst>`__
- `Partenariat Darga-Tech et Université de Ouagadougou <{filename}/psf_2021_02/Darga-Tech-u_Ouagadougo.rst>`__
- `Partenariat Tropical Bio Trade et Université d’Antananarivo <{filename}/psf_2021_02/Tropical-Bio-Trade-u_Antananarivo.rst>`__
- `Impact du Covid sur l'Université en Afrique <{filename}/psf_2021_02/covid_universite_afrique.rst>`__
- `Veille scientifique et hacking <{filename}/psf_2021_02/veille_scientifique_hacking.rst>`__


LE COIN DE L’HUMOUR (ET ON EN A BESOIN EN CE MOMENT !)
------------------------------------------------------

|image33|
|clearboth|
Dessin Aurel Le Monde 05/02/21



.. |image1| image:: /images/bulletin-2021-02/image001.jpg
   :width: 180px
   :class: image-right
      
.. |image33| image:: /images/bulletin-2021-02/image033.jpg
   :width: 450px
	   
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
   
