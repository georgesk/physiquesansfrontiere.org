COVID ET PHYSIQUE APPLIQUÉE, ARTICLES ET ACTIONS
################################################

:date: 2021-02-19 12:01
:category: Open access
:tags: covid
:slug: covi_physique_2021_02

Monitoring efficiency of masks :
================================

https://phys.org/news/2020-11-valves-n95-masks-filter-exhaled.html

|hr|

Documents sur l’utilisation de l’UV-C :
=======================================

(i) https://www.lasercomponents.com/fileadmin/user_upload/home/Datasheets/lc/application-reports/bolb/coronavirus-inactivating-emitters-en.pdf
(ii) Action germicide des UV-C : |br| https://www.lasercomponents.com/fileadmin/user_upload/home/Datasheets/lc/application-reports/life-science/uv-germ-killing.pdf
(iii) Application des UV-C à la purification de l’eau : |br| https://www.lasercomponents.com/fileadmin/user_upload/home/Datasheets/lc/application-reports/life-science/water-purification-uvc-leds.pdf

|hr|

Production de respirateurs au CHILI avec 70 % de pièces découpées par laser :
=============================================================================

https://www.industrial-lasers.com/cutting/article/14187797/lasers-battle-against-covid19

|hr|

Utilisation de lasers pour le traitement de patients atteints de la COVID
=========================================================================

https://www.novuslight.com/doctors-using-laser-therapy-to-treat-covid-19-patients-report-positive-outcomes_N10950.html

|hr|

COVID 19 detection through fluorescence :
=========================================

https://optics.org/news/11/10/7

|hr|

Problem with face shield efficiency revealed by laser :
=======================================================

https://www.osa-opn.org/home/newsroom/2020/september/light_exposes_face_shields_and_vented_masks_as_ine/

Clive B. Beggs, Eldad J. Avital.
*Upper-room ultraviolet airdisinfection might help to reduce COVID-19 transmission in buildings: a feasibility study.*
PeerJ, 2020; 8: e10196 DOI:
`10.7717/peerj.10196 <http://dx.doi.org/10.7717/peerj.10196>`__

|hr|

Nouveaux masques :
==================

https://phys.org/news/2021-01-masks-smarter-safer-covid-.html

|image2|
|clearboth|

|hr|

Masques high tech
=================

https://www.futura-sciences.com/tech/actualites/tech-ces-2021-voici-masques-anti-covid-connectes-82663/#xtor=EPR-23-[HEBDO]-20210114-[ACTU-CES-2021-:-et-voici-les-masques-anti-Covid-connectes--

|hr|

UNIVERSITÉ DE CAMBRIDGE :
=========================

    The vital role of ventilation in the spread of COVID-19 has been
    quantified by researchers, who have found that in
    poorly-ventilated spaces, the virus can spread further than two
    meters in seconds, and is far more likely to spread through
    prolonged talking than through coughing.

Le rôle vital de la ventilation dans la propagation du COVID-19 a été
quantifié par les chercheurs, qui ont découvert que dans les espaces
mal ventilés, le virus peut se propager sur plus de deux mètres en
quelques secondes, et qu'il est beaucoup plus susceptible de se
propager par des conversations prolongées que par la
toux. (*Trad. www.DeepL.com*)

https://www.cam.ac.uk/research/news/free-online-tool-calculates-risk-of-covid-19-transmission-in-poorly-ventilated-spaces?utm_campaign=newsletters&utm_medium=email&utm_source=252316_Research%20weekly%20bulletin%2022%2F01%2F2021&dm_i=6DCF,5EOS,1EA1TD,MQBI,1

Application gratuite pour l’aide au calcul à la mesure des risques de
transmission à l’intérieur en fonction de différents
paramètres. https://airborne.cam/

|hr|

Décontamination, des masques N95 état des lieux (pre-print)
===========================================================

Robert Fischer et al. Assessment of N95 respirator decontamination and
re-use for SARS-CoV-2, (2020).

`DOI: 10.1101/2020.04.11.20062018 <http://dx.doi.org/10.1101/2020.04.11.20062018>`__

|hr|

INDE - VACCINS :
================

L’un des vaccins est fabriqué par la firme pharmaceutique indienne
Bharat Biotech, on l’oublie mais l’Inde est l’un des principaux
fabricants de vaccins.

https://timesofindia.indiatimes.com/india/dcgi-approves-oxfords-covishield-and-bharat-biotechs-covaxin-vaccine-for-restricted-emergency-use/articleshow/80080471.cms

|image3|
|clearboth|
Credit : Times of India

|hr|

Laboratoire espagnol officiel pour le test de conformité des équipements :
==========================================================================

+------------------------------------------------------------+
| **Images de test des capacités de filtrage des masques :** |
+------------------------------------------------------------+
| |image5|                                                   |
+------------------------------------------------------------+
| **Test de conformité des visières :**                      |
+------------------------------------------------------------+
| |image6|                                                   |
+------------------------------------------------------------+

Pourquoi montrer ces images ? C’est pour faire connaître le rôle
important joué par la physique appliquée lors de la crise COVID.


.. |image2| image:: /images/bulletin-2021-02/image002.jpg
   :width: 450px
	   
.. |image3| image:: /images/bulletin-2021-02/image003.jpg
   :width: 450px
	   
.. |image4| image:: /images/bulletin-2021-02/image004.jpg
   :width: 150px
	   
.. |image5| image:: /images/bulletin-2021-02/image005.jpg
   :width: 450px
	   
.. |image6| image:: /images/bulletin-2021-02/image006.jpg
   :width: 450px
	   
	   
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
      
