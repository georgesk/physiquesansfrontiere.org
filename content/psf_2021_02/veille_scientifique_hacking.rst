VEILLE TECHNOLOGIQUE ET BRICOLAGE SCIENTIFIQUE (HACKING)
########################################################

:date: 2021-02-19 12:01
:category: Open access
:tags: technologie frugale


|image29|

La conception et la fabrication d’équipements
scientifiques en source ouverte (open source) se
développe en Afrique, voici l’affiche de « L’African
Institute of Open Science and Hardware ».

|hr|

NOUVEAU MICRO-CONTROLEUR RASPBERRY PI
=====================================
|image30|
https://techxplore.com/news/2021-01-raspberry-unveils-pi-pico-microcontroller.html

|clearboth|
|hr|

DÉTECTEUR DE CO2 PAR LE PROFESSEUR PIERRE CARLES (JUSSIEU)
==========================================================

https://www.instructables.com/CO2-Monitoring-As-an-Anti-Covid19-Measure/

Il est possible de télécharger le pdf.

|image31|
|clearboth|
|hr|

DÉTECTION DE MINES PAR DES DRONES : THERMOGRAPHIE
=================================================

https://www.handicapinternational.be/fr/actualites/premiere-mondiale-belge-des-drones-detecteurs-de-mines

Le drone est muni d’une caméra infrarouge destinée à mesurer les
différences de température, il peut explorer rapidement des zones de
superficie importante permettant d’optimiser la phase de déminage Une
phase expérimentale a été effectuée au Tchad. Cette action a été
développée dans le cadre du projet 'Odyssée 2025', financé par la
**Direction Générale Coopération au développement et Aide humanitaire (DGD) du gouvernement belge**.

|image32|
|clearboth|
Source : Handicap International et Mobility Robotics

Lien vers article du Journal of “Conventional Weapons destruction” «
Proof: How Small Drones Can Find Buried Landmines in the Desert Using
Airborne IR Thermography »

https://commons.lib.jmu.edu/cisr-journal/vol24/iss2/15/

.. |image29| image:: /images/bulletin-2021-02/image029.jpg
   :width: 470px
	   
.. |image30| image:: /images/bulletin-2021-02/image030.jpg
   :width: 300px
	   
.. |image31| image:: /images/bulletin-2021-02/image031.jpg
   :width: 470px
	   
.. |image32| image:: /images/bulletin-2021-02/image032.jpg
   :width: 470px
	   
	   
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
   
  
