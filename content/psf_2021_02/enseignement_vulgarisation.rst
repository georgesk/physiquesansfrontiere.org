ENSEIGNEMENT - VULGARISATION
############################

:date: 2021-02-19 12:01
:category: Enseignement
:tags: vulgarisation

Mallette pédagogique sur la transmission de données par puce optique dans la gamme bleu/UV
==========================================================================================

Cette année, le Réseau Optique et Photonique du CNRS a retenu le
projet porté par Stéphane Trebaol et Loïc Bodiou,
enseignants-chercheurs de l’Institut Foton affiliés à l’Enssat et
l’IUT Lannion. Ce projet a pour objectif de développer une mallette
pédagogique sur la transmission de données par puce optique dans la
gamme bleu/ultraviolet de la lumière. C’est le public
étudiants/lycéens qui est ciblé au travers des démonstrations
scientifiques qui seront réalisées lors des portes ouvertes des
établissements mais aussi lors des salons étudiants. Le grand public
pourra également découvrir cette mallette puisque son utilisation est
programmée pour les prochaines éditions de la Fête de la Science.

|image7|

Nous allons nous renseigner sur la disponibilité de cette mallette
pour pouvoir la tester et éventuellement la proposer à nos partenaires
africains.

|hr|

SUPERBE CONFERENCE ORGANISEE PAR LA SECTION PARIS SUD DE LA SFP
===============================================================

**Hélène Langevin** parle de la vie de ses parents Irène Curie et Frédéric
Joliot et pas seulement sous l’aspect scientifique, passionnant ! :

https://www.youtube.com/watch?v=hSu3zErV_1I&list=PLaEASrX3stq4VjR74_N1CLBIPBAaYkM0u&index=2

|hr|

MATÉRIEL À COÛT RAISONNABLE POUR TRAVAUX PRATIQUES D’OPTIQUE
============================================================

|image8|
Information envoyée par Michael Steinitz (Editeur du Canadian Journal
of Physics), Interféromètre de Michelson à coût soutenable :

https://www.pureunobtanium.com/michelson-interferometer
|clearboth|
|hr|

PYRAMIDE 3D ET HOLOGRAPHIE
==========================

Association Atouts science : La pyramide 3D a été à la mode au moment
de l’apparition des tablettes, on parlait à l’époque d’holographie,
Atout Science 3D revient là-dessus. En tous cas c’est une attraction
pour les jeunes scientifiques.

Pyramide 3D : Est-ce vraiment une image en 3D ?
https://www.youtube.com/watch?v=o0mLwphHV8k

+------------------------------------------------------------------+
| |image9|                                                         |
+------------------------------------------------------------------+
| |image10|                                                        |
+------------------------------------------------------------------+
| Vidéo pour fabriquer la pyramide : https://youtu.be/ZAvA6uk28OY  |
+------------------------------------------------------------------+



.. |image7| image:: /images/bulletin-2021-02/image007.jpg
   :width: 450px
	   
.. |image8| image:: /images/bulletin-2021-02/image008.jpg
   :width: 225px

.. |image9| image:: /images/bulletin-2021-02/image009.jpg
   :width: 450px
      
.. |image10| image:: /images/bulletin-2021-02/image010.jpg
   :width: 450px
      
	   
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
      
