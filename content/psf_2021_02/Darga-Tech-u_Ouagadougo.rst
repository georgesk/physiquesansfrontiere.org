|image26| Burkina-Faso : Actions en partenariat avec Darga-Tech et le Département Informatique de l’Université de Ouagadougou
#############################################################################################################################

:date: 2021-02-19 12:01
:category: Annonces
:tags: jumelages, Afrique

La start-up Darga Tech mène des activités ciblées au Burkina-Faso dans
le but d’apporter des solutions aux problématiques locales ; c’est le
cas de l’approvisionnement de l’eau en zone rurale et l’insuffisance
énergétique en zone urbaine. Deux sujets de stages sont proposés aux
étudiantes des universités du Burkina-Faso. Dans ce contexte, une
convention de collaboration a été signée entre « Diaporeines Africa »
et l'UFR SEA de Université Joseph KI-ZERBO.

Sujet 1 : Irrigation par Pompage Solaire pour Tous
==================================================

L’objectif du stage est le développement d’une plateforme (logiciel et
hardware) pour la gestion et le contrôle à distance d’un système
d’irrigation à pompage solaire. Cette plateforme devrait permettre de :

(i)   mettre en place un système de location-vente à coût abordable
      (moins de 1 euros/jour) ;
(ii)  gérer la pompe à distance (marche/arrêt);
(iii) surveiller la production solaire de la pompe et mesurer les
      paramètres environnementaux (température, humidité,
      ensoleillement) du site.

Sujet 2 : Développement d’outils numériques de diagnostics énergétiques pour l’électrification et la gestion d’énergie
======================================================================================================================

Le but de ce stage est d’élaborer un système qui permet de réaliser un
diagnostic énergique, et de suivre la consommation énergétique des
bâtiments publics et industriels. De façon spécifique, il faudra
développer, à partir d’outils logiciels open-source, des applications
mobiles permettant d’automatiser la collecte et le traitement des
données de terrain. Ces données de terrain permettront de construire
les profils de consommation théoriques ou la demande. Ces profils
théoriques seront confrontés à des données de consommation
réelles. Les données de consommation réelles seront acquises via des
systèmes d’acquisitions élaborés à partir des outils open-source (Node
js, Python...) et open-hardware tels la plateforme Arduino et
Raspberry Pi.


.. |image26| image:: /images/bulletin-2021-02/image026.jpg
   :width: 100px
	   
.. |br| raw:: html

  <br/>
  
.. |hr| raw:: html

  <div class="hr"></div>

.. |clearboth| raw:: html

  <div style="clear: both;"></div>
   
