APPEL D’OFFRE « SCIENCES FRUGALES » CNRS IRD
############################################

:date: 2021-02-19 12:01
:category: Annonces
:tags: nouvelles
:slug: appel_offres_2021_02

La commission Physique sans Frontières est partie prenante d’un projet
soumis à cet appel d’offre par le professeur Emmanuel Maisonhaute
(LISE Jussieu), qui concerne l’application de l’**électrochimie** à des
problèmes sociétaux,

(i) **détection de faux médicaments**,
(ii) **caractérisation des aliments**,
(iii) **détection de métaux lourds dansl’eau**.

Le titre du projet est « **ELABORE** ». Le projet impliquera de
chercheurs de l’**IRD** (Institut de Recherche pour le Développement) au
Bénin ainsi que d’universitaires Béninois et d’un fablab local.
