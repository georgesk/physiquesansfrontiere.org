Veille technologique
====================

:date: 2020-10-19 12:00
:category: Instrumentation
:tags: technologie frugale, optique


Le détournement de technologie (hacking) du disque dur, réalisé par un
labo de recherche, est intéressant pour les utilisateurs de lasers. Ce
n’est pas récent (2007) mais potentiellement intéressant et
participant en plus à l’économie circulaire.

“Hacking examples laser shutter from hard drives”,
https://www.ph.unimelb.edu.au/~scholten/atomopt/shutter/shutter.html

They have developed a high-speed laser shutter based on voice-coil
actuators found in generic hard disk drives:
`Maguire LP, Szilagyi S and Scholten RE, High performance laser shutter using a hard disk drive voice-coil actuator, Rev. Sci. Instrum. 75 (9) 3077-3079 (2004)\: <https://www.ph.unimelb.edu.au/atomopt/publications/shutter_rsi75_p3077_2004.pdf>`__.

Il y a une amelioration introduite en 2007

(update: sub-microsecond shutter) : We have enhanced the shutter with
a new driver circuit described below and a notched flag, achieving
500ns rise time and sub-microsecond pulse durations.

See `Scholten RE, Enhanced laser shutter using a hard disk drive voice-coil actuator, Rev. Sci. Instrum.  78 (2) 026101 (2007)_: <https://www.ph.unimelb.edu.au/atomopt/publications/shutter_rsi78_026101_2007.pdf>`__.

|image0|

Il faut remarquer que les disques durs recèlent d’autres possibilités de
récupération :

i. Les disques eux même dont la surface est extrêmement plate
   (pour que la tête de lecture puisse se déplacer très près de
   la surface) font d’excellents miroirs pour les faisceaux laser.
ii. Spin coating : cela consiste à recouvrir de manière uniforme un
    substrat posé sur le disque associé au moteur par un liquide en
    faisant tourner le disque à grande vitesse.
iii. Le moteur brushless (moteur sans balais) peut être récupéré
     pour diverses applications.
iv. Enfin les aimants situés sous la tête de lecture (très puissants -
    néodyme) peuvent être récupérés, au prix de quelques efforts !!

.. |image0| image:: /images/bulletin-2020-10/disque-dur.png
   :width: 250px
