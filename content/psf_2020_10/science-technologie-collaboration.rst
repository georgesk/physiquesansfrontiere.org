Science, technologie, collaboration, solidarité et COVID 19
===========================================================

:date: 2020-10-19 12:00
:category: Open access
:tags: technologie frugale, covid, optique

(i)Article Duke University
--------------------------

Mise au point d’un dispositif à bas coût permettant de déterminer
l’efficacité de filtration des aérosols par différents types de
masques. La présence d’aérosols est déterminée par l’interaction du
flux en aval du masque avec un faisceau laser mis en forme par une
lentille cylindrique. Une caméra (web cam) prend des images à
intervalles réguliers, et l’analyse de ces images permet de déterminer
le nombre d’aérosols. ( Fischer et al., Sci. Adv.
10.1126/sciadv.abd3083 (2020) .

Pour beaucoup de pays ce serait un moyen de test des masques achetés à
condition de disposer d’un labo d’optique ou de photonique.

(ii) Article Manu Prakash
-------------------------

Nouveau modèle de centrifuge à main associé à un test de détection du
virus, le tout très frugal. Les développements de Manu Prakash sont
ciblés pour les pays à faibles ressources, pour qu’ils puissent même
dans des conditions hostiles réaliser des tests.
(https://doi.org/10.1101/2020.06.30.20143255)

(iii) Développement de respirateurs
-----------------------------------

.. raw:: html

         <!-- format spécial pour forcer le retour à la ligne après -->
         <span style="display: table-cell;">
         
|image0|

.. raw:: html
         
         </span>

Une brève concernant un projet de ventilateur impulsé par un groupe de
scientifiques d’origine italienne spécialistes de la matière noire a
été publié dans le journal de la SFP « Reflets de la physique »
N) 65 page 5). Il faut remarquer que ces scientifiques ont quasiment
arrêté leurs recherches propres pour se consacrer au développement de
respirateurs (avec des partenaires). Cela indique qu’il est possible
d’être un scientifique de haut niveau et de posséder une fibre
solidaire.

Pour plus de renseignements :
http://mvm.care/wp-content/uploads/2020/05/MVM-FDA-EUA-International-Press-Release-1.pdf

Ils ont été soutenus en France par APC, CNRS/IN2P3, Université de
Paris: www.apc.univ-paris7.fr MINES ParisTech, Paris:
mines-paristech.fr SUBATECH, CNRS/IN2P3, IMT-Atlantique, Université de
Nantes: www-subatech.in2p3.fr. Ils ont reçu l’agrément de la FDA (Food
and Drug Administration) aux USA.

La structure du projet est également une illustration de la
multidisciplinarité qui est nécessaire à ce type de projets.

Un collectif français (Makers for Life) a contribué à un projet de
respirateur nommé Makair (https://makair.life/), le projet a été mené
en coopération avec le CEA, le CHU de Nantes, il a été conçu avec des
expert.e.s universitaires et des médecins, il s’agit d’un projet en
source ouverte (Open source), tout le monde peut utiliser les plans et
créer son Mak Air sans payer de licence, les plans sont accessibles :
https://github.com/makers-for-life/makair/. Mak Air veut répondre aux
exigences essentielles de la directive européenne relative aux
dispositifs médicaux 93/42/CEE. Le matériel utilisé est librement
disponible dans le commerce. Il y a un podcast récent (16 octobre) du
journal Le Monde qui a traité récemment de cela :
https://www.lemonde.fr/le-monde-evenements/article/2020/10/12/innovation-citoyenne-l-aventure-du-makair_6055693_4333359.html

Cependant, nous regrettons que personne ne se soit lancé dans des
projets de décontamination par UV-C, que ce soit par des sources à
lampe mercure ou des sources LEDs (à condition que leur prix soit
accessible et qu’elles soient disponibles). Si ce genre de projet
pluridisciplinaire réussit ce serait une bonne nouvelle pour beaucoup
de pays car des masques pourraient être réemployés. Ces méthodes sont
utilisées dans différents pays asiatiques.

.. |image0| image:: /images/bulletin-2020-10/makair.png
   :width: 220px
