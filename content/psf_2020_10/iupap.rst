IUPAP Commission C13 (Physics for Dévelopment)
##############################################

:date: 2020-10-19 12:00
:category: Rencontres
:tags: Asie, Amérique Latine, UNESCO, technologie frugale, nouvelles

Bref compte rendu de la réunion annuelle (par visio-conférence) le 24 août 2020
===============================================================================

Avec le représentant Indien Ajith Kumar, nous avons proposé la
création d’un catalogue (repertory) regroupant par domaines de la
physique les développements instrumentaux réalisés avec le paradigme «
open source hardware » et celui du low cost instruments. Maintenant il
faut le construire.

- Ajith Kumar a préparé une page Github pour lancer le projet : https://iupapc13.github.io/


Quinze personnes de quatre continents étaient présentes en
visio-conférence. Nous avons évoqué les effets, de la pandémie sur
l’enseignement et sur la physique expérimentale. Ces effets étaient la
6plupart du temps importants sauf pour certains pays comme la Suède où
il n’y a eu qu’un confinement très léger et où l’enseignement n’a été
que peu perturbé. Par contre en Amérique latine (Colombie – Pérou)
cela a beaucoup impacté les étudiants « pauvres » et les universités
leur ont également fourni des produits alimentaires. Le problème
important c’est la poursuite de la pandémie à un niveau élevé sans
pause après la première vague.

Nous allons essayer d’établir un document après un sondage rapide des
membres de la commission.

Réflexion de l’UNESCO sur « l’open science »
============================================

En voici le logo

|image0|


.. raw:: html
	 
	 <div style="clear: both"></div>


Dernière minute :
=================

**Le flash info n°6** de l’association Chimistes sans Frontières vient
de sortir, il y a beaucoup d’informations intéressantes sur la
coopération Nord Sud qu’ils promeuvent avec des actions impliquant
beaucoup de jeunes chimistes. Nous espérons que les jeunes physiciens
pourront relever le défi. ! Les informations peuvent être consultées
sur leur site http://www.chimistessansfrontieres.fr/

.. |image0| image:: /images/bulletin-2020-10/openscience-logo.png
   :width: 450px
