Bulletin Physique sans Frontières (oct 2020)
############################################

:date: 2020-10-19 12:01
:category: Bulletin
:tags: nouvelles


ÉDITORIAL:
==========

Le bulletin reprend son cours habituel après l’interruption due à
l’épidémie COVID 19. Cette épidémie a eu beaucoup de conséquences sur
le fonctionnement de toutes les structures d’enseignement.
(universités, grandes écoles,...). La principale conséquence a été le
passage à des enseignements à distance, avec des réussites et des
difficultés. Cependant pour ce qui concerne l’enseignement des
sciences expérimentales peu de solutions alternatives aux travaux
pratiques ont étés mises en place.  Soulignons les efforts de certains
(qu’ils soient professeurs ou scientifiques retraités) comme Emmanuel
Maisonhaute ou Élie Campagnolo qui ont « débridé » leur créativité
pour créer des vidéos permettant aux étudiants de réaliser des
expériences chez eux, basiques certes mais créatives, ne demandant
pratiquement que du matériel disponible chez soi et source de
questionnements. Nous avons inséré dans ce bulletin des liens vers ces
initiatives.

Il y a eu aussi les vidéos de « Merci la Physique » (Jean Michel
Courty) et les expériences confinées de Julien Bobroff.


Si l’impact de l’épidémie a été important pour nos sociétés
développées, il a été dévastateur pour les pays disposant de moins de
ressources. Beaucoup d’étudiants ne disposaient pas des équipements
informatiques nécessaires pour suivre les cours en téléconférence. Les
conditions de vie ont été affectées par la réduction des activités
économiques, ce qui pour une partie de la population a créé des
pénuries pour l’alimentation. Certaines universités ont distribué des
repas aux nombreux étudiants qui rencontraient ce type de problème.

L’avenir ne se présente hélas pas radieux pour l’instant. L’épidémie
se poursuit et continue à affecter les économies. L’enseignement
scientifique est très affecté. L’impact pour les matières théoriques
est moindre que pour les sciences expérimentales, cependant dans tous
les cas le manque de contacts directs est préjudiciable.

Nous sommes plusieurs à penser que pour les sciences expérimentales le paradigme de la création
d’équipements en source ouverte (open source) est à même de contribuer à la réalisation de travaux
pratiques à coût réduit et innovants. Il manque cependant un « catalogue » recensant ces équipements
par domaines de la physique, un projet d’établissement de catalogue est en cours à la commission C13
(Physics for Development) de la IUPAP.

La créativité exprimée par certains d’entre nous dans la réalisation
de nouveaux TP et d’expériences confinées est importante car elle sera
mise à profit pour améliorer la situation pour l’enseignement 1des
premières années d’université dans les pays à faibles ressources à
condition qu’il y ait des échanges efficaces et des formations (qui
devront aussi être pour l’instant effectuées à distance).

Notre action « Experiment action » ( ateliers de physique sans
frontières ) qui devait se dérouler au Burkina Faso est programmée
pour décembre. Pour le Burkina Faso, les vols entre la France et le
Burkina ayant repris, l’organisateur Arouna Darga (maître de
conférences à Sorbonne Université- centre de Jussieu) devrait s’y
rendre en décembre pour l’atelier consacré au « solaire », ceci bien
sur si les conditions sanitaires le permettent ainsi que les
conditions d’absence de quarantaine.

Enfin, il nous faudrait convaincre des étudiants français à travailler
à la réalisation de kits expérimentaux pouvant être utilisés dans des
laboratoires ayant des connaissances en photonique sur le modèle du
dispositif conçu par des chercheurs de l’université Duke. Ce
dispositif « à bas coût » dont vous trouverez les références dans ce
bulletin permet le test de l’efficacité de filtrage des aérosols pour
différents types de masques. Il est basé sur l’hypothèse du transport
du virus par les aérosols, gouttelettes, etc.. Un autre axe de travail
serait l’utilisation de l’UV-C pour la décontamination des masques et
la réalisation d’un dispositif « modeste » à partir de LEDs UV-C. Il
faudra cependant absolument travailler avec des médecins pour
confirmer l’efficacité du dispositif. Des jeunes chercheurs africains
formés en optique pourront être associés à ce projet. Remarquons que
ce genre de dispositif est déjà utilisé en Corée du Sud, Chine et
Taïwan et que des articles scientifiques existent sur ce sujet.

Il reste cependant à sensibiliser et trouver des étudiants en thèse
désireux de travailler (à temps très partiel) pour des actions
destinées à l’humanitaire scientifique. Ceci pourrait être valorisé
dans le cadre de leur thèse ou être l’objet de leur éventuelle année
de césure. Ce type d’action est courant dans les pays anglo-saxons,
comme le microscope « waterscope » développé par des jeunes thésards
du département de physique de l’université de Cambridge pour ne
prendre que l’un des exemples. Cette action a de plus été soutenue
très efficacement par l’université de Cambridge (ainsi que celle de
Bath) qui a financé un Fab Lab (laboratoire de fabrication -ici en
particulier c’est l’impression 3D qui est utilisée) en Tanzanie pour
fabriquer le microscope en Afrique.

Il nous reste du travail pour convaincre des institutions françaises à
travailler à des projets semblables à ceux réalisés par nos collègues
d’outre-manche.

Les articles de ce bulletin :
-----------------------------

- `Science, technologie, collaboration, solidarité et COVID 19 <{filename}/psf_2020_10/science-technologie-collaboration.rst>`__
- `Veille technologique <{filename}/psf_2020_10/veille-technologique.rst>`__
- `Physique et sciences expérimentales confinées <{filename}/psf_2020_10/physique-confinee.rst>`__

Nouvelles :
-----------

- `AFRIQUE <{filename}/psf_2020_10/afrique.rst>`__
- `AMÉRIQUE LATINE <{filename}/psf_2020_10/amerique-latine.rst>`__ 
- `IUPAP Commission C13 (Physics for Dévelopment) <{filename}/psf_2020_10/iupap.rst>`__
