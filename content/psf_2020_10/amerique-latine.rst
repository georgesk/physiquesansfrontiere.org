Nouvelles d'Amérique Latine :
#############################

:date: 2020-10-19 12:00
:category: Rencontres
:tags: Amérique Latine, technologie frugale, chimie, biologie, nouvelles

Pérou :
=======

**Montoya Eduardo: nouveau spectro Raman à bas coût**
http://article.sapub.org/10.5923.j.jlce.20150304.02.html
et https://hackaday.io/project/33762-3d-printable-raman-probe

Mexique :
=========

**Microscope de fluorescence imprimé en 3D** Journal Plos one :
http://article.sapub.org/10.5923.j.jlce.20150304.02.html
https://doi.org/10.1371/journal.pone.0215114 october 2019

Bolivie :
=========

**Omar Omarchea**, nouveau dispositif de tests biologiques **ELISA** à bas
coût avec smartphone (video en espagnol disponible sur demande).
