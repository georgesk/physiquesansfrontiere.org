PHYSIQUE ET SCIENCES EXPÉRIMENTALES CONFINÉES
#############################################

:date: 2020-10-19 12:00
:category: Enseignement
:tags: vulgarisation, chimie, optique, technologie frugale

Diverses manips
===============

Manips confinées présentées par Julien Bobroff,
La série de vidéos « Merci la Physique »
réalisées par Jean Michel Courty.

**Ulysse Delambre** : livre Smartphonique
(**Expériences de physique avec un smartphone** ed. Dunod ) l’utilisation
du smartphone permet de rendre les travaux pratiques plus attractifs
et en utilisant moins de moyens.

L’utilisation des applications (App) libres comme **Phyphox** permet
l’accès aux différents capteurs et actuateurs des smartphones.

Électrochimie confinée
======================

Notre collègue *Emmanuel Maisonhaute* (professeur LISE UMR 8235
CNRS-Sorbonne Universités Jussieu) a lancé un canal Youtube qui
s’appelle le « **Le courant passe** » pour que les étudiants motivés
puissent réaliser des manips à la maison et se former aux bases
expérimentales de l’électrochimie.  Je vous recommande **l’épisode 3**
https://www.youtube.com/watch?v=bifEBSB1_9Y

La saison 2 ...
===============

\... est maintenant disponible et s’intitule « **on met le feu !** » :
https://www.youtube.com/watch?v=X2YKy19uwfQ

Autres
======

Par ailleurs, *Emmanuel Maisonhaute* et *Élie Campagnolo* (Grenoble) ont
développé un potentiostat à bas coût et en source ouverte.
(https://publiclab.org/wiki/potentiostat ,
https://en.wikipedia.org/wiki/Potentiostat). Cette chaine You Tube
arrive à un nombre de 600 abonnés.

Une des applications importantes de l’électrochimie est la détection
de métaux lourds dans l’eau. C’est pourquoi, il serait important de
développer un dispositif à faible coût et en source ouverte pour qu’il
soit disponible pour beaucoup de pays.

Paradoxalement, ces développements créatifs destinés à remédier en
partie à l’impossibilité de réaliser des travaux pratiques en
présentiel et à l’impossibilité de pratiquer les sciences
expérimentales peuvent se révéler une chance pour la physique et les
sciences expérimentales dans les pays à faibles ressources.
