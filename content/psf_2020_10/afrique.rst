Nouvelles d'Afrique :
#####################

:date: 2020-10-19 12:00
:category: Rencontres
:tags: Afrique, covid, projet, nouvelles


Ateliers Physique sans Frontières
=================================

Action « Experiment Action »
----------------------------

Cette action est sous la supervision de **AROUNA DARGA** (Maître de Conférence à
Sorbonne Université)

Pour notre premier atelier « Experiment action » au Burkina Faso, voici quelques informations :

- **Lieu de l'atelier :** École Normale Supérieure de Koudougou
- **Publics ou participants ciblés :** élèves enseignants du secondaire et du supérieur en formation
- **participation locale :**
    i. Madame Wénégouda Olivia Solange ZAGARE, épouse CONGO Enseignante-chercheur en didactique et génie électrique à l'École Normale Supérieure de Koudougou
    #. M\. Moussa TISSOLO, Enseignant-chercheur en électronique, Université Norbert Zongo de Koudougou / École Supérieure Polytechnique de la Jeunesse (ESUPJ)
- **Thème de l'atelier :** Le solaire photovoltaïque comme support pour la diffusion de la science expérimentale. Atelier de fabrication et de caractérisation optoélectronique d'un module solaire photovoltaïque à base de Silicium cristallin.
    i. Fonctionnement d'un composant optoélectronique, la cellule solaire en mode générateur ou/et capteur de lumière ; mesure de la caractéristique courant-tension, photoluminescence ;
    #. de la cellule au module solaire : encapsulant, circuits électriques, process industriel vs artisanat ;
    #. conception & fabrication d'une lampe solaire.
- **Date prévue (si tout va bien) :** entre le 23 novembre et le 5 décembre 2020. Nous avons contacté DANIEL LINCOT de l’Institut Photovoltaïque d’Île de France et nous allons essayer de nous regrouper pour acheter des cellules solaires.


Par ailleurs **Arouna Darga** supervise la césure de Sébastien FERREIRA
étudiant à l’école Normale Supérieure de Rennes et agrégé de sciences
Industrielles, il se trouve actuellement à Saint Louis du Sénégal pour
le projet « *Turning sun into water* » (GeePs, Imperial College London,
Stanford University et Dargatech Sarl). Le projet consiste à
développer un modèle de demande en eau plus à même de prédire avec
plus de précision la consommation et réduire in fine le coût de l’eau.
Lien pour le financement participatif :
https://www.helloasso.com/associations/eau-fil-du-soleil/collectes/turning-sun-into-water-demand-model

Information fournie par François Scylla :
-----------------------------------------
un étudiant guinéen vient d’intégrer l’école Polytechnique.

Thèse :
-------

Nous avons reçu la thèse de **Benoit Kouakou** (Institut Houphouët Boigny
Yamasoukroh), qui a consisté à monter un lidar et à sa mise au point
pour la détection d’insectes, en particulier les moustiques femelles
pour observer leurs changements d’habitudes. C’est Mikkel Brydegaard
de l’université de Lund qui a été l’un des encadrants de cette
thèse. Le directeur du labo est Jérémie Zoueu. Il y a eu un soutien
financier important de la part de l’International Science program
(ISP) organisme gérant la coopération internationale suédoise. Ce
soutien a concerné la fourniture du matériel (Lidar) ainsi que les
déplacements de l’encadrant.

Situation en Tunisie :
----------------------

**Le professeur Samia Charfi Kaddour Directrice Générale de la Recherche Scientifique nous informe sur la manière dont l’enseignement à l’université en Tunisie a traversé la crise du COVID :**

« Nous nous sommes mis aux cours en ligne pendant la pandémie avec une
adhésion de 70 % des enseignants avec les moyens du bord. Différents
outils ont été utilisés avec les supports disponibles.  Des cours en
ligne ont été assurés avec le soutien logistique de l'université
virtuelle de Tunis et avec les outils disponibles dans les universités
comme Google meet, teams, etc ...

Les enseignants ont découvert la communication à distance et un
certain nombre y prennent goût.

Des collectes ont été effectuées pour aider les plus nécessiteux qui
n'avaient pas d'ordinateurs.

Étant donné que la pandémie a bien été maîtrisée, nous avons pu
reprendre les cours fin mai, le mois de juin et nous avons fait passer
le bac, tous les examens nationaux et tous les examens à l'université.
La rentrée a pris un peu de retard car certains établissements n'ont
pas fini la seconde session.

Par ailleurs, on a lancé à la DGRS un appel à projet R&D covid 19 et
nous avons actuellement 14 projets qui tournent avec des
problématiques autour du covid 19 (médecine, biotechnologie,
plateforme numérique, respirateurs ...) »

