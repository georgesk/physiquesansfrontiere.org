#! /usr/bin/python3

import sys, os, subprocess, re, bs4

def lienGitHub (theme, lesLiens):
    """
    renvoye un lien HTML vers une page de GitHub
    @param theme le nom d'un thème pour Pelican
    @param lesLiens in dictionnaire nom de thème => lien Github
    """
    if theme in lesLiens:
        lien=f"""<a href="{lesLiens[theme]}" target="_github">Source dans Github</a>"""
    else:
        lien="<br/>"
    return lien

def traiteImages(imagedir, outfile=None):
    """
    Recherche les images nommées machin.png dans un répertoire et fabrique
    une page HTML qui les présente

    """
    if outfile is None:
        outfile=sys.stdout
    pngPattern = re.compile(r"(.*)\.png")
    cmd="wget https://github.com/getpelican/pelican-themes -O -"
    out, err= subprocess.Popen(
        cmd, shell=True, stdout=subprocess.PIPE, stderr= subprocess.PIPE
    ).communicate()
    githubPage=out.decode("utf-8")
    githubPage=bs4.BeautifulSoup(githubPage,features="lxml")
    lesLiens={}
    themePattern=re.compile(r"([-_\w]+)( @ [0-9a-f]+)?")
    # il faut considérer seulement les descendants de <div class="file-wrap">
    div=githubPage.find(attrs={"class":"file-wrap"});
    for a in div.find_all("a"):
        m=themePattern.match(str(a.string))
        if m:
            lien=a.get("href")
            if lien and lien[0]=="/":
                lien="https://github.com"+lien
            lesLiens[m.group(1)]=lien

    outfile.write("""\
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title> Les styles</title>
  <meta charset="utf-8" />
</head>
<body>
<table>
""")

    cmd=f"ls {imagedir}/*.png | grep -v 'mini'"
    out, err = subprocess.Popen(
        cmd, shell=True, stdout=subprocess.PIPE, stderr= subprocess.PIPE
    ).communicate()
    for f in out.decode("utf-8").split("\n")[:-1]:
        p=os.path.basename(f)
        m=pngPattern.match(os.path.basename(p))
        ligne=f"""\
  <tr>
    <td>{m.group(1)}</td>
    <td><a href="/images/psf-themes/{p}" target="_view"><img src="/images/psf-themes/{m.group(1)}-mini.png" alt="copie d'écran du thème {m.group(1)}"/></a></td>
    <td>{lienGitHub(m.group(1), lesLiens)}</td>
  </tr>
"""
        outfile.write(ligne)
        
    outfile.write("""\
</table>
</body>
</html>
""")
    return

if __name__=="__main__":
    imagedir="content/images/psf-themes"
    outfile=None
    if len(sys.argv) <= 1:
        print(f"Usage : {sys.argv[0]} <nom de fichier HTML à produire>")
        print("\n fait une liste des copies d'écran obtenues pour divers styles")
    else:
        outfile=open(sys.argv[1],"w")
        traiteImages(imagedir, outfile)
        print(f"Génération du fichier {sys.argv[1]} terminée")
