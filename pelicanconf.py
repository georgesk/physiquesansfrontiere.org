#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
from subprocess import Popen, PIPE

AUTHOR = u'PSF'
SITENAME = u'Physique Sans Frontière'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
FEED_RSS="feeds/rss.xml"

# Blogroll
LINKS = (
    ('SFP', 'Société Française de Physique', 'https://www.sfpnet.fr/'),
    ('UDPPC','Union des Professeurs de Physique et de Chimie','http://national.udppc.asso.fr/'),
)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)
SOCIAL = None

DEFAULT_PAGINATION = 20

THEME = 'themes/waterspill-fr'

PAGE_PATHS = ['images/favicon.png']
STATIC_PATHS = ['images','data','pages']
EXTRA_PATH_METADATA = {
    'images/favicon.png': {'path': 'favicon.png'}
}

READERS = {'html': None}

# adds the icon of w3c validator at the bottom of pages enriching base.html
W3C_VALIDATOR = True

SLUGIFY_SOURCE = 'basename'

ARTICLE_URL = 'articles/{date:%Y}/{date:%b}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = 'articles/{date:%Y}/{date:%b}/{date:%d}/{slug}/index.html'
PAGE_URL = 'pages/{slug}/'
PAGE_SAVE_AS = 'pages/{slug}/index.html'

# if the hostame is "psf" RELATIVE_URLS will be False
RELATIVE_URLS = Popen("hostname", shell=True, stdout=PIPE, stderr=PIPE).communicate()[0]!="psf"
