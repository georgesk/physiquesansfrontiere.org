/**
 * routines propres au style waterspill-fr
 **/

$(function(){
    /* quand le document est chargé */
    /**
     * on met en ordre alphabétique les tags
     **/
    var tagList=$("#tag_list");
    var li=tagList.find("li");
    li.sort(function(a, b){
	return ($(b).text()) < ($(a).text()) ? 1 : -1;
    }).appendTo(tagList);
    /**
     * s'il y a des articles, on dévoile le lien vers eux
     **/
    var otherBlogItems = $(".otherBlogItem");
    if (otherBlogItems.length > 0){
	$("#autres").css({"display": "block",});
    }
});
